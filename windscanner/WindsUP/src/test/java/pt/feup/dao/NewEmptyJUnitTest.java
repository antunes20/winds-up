/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import pt.feup.entity.Campaign;
import pt.feup.entity.User;

/**
 *
 * @author José
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:WindsUP-servlet.xml")
@TransactionConfiguration
public class NewEmptyJUnitTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    private UserDao UserDao;
    @Autowired
    private CampaignDao campaignDao;

    public NewEmptyJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void getUserbyID() {
   // Collection <User> ulist = new ArrayList <User> ();
        //UserDao userDao = new UserDaoImpl();
        User u = UserDao.getUserByID(100L);
        Assert.assertEquals(100L, u.getId().longValue());
    }

    @Test
    public void getCampaignByID() {
   // Collection <User> ulist = new ArrayList <User> ();
        //UserDao userDao = new UserDaoImpl();
        Campaign c = campaignDao.getCampaignById(1L);
        Assert.assertEquals(1L, c.getId().longValue());
    }
    
    @Test
    public void getAllCampaigns() {
   // Collection <User> ulist = new ArrayList <User> ();
        //UserDao userDao = new UserDaoImpl();
        //List<Campaign> c = campaignDao.findByCriteria(null);
         //Assert.assertEquals(4, c.size());
    }

    /* @Test
     public void addUser(){
     User u = new User();
     u.setActive(true);
     u.setUsername("antunes20");
     u.setPassword("teste");
     u.setFamilyName("Magalhães");
     u.setFirstName("José");
     u.setEmail("jose@fe.up.pt");
     //UserDao.addUser(u);
     UserDao.saveOrUpdate(u);
     u = new User();
     u = UserDao.findByName("antunes20");
     Assert.assertEquals("antunes20", u.getUsername());
     }*/
}
