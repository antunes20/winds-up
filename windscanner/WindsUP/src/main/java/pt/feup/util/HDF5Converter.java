/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.util;

import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5File;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.CampaignDao;
import pt.feup.dao.DSTemplateDao;
import pt.feup.dao.DatasetDao;
import pt.feup.dao.MeasurementDao;
import pt.feup.dao.ScenarioDao;
import pt.feup.entity.Campaign;
import pt.feup.entity.DatasetTemplate;
import pt.feup.entity.Measurement;
import pt.feup.entity.Scenario;
import pt.feup.task.DatasetTaskFactory;
import pt.feup.task.DatasetToDbTask;
import pt.feup.task.MeasurementToDbTask;
import pt.feup.task.ScenarioTaskFactory;
import pt.feup.task.ScenarioToDbTask;

/**
 *
 * @author José Magalhães
 */
@Component
@Transactional
public class HDF5Converter {

    private static String fname = "C:/hdf/wind.h5";
    private static long[] dims2D = {0, 0};
    private static long[] chunks = {10, 1};
    private static H5File testFile;
    private static int dim_x, dim_y;
    private static Group root;
    private static final ExecutorService dbService = Executors.newSingleThreadExecutor();
    private static final ExecutorService taskService = Executors.newFixedThreadPool(Constants.NUM_THREADS - 2);
    private final static Object lock = new Object();

    private ArrayList<Scenario> scenarios = new ArrayList<Scenario>();
    private ArrayList<pt.feup.entity.Dataset> datasets = new ArrayList<pt.feup.entity.Dataset>();
    private ArrayList<Measurement> measus = new ArrayList<pt.feup.entity.Measurement>();
    private int count = 0;

    @Autowired
    private CampaignDao campaignDao;
    
    @Autowired 
    private DSTemplateDao dsTemplateDao;
    
    @Autowired
    private DatasetDao datasetDao;

    @Autowired
    private ScenarioDao scenarioDao;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private ScenarioTaskFactory scTaskFactory;

    @Autowired
    private DatasetTaskFactory dtTaskFactory;

    private static HDF5Converter converter;

    /*public static void main(String args[]) throws Exception {
     if(args.length != 1){
     System.out.println("Usage:HDF5Converter <path>");
     return;
     }
     // retrieve an instance of H5File
     FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);
            
     if (fileFormat == null) {
     System.err.println("Cannot find HDF5 FileFormat.");
     return;
     }

     // create a new file with a given file name.
     testFile = (H5File) fileFormat.createFile(fname, FileFormat.FILE_CREATE_DELETE);

     if (testFile == null) {
     System.err.println("Failed to create file:" + fname);
     return;
     }
     testFile.open();
     root = (Group) ((javax.swing.tree.DefaultMutableTreeNode) testFile.getRootNode()).getUserObject();
     System.out.println("Extracting...");
     //unzip(args[0], null);
     System.out.println("Done!");
     System.out.println("Converting...");
     convertToHDF5();
     System.out.println("Done!");
     // close file resource
     testFile.close();
     executorService.shutdown();

     }*/
    public static HDF5Converter BuildFactory() {
        if (converter == null) {
            converter = new HDF5Converter();
        }
        return converter;
    }

    private static void readFromFile(File file, Group g1) throws FileNotFoundException, Exception {
        ArrayList<Double> data = new ArrayList<Double>();
        CSVReader reader = null;
        long id_start, id_stop;
        double azimuth, elevation, t_start, t_stop;
        int i = Constants.DISTANCE_FIELD;
        int k = 0;
        String filename = file.getName();
        String name = "";
        int pos = filename.lastIndexOf(".");
        if (pos > 0) {
            name = filename.substring(0, pos);
        }
        reader = new CSVReader(new FileReader(file.getPath()), ';');

        try {
            //Get the CSVReader instance with specifying the delimiter to be used

            String[] nextLine;
            //Read one line at a time
            while ((nextLine = reader.readNext()) != null) {
                String[] nextLine2 = new String[nextLine.length - 6];
                k++;
                id_start = Long.parseLong(nextLine[Constants.ID_START_FIELD]);
                id_stop = Long.parseLong(nextLine[Constants.ID_STOP_FIELD]);
                t_start = Double.parseDouble(nextLine[Constants.T_START_FIELD]);
                t_stop = Double.parseDouble(nextLine[Constants.T_STOP_FIELD]);
                azimuth = Double.parseDouble(nextLine[Constants.AZIMUTH_FIELD]);
                elevation = Double.parseDouble(nextLine[Constants.ELEVATION_FIELD]);
                System.arraycopy(nextLine, 6, nextLine2, 0, nextLine.length - 6);
                for (String token : nextLine2) {
                    switch (i) {
                        case Constants.DISTANCE_FIELD:
                            data.add((double) id_start);
                            data.add((double) id_stop);
                            data.add(t_start);
                            data.add(t_stop);
                            data.add(azimuth);
                            data.add(elevation);
                            data.add(Double.parseDouble(token));
                            i = Constants.RADIAL_SPEED_FIELD;
                            break;
                        case Constants.RADIAL_SPEED_FIELD:
                            data.add(Double.parseDouble(token));
                            i = Constants.CNR_FIELD;
                            break;
                        case Constants.CNR_FIELD:
                            data.add(Double.parseDouble(token));
                            i = Constants.DISPERSION_FIELD;
                            break;
                        case Constants.DISPERSION_FIELD:
                            data.add(Double.parseDouble(token));
                            i = Constants.DISTANCE_FIELD;
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        dim_x = Constants.NR_FIELDS;
        dim_y = data.size() / dim_x;
        dims2D[0] = dim_y;
        dims2D[1] = 1;
        chunks[0] = dim_y / 2;
        synchronized (lock) {
            // create 2D 64-bit (8 bytes) integer dataset of 20 by 10
            Datatype dtype = testFile.createDatatype(Datatype.CLASS_FLOAT, 4, Datatype.NATIVE, Datatype.NATIVE);
            testFile.createScalarDS(name, g1, dtype, dims2D, null, chunks, 9, data.toArray(new Double[data.size()]));
        }
    }
    //Zip4j

    public static void convertToHDF5() throws Exception {
        // create new filename filter
        FilenameFilter fileNameFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if (name.contains(Constants.WIND_FILE)) {
                    return true;
                }
                return false;
            }
        };

        // retrieve an instance of H5File
        FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);

        if (fileFormat == null) {
            System.err.println("Cannot find HDF5 FileFormat.");
            return;
        }

        // create a new file with a given file name.
        testFile = (H5File) fileFormat.createFile(fname, FileFormat.FILE_CREATE_DELETE);

        if (testFile == null) {
            System.err.println("Failed to create file:" + fname);
            return;
        }
        testFile.open();
        root = (Group) ((javax.swing.tree.DefaultMutableTreeNode) testFile.getRootNode()).getUserObject();

        File folder = new File(Constants.UNZIP_DIRECTORY);
        File[] listOfFiles = folder.listFiles();
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                System.out.println("File " + listOfFile.getName());
            } else if (listOfFile.isDirectory()) {
                File[] list;
                list = listOfFile.listFiles(fileNameFilter);
                // create groups at the root
                Group g1 = testFile.createGroup(listOfFile.getName(), root);
                ExecutorCompletionService ecs = new ExecutorCompletionService(dbService);
                for (File list1 : list) {
                    //executorService.execute(;
                    ecs.submit(new Task(list1, g1));
                }
                for (File list1 : list) {
                    ecs.take();
                }
                System.out.println("Directory " + listOfFile.getName() + " completed");
            }
        }
        // close file resource
        testFile.close();
        dbService.shutdown();
    }

    public static class Task implements Callable {

        File file;
        Group g;

        public Task(File f, Group _g) {
            file = f;
            g = _g;
        }

        @Override
        public Boolean call() throws Exception {
            try {
                readFromFile(file, g);
            } catch (Exception ex) {
                Logger.getLogger(HDF5Converter.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            return true;
        }
    }

    public void HDF5Import(String fname) throws Exception {
        // retrieve an instance of H5File
        FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);

        if (fileFormat == null) {
            System.err.println("Cannot find HDF5 FileFormat.");
            return;
        }

        // open the file with read and write access
        FileFormat testFile = fileFormat.createInstance(fname, FileFormat.READ);

        if (testFile == null) {
            System.err.println("Failed to open file: " + fname);
            return;
        }

        // open the file and retrieve the file structure
        testFile.open();
        root = (Group) ((javax.swing.tree.DefaultMutableTreeNode) testFile.getRootNode()).getUserObject();
        readFromHDF5(root.getMemberList(), null);
        //scTask.obj = (ArrayList<Scenario>) scenarios;
        //executorService.execute(scTask);
        //taskService.shutdown();
        taskService.shutdown();
        while (!taskService.isTerminated());

        System.out.println(count / 10);
    }

    private void readFromHDF5(List<HObject> objs, final Scenario scenario) throws Exception {
        List<Attribute> list = new ArrayList<>();
        list = root.getMetadata();
        long [] tmp = (long[])(list.get(0).getValue());
        long campaign_id = tmp[0];
        final DatasetTemplate dsTemplate = dsTemplateDao.findById(4L);
        for (final HObject obj : objs) {
            if (obj instanceof Group) {
                Scenario mr = new Scenario();
                mr.setDatasetCollection(new ArrayList<pt.feup.entity.Dataset>());
                mr.setCampaign(campaignDao.findById(campaign_id));
                mr.setCreatedAt(new Date());
                mr.setUpdatedAt(new Date());
                mr.setName(obj.getName());
                mr.setTimestampFrom(new Date());
                mr.setTimestampTo(new Date());
                ScenarioToDbTask scTask = scTaskFactory.createTask();
                scTask.obj.add(mr);
                taskService.execute(scTask);
                //scenarioDao.save(mr);
                readFromHDF5(((Group) obj).getMemberList(), mr);
            } else if (obj instanceof Dataset) {
                taskService.execute(new Runnable() {
                    @Override
                    public void run() {
                        importToDatabase((Dataset) obj, scenario, dsTemplate);
                    }
                });
            }
        }
    }

    public void importToDatabase(Dataset dt, Scenario scenario, DatasetTemplate dsTemplate) {
        pt.feup.entity.Dataset datasetdb = new pt.feup.entity.Dataset();
        datasetdb.setMeasurementCollection(new ArrayList<Measurement>());
        datasetdb.setName(dt.getName());
        datasetdb.setScenario(scenario);
        datasetdb.setComplete(true);
        datasetdb.setPublished(true);
        datasetdb.setUpdatedAt(new Date());
        datasetdb.setCreatedAt(new Date());
        datasetdb.setDsTemplateId(dsTemplate);
        //datasets.add(datasetdb);
        //scenario.getDatasetCollection().add(datasetdb);
        //dtTask.obj = (ArrayList<pt.feup.entity.Dataset>) datasets.clone();
        //executorService.execute(dtTask);
        // sizes[0] = 1;
        float[] tmp = null;
        try {
            tmp = (float[]) dt.read();
        } catch (Exception ex) {
            Logger.getLogger(HDF5Converter.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (OutOfMemoryError ex) {
            Logger.getLogger(HDF5Converter.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        //sizes[1] = Constants.NR_FIELDS;
        //start[1] = 0;
        for (int i = 0; i < tmp.length; i = i + 10) {
            //start[0] = i;
            Measurement m = new Measurement();
            m.setDsId(datasetdb);
            try {
                //float[] tmp = (float[]) dt.read();
                BigDecimal[] bg = new BigDecimal[Constants.NR_FIELDS];
                for (int k = 0; k < Constants.NR_FIELDS; k++) {
                    bg[k] = new BigDecimal(tmp[i + k]);
                }
                m.setData(bg);

            } catch (Exception ex) {
                Logger.getLogger(HDF5Converter.class
                        .getName()).log(Level.SEVERE, null, ex);
            } catch (OutOfMemoryError ex) {
                Logger.getLogger(HDF5Converter.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
          
            Date d = new Date();
            //System.currentTimeMillis();
            /*Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.YEAR, -66);
            m.setTimedate(cal.getTime());*/
            m.setTimedate(d);
            datasetdb.getMeasurementCollection().add(m);
              try {
                Thread.sleep(1L);
            } catch (InterruptedException ex) {
                Logger.getLogger(HDF5Converter.class.getName()).log(Level.SEVERE, null, ex);
            }
            //measus.add(m);
            /*if (i % 500 == 0) {
             session.flush();
             session.clear();
             }
             if (i % 10000 == 0 && i != 0) {
             System.out.println("reached 10 000!");
             }
             }
             //session.flush();
             //session.clear();
             //tx.commit();*/
        }
        DatasetToDbTask dtTask = dtTaskFactory.createTask();
        dtTask.obj.add(datasetdb);
        //taskService.execute(dtTask);
        dtTask.run();
        //datasetDao.save(datasetdb);
        
        //dbService.execute(dtTask);
        //Session s = sessionFactory.openSession();
        //Transaction tx = s.beginTransaction();
        //datasetDao.save(datasetdb, s);
        //s.flush();
        //s.clear();
        //tx.commit();
        //s.close();
         count += tmp.length;
        //scenarios.clear();
        //ystem.out.println("Processed!");
        /*msTask.obj = (ArrayList<Measurement>) measus.clone();
         executorService.execute(msTask);
         scenarios.clear();
         datasets.clear();
         measus.clear();*/
    }
}
