/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author José Magalhães
 */
@Embeddable
public class ProducedPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "dataset_id")
    private long datasetId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "equipment_id")
    private String equipmentId;

    public ProducedPK() {
    }

    public ProducedPK(long datasetId, String equipmentId) {
        this.datasetId = datasetId;
        this.equipmentId = equipmentId;
    }

    public long getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(long datasetId) {
        this.datasetId = datasetId;
    }

    public String getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) datasetId;
        hash += (equipmentId != null ? equipmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProducedPK)) {
            return false;
        }
        ProducedPK other = (ProducedPK) object;
        if (this.datasetId != other.datasetId) {
            return false;
        }
        if ((this.equipmentId == null && other.equipmentId != null) || (this.equipmentId != null && !this.equipmentId.equals(other.equipmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.ProducedPK[ datasetId=" + datasetId + ", equipmentId=" + equipmentId + " ]";
    }
    
}
