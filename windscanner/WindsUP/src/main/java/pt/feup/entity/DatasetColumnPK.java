/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author José Magalhães
 */
@Embeddable
public class DatasetColumnPK implements Serializable {

    @JoinColumn(name = "ds_template_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private DatasetTemplate datasetTemplate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "column_pos")
    private short columnPos;

    public DatasetColumnPK() {
    }

    public DatasetTemplate getDatasetTemplate() {
        return datasetTemplate;
    }

    public void setDatasetTemplate(DatasetTemplate datasetTemplate) {
        this.datasetTemplate = datasetTemplate;
    }

    
    public short getColumnPos() {
        return columnPos;
    }

    public void setColumnPos(short columnPos) {
        this.columnPos = columnPos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.datasetTemplate);
        hash = 67 * hash + this.columnPos;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DatasetColumnPK other = (DatasetColumnPK) obj;
        if (!Objects.equals(this.datasetTemplate, other.datasetTemplate)) {
            return false;
        }
        if (this.columnPos != other.columnPos) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DatasetColumnPK{" + "datasetTemplate=" + datasetTemplate + ", columnPos=" + columnPos + '}';
    }

    

}
