/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "dataset_column")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatasetColumn.findAll", query = "SELECT d FROM DatasetColumn d"),
    @NamedQuery(name = "DatasetColumn.findByDsTemplateId", query = "SELECT d FROM DatasetColumn d WHERE d.datasetColumnPK.datasetTemplate.id = :dsTemplateId"),
    @NamedQuery(name = "DatasetColumn.findByColumnPos", query = "SELECT d FROM DatasetColumn d WHERE d.datasetColumnPK.columnPos = :columnPos"),
    @NamedQuery(name = "DatasetColumn.findByDisplayPos", query = "SELECT d FROM DatasetColumn d WHERE d.displayPos = :displayPos"),
    @NamedQuery(name = "DatasetColumn.findByShortName", query = "SELECT d FROM DatasetColumn d WHERE d.shortName = :shortName"),
    @NamedQuery(name = "DatasetColumn.findByLongName", query = "SELECT d FROM DatasetColumn d WHERE d.longName = :longName"),
    @NamedQuery(name = "DatasetColumn.findByUnit", query = "SELECT d FROM DatasetColumn d WHERE d.unit = :unit"),
    @NamedQuery(name = "DatasetColumn.findBySize", query = "SELECT d FROM DatasetColumn d WHERE d.size = :size"),
    @NamedQuery(name = "DatasetColumn.findByDataType", query = "SELECT d FROM DatasetColumn d WHERE d.dataType = :dataType")})
public class DatasetColumn implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DatasetColumnPK datasetColumnPK = new DatasetColumnPK();
    @Column(name = "display_pos")
    private Short displayPos;
    @Size(max = 50)
    @Column(name = "short_name")
    private String shortName;
    @Size(max = 50)
    @Column(name = "long_name")
    private String longName;
    @Size(max = 20)
    @Column(name = "unit")
    private String unit;
    @Column(name = "size")
    private Short size;
    @Column(name = "data_type")
    private Short dataType;

    public DatasetColumn() {
    }

    public DatasetColumnPK getDatasetColumnPK() {
        return datasetColumnPK;
    }

    public void setDatasetColumnPK(DatasetColumnPK datasetColumnPK) {
        this.datasetColumnPK = datasetColumnPK;
    }

    public Short getDisplayPos() {
        return displayPos;
    }

    public void setDisplayPos(Short displayPos) {
        this.displayPos = displayPos;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Short getSize() {
        return size;
    }

    public void setSize(Short size) {
        this.size = size;
    }

    public Short getDataType() {
        return dataType;
    }

    public void setDataType(Short dataType) {
        this.dataType = dataType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (datasetColumnPK != null ? datasetColumnPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatasetColumn)) {
            return false;
        }
        DatasetColumn other = (DatasetColumn) object;
        if ((this.datasetColumnPK == null && other.datasetColumnPK != null) || (this.datasetColumnPK != null && !this.datasetColumnPK.equals(other.datasetColumnPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.DatasetColumn[ datasetColumnPK=" + datasetColumnPK + " ]";
    }
    
}
