/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "campaign_document")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CampaignDocument.findAll", query = "SELECT c FROM CampaignDocument c"),
    @NamedQuery(name = "CampaignDocument.findById", query = "SELECT c FROM CampaignDocument c WHERE c.id = :id"),
    @NamedQuery(name = "CampaignDocument.findByType", query = "SELECT c FROM CampaignDocument c WHERE c.type = :type"),
    @NamedQuery(name = "CampaignDocument.findByName", query = "SELECT c FROM CampaignDocument c WHERE c.name = :name"),
    @NamedQuery(name = "CampaignDocument.findByCreatedAt", query = "SELECT c FROM CampaignDocument c WHERE c.createdAt = :createdAt"),
    @NamedQuery(name = "CampaignDocument.findByUpdatedAt", query = "SELECT c FROM CampaignDocument c WHERE c.updatedAt = :updatedAt"),
    @NamedQuery(name = "CampaignDocument.findByFilename", query = "SELECT c FROM CampaignDocument c WHERE c.filename = :filename"),
    @NamedQuery(name = "CampaignDocument.findByDescription", query = "SELECT c FROM CampaignDocument c WHERE c.description = :description")})
public class CampaignDocument implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Size(max = 254)
    @Column(name = "filename")
    private String filename;
    @Size(max = 100)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User ownerId;
    @JoinColumn(name = "m_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Scenario mId;
    @JoinColumn(name = "c_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Campaign cId;
    @OneToMany(mappedBy = "configId", fetch = FetchType.LAZY)
    private Collection<Dataset> datasetCollection;

    public CampaignDocument() {
    }

    public CampaignDocument(Long id) {
        this.id = id;
    }

    public CampaignDocument(Long id, String type, String name, Date createdAt, Date updatedAt) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    public Scenario getMId() {
        return mId;
    }

    public void setMId(Scenario mId) {
        this.mId = mId;
    }

    public Campaign getCId() {
        return cId;
    }

    public void setCId(Campaign cId) {
        this.cId = cId;
    }

    @XmlTransient
    public Collection<Dataset> getDatasetCollection() {
        return datasetCollection;
    }

    public void setDatasetCollection(Collection<Dataset> datasetCollection) {
        this.datasetCollection = datasetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampaignDocument)) {
            return false;
        }
        CampaignDocument other = (CampaignDocument) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.CampaignDocument[ id=" + id + " ]";
    }
    
}
