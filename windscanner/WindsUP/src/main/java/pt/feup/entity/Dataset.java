/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "dataset")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dataset.findAll", query = "SELECT d FROM Dataset d"),
    @NamedQuery(name = "Dataset.findById", query = "SELECT d FROM Dataset d WHERE d.id = :id"),
    @NamedQuery(name = "Dataset.findByName", query = "SELECT d FROM Dataset d WHERE d.name = :name"),
    @NamedQuery(name = "Dataset.findByCreatedAt", query = "SELECT d FROM Dataset d WHERE d.createdAt = :createdAt"),
    @NamedQuery(name = "Dataset.findByUpdatedAt", query = "SELECT d FROM Dataset d WHERE d.updatedAt = :updatedAt"),
    @NamedQuery(name = "Dataset.findByComplete", query = "SELECT d FROM Dataset d WHERE d.complete = :complete"),
    @NamedQuery(name = "Dataset.findByPublished", query = "SELECT d FROM Dataset d WHERE d.published = :published"),
    @NamedQuery(name = "Dataset.findByDerived", query = "SELECT d FROM Dataset d WHERE d.derived = :derived")})
public class Dataset implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "complete")
    private boolean complete;
    @Basic(optional = false)
    @NotNull
    @Column(name = "published")
    private boolean published;
    @Basic(optional = false)
    @NotNull
    @Column(name = "derived")
    private boolean derived;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User ownerId;
    @JoinColumn(name = "scenario_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Scenario scenario;
    @JoinColumn(name = "ds_template_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private DatasetTemplate dsTemplateId;
    @JoinColumn(name = "config_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private CampaignDocument configId;
    @OneToMany(mappedBy = "ownerId", fetch = FetchType.LAZY)
    private Collection<Event> eventCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dataset", fetch = FetchType.EAGER)
    private Collection<Produced> producedCollection;
    @OneToMany(mappedBy = "dsId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<Measurement> measurementCollection;
    

    public Dataset() {
    }

    public Dataset(Long id) {
        this.id = id;
    }

    public Dataset(Long id, Date createdAt, Date updatedAt, boolean complete, boolean published, boolean derived) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.complete = complete;
        this.published = published;
        this.derived = derived;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean getComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public boolean getPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public boolean getDerived() {
        return derived;
    }

    public void setDerived(boolean derived) {
        this.derived = derived;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public DatasetTemplate getDsTemplateId() {
        return dsTemplateId;
    }

    public void setDsTemplateId(DatasetTemplate dsTemplateId) {
        this.dsTemplateId = dsTemplateId;
    }

    public CampaignDocument getConfigId() {
        return configId;
    }

    public void setConfigId(CampaignDocument configId) {
        this.configId = configId;
    }

    @XmlTransient
    public Collection<Event> getEventCollection() {
        return eventCollection;
    }

    public void setEventCollection(Collection<Event> eventCollection) {
        this.eventCollection = eventCollection;
    }

    @XmlTransient
    public Collection<Produced> getProducedCollection() {
        return producedCollection;
    }

    public void setProducedCollection(Collection<Produced> producedCollection) {
        this.producedCollection = producedCollection;
    }

    @XmlTransient
    public Collection<Measurement> getMeasurementCollection() {
        return measurementCollection;
    }

    public void setMeasurementCollection(Collection<Measurement> measurementCollection) {
        this.measurementCollection = measurementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dataset)) {
            return false;
        }
        Dataset other = (Dataset) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.Dataset[ id=" + id + " ]";
    }
    
}
