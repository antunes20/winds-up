/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "blob")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Blob.findAll", query = "SELECT b FROM Blob b"),
    @NamedQuery(name = "Blob.findById", query = "SELECT b FROM Blob b WHERE b.id = :id"),
    @NamedQuery(name = "Blob.findByGpsTime", query = "SELECT b FROM Blob b WHERE b.gpsTime = :gpsTime"),
    @NamedQuery(name = "Blob.findByName", query = "SELECT b FROM Blob b WHERE b.name = :name"),
    @NamedQuery(name = "Blob.findByPathname", query = "SELECT b FROM Blob b WHERE b.pathname = :pathname")})
public class Blob implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gps_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date gpsTime;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "pathname")
    private String pathname;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User ownerId;
    @JoinColumn(name = "sc_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Scenario scId;

    public Blob() {
    }

    public Blob(Long id) {
        this.id = id;
    }

    public Blob(Long id, Date gpsTime, String pathname) {
        this.id = id;
        this.gpsTime = gpsTime;
        this.pathname = pathname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGpsTime() {
        return gpsTime;
    }

    public void setGpsTime(Date gpsTime) {
        this.gpsTime = gpsTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathname() {
        return pathname;
    }

    public void setPathname(String pathname) {
        this.pathname = pathname;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    public Scenario getScId() {
        return scId;
    }

    public void setScId(Scenario scId) {
        this.scId = scId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Blob)) {
            return false;
        }
        Blob other = (Blob) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.Blob[ id=" + id + " ]";
    }
    
}
