/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "scenario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Scenario.findAll", query = "SELECT s FROM Scenario s"),
    @NamedQuery(name = "Scenario.findById", query = "SELECT s FROM Scenario s WHERE s.id = :id"),
    @NamedQuery(name = "Scenario.findByName", query = "SELECT s FROM Scenario s WHERE s.name = :name"),
    @NamedQuery(name = "Scenario.findByCreatedAt", query = "SELECT s FROM Scenario s WHERE s.createdAt = :createdAt"),
    @NamedQuery(name = "Scenario.findByUpdatedAt", query = "SELECT s FROM Scenario s WHERE s.updatedAt = :updatedAt")})
public class Scenario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "timestamp_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampFrom;
    @Column(name = "timestamp_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampTo;
    @OneToMany(mappedBy = "scId", fetch = FetchType.LAZY)
    private Collection<Blob> blobCollection;
    @OneToMany(mappedBy = "mId", fetch = FetchType.LAZY)
    private Collection<CampaignDocument> campaignDocumentCollection;
    @JoinColumn(name = "c_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Campaign campaign;
    @OneToMany(mappedBy = "scenario", fetch = FetchType.LAZY)
    private Collection<Dataset> datasetCollection;

    public Scenario() {
    }

    public Scenario(Long id) {
        this.id = id;
    }

    public Scenario(Long id, Date createdAt, Date updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getTimestampFrom() {
        return timestampFrom;
    }

    public void setTimestampFrom(Date timestampFrom) {
        this.timestampFrom = timestampFrom;
    }

    public Date getTimestampTo() {
        return timestampTo;
    }

    public void setTimestampTo(Date timestampTo) {
        this.timestampTo = timestampTo;
    }
    
    @XmlTransient
    public Collection<Blob> getBlobCollection() {
        return blobCollection;
    }

    public void setBlobCollection(Collection<Blob> blobCollection) {
        this.blobCollection = blobCollection;
    }

    @XmlTransient
    public Collection<CampaignDocument> getCampaignDocumentCollection() {
        return campaignDocumentCollection;
    }

    public void setCampaignDocumentCollection(Collection<CampaignDocument> campaignDocumentCollection) {
        this.campaignDocumentCollection = campaignDocumentCollection;
    }

    public Campaign getCampaign() {
        return campaign;
    }

    public void setCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    @XmlTransient
    public Collection<Dataset> getDatasetCollection() {
        return datasetCollection;
    }

    public void setDatasetCollection(Collection<Dataset> datasetCollection) {
        this.datasetCollection = datasetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Scenario)) {
            return false;
        }
        Scenario other = (Scenario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.Scenario[ id=" + id + " ]";
    }

}
