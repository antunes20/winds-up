/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "produced")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produced.findAll", query = "SELECT p FROM Produced p"),
    @NamedQuery(name = "Produced.findByDatasetId", query = "SELECT p FROM Produced p WHERE p.producedPK.datasetId = :datasetId"),
    @NamedQuery(name = "Produced.findByEquipmentId", query = "SELECT p FROM Produced p WHERE p.producedPK.equipmentId = :equipmentId"),
    @NamedQuery(name = "Produced.findByStartDate", query = "SELECT p FROM Produced p WHERE p.startDate = :startDate"),
    @NamedQuery(name = "Produced.findByEndDate", query = "SELECT p FROM Produced p WHERE p.endDate = :endDate"),
    @NamedQuery(name = "Produced.findByHeight", query = "SELECT p FROM Produced p WHERE p.height = :height"),
    @NamedQuery(name = "Produced.findByAzimuthAngle", query = "SELECT p FROM Produced p WHERE p.azimuthAngle = :azimuthAngle")})
public class Produced implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProducedPK producedPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "height")
    private Float height;
    @Column(name = "azimuth_angle")
    private Float azimuthAngle;
    @JoinColumn(name = "station_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Station stationId;
    @JoinColumn(name = "equipment_id", referencedColumnName = "serial_number", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Equipment equipment;
    @JoinColumn(name = "dataset_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Dataset dataset;

    public Produced() {
    }

    public Produced(ProducedPK producedPK) {
        this.producedPK = producedPK;
    }

    public Produced(ProducedPK producedPK, Date startDate, Date endDate) {
        this.producedPK = producedPK;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Produced(long datasetId, String equipmentId) {
        this.producedPK = new ProducedPK(datasetId, equipmentId);
    }

    public ProducedPK getProducedPK() {
        return producedPK;
    }

    public void setProducedPK(ProducedPK producedPK) {
        this.producedPK = producedPK;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getAzimuthAngle() {
        return azimuthAngle;
    }

    public void setAzimuthAngle(Float azimuthAngle) {
        this.azimuthAngle = azimuthAngle;
    }

    public Station getStationId() {
        return stationId;
    }

    public void setStationId(Station stationId) {
        this.stationId = stationId;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (producedPK != null ? producedPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produced)) {
            return false;
        }
        Produced other = (Produced) object;
        if ((this.producedPK == null && other.producedPK != null) || (this.producedPK != null && !this.producedPK.equals(other.producedPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.Produced[ producedPK=" + producedPK + " ]";
    }
    
}
