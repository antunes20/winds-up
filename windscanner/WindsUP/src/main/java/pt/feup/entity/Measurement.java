/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Type;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "measurement")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Measurement.findAll", query = "SELECT m FROM Measurement m"),
    @NamedQuery(name = "Measurement.findById", query = "SELECT m FROM Measurement m WHERE m.id = :id"),
    @NamedQuery(name = "Measurement.findByTimedate", query = "SELECT m FROM Measurement m WHERE m.timedate = :timedate"),
    @NamedQuery(name = "Measurement.findByData", query = "SELECT m FROM Measurement m WHERE m.data = :data"),
    @NamedQuery(name = "Measurement.findByDataInt", query = "SELECT m FROM Measurement m WHERE m.dataInt = :dataInt")})
public class Measurement implements Serializable {
   private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "timedate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timedate;
    @Type(type = "pt.feup.hibernate.NumericArrayUserType")
    @Column(name = "data")
    private BigDecimal[] data;
    @Type(type = "pt.feup.hibernate.IntArrayUserType")
    @Column(name = "data_int")
    private Integer[] dataInt;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ds_id", referencedColumnName = "id")
    private Dataset dsId;

    public Measurement() {
    }

    public Measurement(Long id) {
        this.id = id;
    }

    public Measurement(Long id, Date timedate) {
        this.id = id;
        this.timedate = timedate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimedate() {
        return timedate;
    }

    public void setTimedate(Date timedate) {
        this.timedate = timedate;
    }

    public BigDecimal[] getData() {
        return data;
    }

    public void setData(BigDecimal[] data) {
        this.data = data;
    }

    public Integer[] getDataInt() {
        return dataInt;
    }

    public void setDataInt(Integer[] dataInt) {
        this.dataInt = dataInt;
    }
    
    @JsonIgnore
    public Dataset getDsId() {
        return dsId;
    }
    
    @JsonIgnore
    public void setDsId(Dataset dsId) {
        this.dsId = dsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Measurement)) {
            return false;
        }
        Measurement other = (Measurement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.Measurement[ id=" + id + " ]";
    }
    
}
