/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "campaign")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Campaign.findAll", query = "SELECT c FROM Campaign c"),
    @NamedQuery(name = "Campaign.findById", query = "SELECT c FROM Campaign c WHERE c.id = :id"),
    @NamedQuery(name = "Campaign.findByName", query = "SELECT c FROM Campaign c WHERE c.name = :name"),
    @NamedQuery(name = "Campaign.findByCreatedAt", query = "SELECT c FROM Campaign c WHERE c.createdAt = :createdAt"),
    @NamedQuery(name = "Campaign.findByUpdatedAt", query = "SELECT c FROM Campaign c WHERE c.updatedAt = :updatedAt"),
    @NamedQuery(name = "Campaign.findByTimestampFrom", query = "SELECT c FROM Campaign c WHERE c.timestampFrom = :timestampFrom"),
    @NamedQuery(name = "Campaign.findByTimestampTo", query = "SELECT c FROM Campaign c WHERE c.timestampTo = :timestampTo")})
public class Campaign implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name = "timestamp_from")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampFrom;
    @Column(name = "timestamp_to")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampTo;
    @OneToMany(mappedBy = "cId", fetch = FetchType.LAZY)
    private Collection<CampaignDocument> campaignDocumentCollection;
    @OneToMany(mappedBy = "campaign", fetch = FetchType.LAZY)
    private Collection<Scenario> scenarioCollection;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User ownerId;
    @JoinColumn(name = "l_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Site site;

    public Campaign() {
    }

    public Campaign(Long id) {
        this.id = id;
    }

    public Campaign(Long id, Date createdAt, Date updatedAt) {
        this.id = id;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getTimestampFrom() {
        return timestampFrom;
    }

    public void setTimestampFrom(Date timestampFrom) {
        this.timestampFrom = timestampFrom;
    }

    public Date getTimestampTo() {
        return timestampTo;
    }

    public void setTimestampTo(Date timestampTo) {
        this.timestampTo = timestampTo;
    }

    @XmlTransient
    public Collection<CampaignDocument> getCampaignDocumentCollection() {
        return campaignDocumentCollection;
    }

    public void setCampaignDocumentCollection(Collection<CampaignDocument> campaignDocumentCollection) {
        this.campaignDocumentCollection = campaignDocumentCollection;
    }

    @XmlTransient
    public Collection<Scenario> getScenarioCollection() {
        return scenarioCollection;
    }

    public void setScenarioCollection(Collection<Scenario> scenarioCollection) {
        this.scenarioCollection = scenarioCollection;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site lId) {
        this.site = lId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Campaign)) {
            return false;
        }
        Campaign other = (Campaign) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.Campaign[ id=" + id + " ]";
    }
    
}
