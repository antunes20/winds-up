package pt.feup.entity;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

/**
 * This class extends the Spring Security User class
 * for interaction with Spring Security.
 */
public class UserAuth extends org.springframework.security.core.userdetails.User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4644188391182497904L;

	private Long id;

	public UserAuth(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
	}

	public UserAuth(Long id, String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserAuth [id=" + id + ", getUsername()=" + getUsername()
				+ ", isEnabled()=" + isEnabled() + ", isAccountNonExpired()="
				+ isAccountNonExpired() + "]";
	}

	/**
	 * @return the user id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
