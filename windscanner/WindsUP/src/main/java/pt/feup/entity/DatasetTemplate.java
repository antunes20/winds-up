/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author José Magalhães
 */
@Entity
@Table(name = "dataset_template")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatasetTemplate.findAll", query = "SELECT d FROM DatasetTemplate d"),
    @NamedQuery(name = "DatasetTemplate.findById", query = "SELECT d FROM DatasetTemplate d WHERE d.id = :id"),
    @NamedQuery(name = "DatasetTemplate.findByName", query = "SELECT d FROM DatasetTemplate d WHERE d.name = :name"),
    @NamedQuery(name = "DatasetTemplate.findByDescription", query = "SELECT d FROM DatasetTemplate d WHERE d.description = :description"),
    @NamedQuery(name = "DatasetTemplate.findByCreatedAt", query = "SELECT d FROM DatasetTemplate d WHERE d.createdAt = :createdAt"),
    @NamedQuery(name = "DatasetTemplate.findByUpdatedAt", query = "SELECT d FROM DatasetTemplate d WHERE d.updatedAt = :updatedAt")})
public class DatasetTemplate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "datasetColumnPK.datasetTemplate", fetch = FetchType.LAZY)
    private Collection<DatasetColumn> datasetColumnCollection;
    @OneToMany(mappedBy = "dsTemplateId", fetch = FetchType.LAZY)
    private Collection<Dataset> datasetCollection;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User ownerId;

    public DatasetTemplate() {
    }

    public DatasetTemplate(Long id) {
        this.id = id;
    }

    public DatasetTemplate(Long id, String name, Date createdAt, Date updatedAt) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @XmlTransient
    public Collection<DatasetColumn> getDatasetColumnCollection() {
        return datasetColumnCollection;
    }

    public void setDatasetColumnCollection(Collection<DatasetColumn> datasetColumnCollection) {
        this.datasetColumnCollection = datasetColumnCollection;
    }

    @XmlTransient
    public Collection<Dataset> getDatasetCollection() {
        return datasetCollection;
    }

    public void setDatasetCollection(Collection<Dataset> datasetCollection) {
        this.datasetCollection = datasetCollection;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatasetTemplate)) {
            return false;
        }
        DatasetTemplate other = (DatasetTemplate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.feup.entity.DatasetTemplate[ id=" + id + " ]";
    }
    
}
