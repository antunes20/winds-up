package pt.feup.security;

import java.util.ArrayList;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.UserDao;
import pt.feup.entity.SecurityRole;

@Service("AuthenticationService")
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private UserDao dao;

	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		pt.feup.entity.User user = dao.findByName(username);
		if (user == null)
			throw new UsernameNotFoundException("user not found");
		return buildUserFromUserEntity(user);
	}
        
        private User buildUserFromUserEntity(pt.feup.entity.User user ){
            String username = user.getUsername();
		String password = user.getPassword();
		boolean enabled = user.isActive();
		boolean accountNonExpired = user.isActive();
		boolean credentialsNonExpired = user.isActive();
		boolean accountNonLocked = user.isActive();
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		for (SecurityRole role : user.getSecurityRoleCollection()) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		pt.feup.entity.UserAuth newUser = new pt.feup.entity.UserAuth(user.getId(),
				username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		return newUser;
        }
}
