package pt.feup.dao;

import java.util.Collection;

import org.springframework.stereotype.Component;

import pt.feup.entity.DatasetTemplate;

@Component
public interface DSTemplateDao extends AbstractDao<DatasetTemplate, Long> {

	Collection<DatasetTemplate> getByNameMatch(String query);
        Collection<DatasetTemplate> getAllDSTemplates();
	Collection<DatasetTemplate> getDSTemplates(Long ownerId, int firstResult,
			int pageSize);
	Integer getDSTemplatesCount(Long ownerId);

}
