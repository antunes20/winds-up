package pt.feup.dao;

import java.util.List;
import org.hibernate.criterion.Order;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pt.feup.dao.DSColumnDao;
import pt.feup.entity.DatasetColumn;
import pt.feup.entity.DatasetTemplate;

@Repository
public class DSColumnDaoImpl extends AbstractDaoImpl<DatasetColumn, Long>
implements DSColumnDao {

	protected DSColumnDaoImpl() {
		super(DatasetColumn.class);
	}

	@Override
	public List<DatasetColumn> getDSColumnsByDSTId(DatasetTemplate dst) {
		return findByCriteria(Order.asc("displayPos"), Restrictions.eq("datasetColumnPK.datasetTemplate", dst));
	}
}
