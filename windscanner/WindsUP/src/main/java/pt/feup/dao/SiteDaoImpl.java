/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.dao;

import java.util.Collection;
import org.springframework.stereotype.Repository;
import pt.feup.entity.Site;

/**
 *
 * @author José
 */
@Repository
public class SiteDaoImpl extends AbstractDaoImpl<Site, Long> implements SiteDao {

    public SiteDaoImpl() {
        super(Site.class);
    }

    @Override
    public Collection<Site> getAllSites() {
         return findByCriteria(null);
    }
    
}
