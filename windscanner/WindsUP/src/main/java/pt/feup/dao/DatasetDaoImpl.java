package pt.feup.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pt.feup.dao.DatasetDao;
import pt.feup.entity.Dataset;
import pt.feup.entity.Measurement;

@Repository
public class DatasetDaoImpl extends AbstractDaoImpl<Dataset, Long>
implements DatasetDao {

	protected DatasetDaoImpl() {
		super(Dataset.class);
	}

	@Override
	public void saveDataset(Dataset ds) {
		saveOrUpdate(ds);
	}

	@Override
	public Collection<Measurement> getMeasurementsByDSId(Long dsId) {
		return findById(dsId).getMeasurementCollection();    
	}

	@Override
	public List<Dataset> getMeasurementsByDSId(Long dsId, Date from,
			Date to) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Dataset getDatasetById(Long dsId) {
		List<Dataset> list = findByCriteria(Restrictions.eq("id", dsId));
		if ( list.size() > 0 )
			return list.get(0);
		else
			return null;
	}

	@Override
	public List<Dataset> getDatasetsByOwner(Long ownerId) {
		// TODO: rever melhor forma de fazer uma query por uma foreign key
		List<Dataset> list = findByCriteria(Restrictions.eq("owner.userId", ownerId));
		return list;
	}

	/*@Override
	public boolean datasetDataBulkInsert(Long dsId,
			Collection<? extends DatasetDataItem> data) {
		Session s = null;
		//Transaction t = null;

		try {
			s = getCurrentSession();
			if (!s.isConnected())
				throw new RuntimeException("!connected");
			if (!s.isOpen())
				throw new RuntimeException("!open");
			if (dsId == null)
				throw new RuntimeException("dsId null");
			int count = data.size();
			int n = 0;
			for (DatasetDataItem m : data) {
				n++;
				if (n % 10 == 1)
					;//t = s.beginTransaction();
				if (m == null)
					throw new RuntimeException("m null");
				m.setDSId(dsId);
				s.saveOrUpdate(m);
				if (n % 10 == 0 || n == count)
					{s.flush();s.clear();}//t.commit();
			}
		} catch(RuntimeException e){
			try {
				;//t.rollback();
			} catch(RuntimeException rbe) {
				//log.error("Couldnâ€™t roll back transaction", rbe);
			}
			throw e;
		} finally {

			if(s != null){
				//s.close();
			}
		}
		return true;
	}*/

	@Override
	public List<Dataset> getDatasetsByParent(Long cId) {
		return findByCriteria(Restrictions.eq("parentId", cId));
	}

	@Override
	public Collection<Dataset> getByNameMatch(String query) {
		return findByCriteria(Restrictions.and(
				Restrictions.eq("published", true),
				Restrictions.ilike("name", "%"+query+"%")));
	}

	@Override
	public Long getLastDatasetId() {
		/*List<Dataset> list = findByCriteria(Order.desc("dsId"), 0, 1);
		if (list != null && list.size() > 0)
			return list.get(0).getDsId();
		else*/
			return (long) -1;
	}
}
