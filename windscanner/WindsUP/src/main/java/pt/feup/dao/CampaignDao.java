package pt.feup.dao;

import java.util.Collection;
import java.util.List;
import org.springframework.stereotype.Component;
import pt.feup.entity.Campaign;
import pt.feup.entity.Site;

/**
 * Campaign DAO.
 */
@Component
public interface CampaignDao extends AbstractDao<Campaign, Long> {
	
	Campaign getCampaignById(Long cId);
	void saveCampaign(Campaign m);

	List<Campaign> getCampaignsByOwner(Long ownerId);
	
	/**
	 * Inserts records into the database, in batches
	 * 
	 * @param cId
	 * @param data
	 * @return whether the operation was successful.
	 */
	//boolean campaignDataBulkInsert(Long cId, Collection<? extends CampaignDataItem> data);
	List<Campaign> findByCampaignId(Long cId);
	Collection<Campaign> getLastCampaigns(int pageSize);
        
       Collection<Campaign> getAllCampaigns();
       
       Site getSiteById(Long cId);
}
