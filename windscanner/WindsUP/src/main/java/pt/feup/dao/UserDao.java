package pt.feup.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import pt.feup.entity.SecurityRole;
import pt.feup.entity.User;


public interface UserDao extends AbstractDao<User, Long> {

	public void addUser(User user);
	public User findByName(String username);
	public User getUserByID(Long id);
	public String activateUser(Long id);
	public String disableUser(Long id);
	public List<User> listUser();
	public void removeUser(Long id);
	public Set<SecurityRole> getSecurityRolesForUsername(String username);

	void saveUser(User user);
	List<User> findUsers(String username);

	/**
	 * Returns the list of users with a given role
	 * @param role role name; null for users with no roles
	 * @return
	 */
	Collection<User> getUsersWithRole(String role);
}
