package pt.feup.dao;

import java.util.Collection;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pt.feup.entity.DatasetTemplate;

@Repository
public class DSTemplateDaoImpl extends AbstractDaoImpl<DatasetTemplate, Long>
implements DSTemplateDao {

	protected DSTemplateDaoImpl() {
		super(DatasetTemplate.class);
	}

	@Override
	public Collection<DatasetTemplate> getByNameMatch(String query) {
		return findByCriteria(Restrictions.ilike("name", "%"+query+"%"));
	}

	@Override
	public Collection<DatasetTemplate> getDSTemplates(Long ownerId, int firstResult,
			int pageSize) {
		if ( ownerId == null)
			//return this.findByCriteria(Order.asc("updated_at"));
			return findByCriteria(Order.asc("updated_at"), firstResult, pageSize,
					Restrictions.isNotNull("name"));
		else
			return findByCriteria(Order.asc("updated_at"), firstResult, pageSize,
					Restrictions.eq("owner.userId", ownerId));
	}

	@Override
	public Integer getDSTemplatesCount(Long ownerId) {
		if ( ownerId == null)
			return this.findByCriteriaCount();
		else
			return this.findByCriteriaCount(Restrictions.eq("owner.userId", ownerId));
	}
        
        @Override
       public Collection<DatasetTemplate> getAllDSTemplates(){
            return findByCriteria(null);
        }

}
