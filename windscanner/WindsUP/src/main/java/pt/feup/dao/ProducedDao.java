/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.dao;

import pt.feup.entity.Produced;
import pt.feup.entity.ProducedPK;

/**
 *
 * @author José
 */
public interface ProducedDao extends AbstractDao<Produced, ProducedPK> {
    
}
