package pt.feup.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import pt.feup.entity.SecurityRole;
import pt.feup.entity.User;
import pt.feup.exception.BadRequestException;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, Long> implements UserDao {

	public UserDaoImpl() {
		super(User.class);
                
	}

	@Override
	public void addUser(User user) {
		String errMsg = "";
		if ( user.getUsername() == null || user.getUsername().length() < 4 )
			errMsg += "username,";
		if ( user.getPassword() == null || user.getPassword().length() < 4 )
			errMsg += "password,";
		if (user.getFirstName() == null || user.getFirstName().length() < 3 )
			errMsg += "first name,";
		if ( user.getFamilyName() == null || user.getFamilyName().length() < 3 )
			errMsg += "family name,";
		if ( user.getEmail() == null || user.getEmail().length() < 4 )
			errMsg += "email,";
		if ( errMsg.length() > 0 )
			throw new BadRequestException(errMsg.substring(0, errMsg.length() - 1));
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setPassword( passwordEncoder.encode(user.getPassword()) );

		try {
			save(user);
		} catch (ConstraintViolationException e) {
			if (e.getCause().getMessage().contains("duplicate key value")) {
				String msg =  e.getCause().getMessage().substring(
						e.getCause().getMessage().indexOf("Detail: ") + 8);
				throw new BadRequestException(msg);
			}
		}
	}

	@Override
	public User findByName(String username) {
		List<User> list = findByCriteria(Restrictions.eq("username", username));
		if ( list.isEmpty() )
			return null;
		else
			return list.get(0);
	}

	@Override
	public User getUserByID(Long id) {
		User user = (User) getCurrentSession().createQuery(
				"select u from User u where u.id = '" + id + "'").uniqueResult();
		return user;
	}

	@Override
	public String activateUser(Long id) {
		String hql = "update Userset active = :active where id = :id";
		org.hibernate.Query query = getCurrentSession().createQuery(hql);
		query.setString("active","Y");
		query.setLong("user_id",id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		return "";
	}

	@Override
	public String disableUser(Long id) {
		String hql = "update users set active = :active where user_id = :id";
		org.hibernate.Query query = getCurrentSession().createQuery(hql);
		query.setInteger("active",0);
		query.setLong("user_id",id);
		int rowCount = query.executeUpdate();
		System.out.println("Rows affected: " + rowCount);
		return "";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUser() {
		return getCurrentSession().createQuery("from users")
				.list();
	}

	@Override
	public void removeUser(Long id) {
		User user = (User) getCurrentSession().load(
				User.class, id);
		if (null != user) {
			getCurrentSession().delete(user);
		}
	}

	@Override
	public Set<SecurityRole> getSecurityRolesForUsername(String username) {
		User user = (User) getCurrentSession().createQuery(
				"select u from users u where u.username = '" + username + "'").uniqueResult();
		if (user!= null) {
			Set<SecurityRole> roles = (Set<SecurityRole>) user.getSecurityRoleCollection();
			if (roles != null && roles.size() > 0) {
				return roles;
			}
		}
		return null;
	}

	@Override
	public void saveUser(User user) {
		saveOrUpdate(user);
	}

	@Override
	public List<User> findUsers(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<User> getUsersWithRole(String role) {
		final String noRolesHql =
				"SELECT u FROM User u WHERE u.active = TRUE AND u.securityRoleCollection IS EMPTY";
		final String hql = "select u FROM User u join u.securityRoleCollection sroles "
				+ "where u.active = TRUE and sroles.name = :filterrole";
		if ( role == null || role.length() < 6)
			return (List<User>)getCurrentSession().createQuery(noRolesHql).list();
		else
			return (List<User>)getCurrentSession().createQuery(hql).setString("filterrole", role).list();
	}

}
