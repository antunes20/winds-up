package pt.feup.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import pt.feup.entity.DatasetColumn;
import pt.feup.entity.DatasetTemplate;

@Component
public interface DSColumnDao extends AbstractDao<DatasetColumn, Long> {

	List<DatasetColumn> getDSColumnsByDSTId(DatasetTemplate dst);

}
