package pt.feup.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import pt.feup.entity.Measurement;

@Component
public interface MeasurementDao extends AbstractDao<Measurement, Long> {

    void saveMeasurement(Measurement m);

    List<Measurement> getMeasurementsByDSId(Long dsId);

    List<Measurement> getMeasurementsByDSId(Long dsId, String order, String orderBy, int firstResult, int maxResults);

    List<Measurement> getMeasurementsByDSId(Long dsId, int firstResult, int maxResults);

    List<Measurement> getMeasurementsByDSId(Long dsId, Date from, Date to);

    List<Measurement> getMeasurementsByDSId(Long dsId, Date from, Date to,
            int firstResult, int maxResults);

    List<Measurement> getMeasurementsByDSId(Long dsId, String order, String orderBy, int firstResult, int maxResults, Date from,
            Date to);

    Integer getMeasurementsByDSIdCount(Long dsId);
    Integer getMeasurementsByDSIdCount(Long dsId, Date from, Date to);
}
