/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.dao;

import java.util.Collection;
import pt.feup.entity.Site;

/**
 *
 * @author José
 */
public interface SiteDao extends AbstractDao<Site, Long> {


    Collection<Site> getAllSites();
}
