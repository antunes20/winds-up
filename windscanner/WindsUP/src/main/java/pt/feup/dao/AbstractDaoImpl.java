package pt.feup.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractDaoImpl<E, I extends Serializable> implements AbstractDao<E, I> {

    private Class<E> entityClass;

    protected AbstractDaoImpl(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    @Autowired
    private SessionFactory sessionFactory;

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    @Override
    public E findById(I id) {
        return (E) getCurrentSession().get(entityClass, id);
    }

    @Override
    public void save(E e) {
        getCurrentSession().save(e);
        //flush();
        //clear();    
    }

    @Override
    public void saveOrUpdate(E e) {
        getCurrentSession().saveOrUpdate(e);
    }

    @Override
    public void delete(E e) {
        getCurrentSession().delete(e);
    }

    @Override
    public void flush() {
        getCurrentSession().flush();
    }

    @Override
    public void clear() {
        getCurrentSession().clear();
    }

    @Override
    public List<E> findByCriteria(Criterion criterion) {
        return findByCriteria(null, 0, 0, criterion);
    }

    @Override
    public List<E> findByCriteria(Order order, Criterion... criterion) {
        return findByCriteria(order, 0, 0, criterion);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<E> findByCriteria(Order order, int firstResult, int maxResults, Criterion... criterion) {
        Criteria criteria = getCriteria(order, firstResult, maxResults, criterion);
        return criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }

    @Override
    public Integer findByCriteriaCount(Criterion... criterion) {
        return findByCriteriaCount(0, 0, criterion);
    }

    @Override
    public Integer findByCriteriaCount(int firstResult, int maxResults, Criterion... criterion) {
        Criteria criteria = getCriteria(null, firstResult, maxResults, criterion);
        return ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    private Criteria getCriteria(Order order, int firstResult, int maxResults, Criterion... criterion) {
        Criteria criteria = getCurrentSession().createCriteria(entityClass);
        if (criterion[0] != null) {
            for (Criterion c : criterion) {
                criteria.add(c);
            }
        }
        if (order != null) {
            criteria.addOrder(order);
        }
        if (maxResults > 0) {
            criteria.setFirstResult(firstResult);
            criteria.setMaxResults(maxResults);
        }
        return criteria;
    }

    @Override
    public void bulkInsert(ArrayList<E> objs, Session s) {
        for (E obj : objs) {
            s.save(obj);
        }
    }

    @Override
    public void save(E obj, Session s) {
        s.save(obj);
    }
}
