package pt.feup.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Component;
import pt.feup.entity.Dataset;
import pt.feup.entity.Measurement;

@Component
public interface DatasetDao extends AbstractDao<Dataset, Long> {

	Dataset getDatasetById(Long dsId);
	void saveDataset(Dataset ds);
	Collection<Measurement> getMeasurementsByDSId(Long dsId);
	List<Dataset> getMeasurementsByDSId(Long dsId, Date from, Date to);
	List<Dataset> getDatasetsByOwner(Long ownerId);

	/**
	 * Inserts records into the database, in batches
	 * 
	 * @param dsId
	 * @param data
	 * @return whether the operation was successful.
	 */
	//boolean datasetDataBulkInsert(Long dsId, Collection<? extends DatasetDataItem> data);
	List<Dataset> getDatasetsByParent(Long cId);
	Collection<Dataset> getByNameMatch(String query);

	Long getLastDatasetId();

}
