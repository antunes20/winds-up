/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.feup.dao;

import pt.feup.entity.Scenario;

/**
 *
 * @author José Magalhães
 */
public interface ScenarioDao extends  AbstractDao<Scenario, Long>  {
    
    void saveScenario(Scenario m);
}
