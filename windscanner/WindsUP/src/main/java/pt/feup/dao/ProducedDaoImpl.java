/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.dao;

import org.springframework.stereotype.Repository;
import pt.feup.entity.Produced;
import pt.feup.entity.ProducedPK;

/**
 *
 * @author José
 */
@Repository
public class ProducedDaoImpl extends AbstractDaoImpl<Produced, ProducedPK> implements ProducedDao {

    public ProducedDaoImpl() {
        super(Produced.class);
    }
    
}
