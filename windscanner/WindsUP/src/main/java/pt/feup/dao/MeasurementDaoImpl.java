package pt.feup.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pt.feup.entity.Measurement;

@Repository
public class MeasurementDaoImpl extends AbstractDaoImpl<Measurement, Long> implements MeasurementDao {

	protected MeasurementDaoImpl() {
		super(Measurement.class);
	}

	@Override
	public void saveMeasurement(Measurement m) {
		saveOrUpdate(m);
	}

	@Override
	public List<Measurement> getMeasurementsByDSId(Long dsId) {
		return findByCriteria(Order.asc("timedate"), Restrictions.eq("dsId.id", dsId));
	}
        
        @Override
        public List<Measurement> getMeasurementsByDSId(Long dsId, String order, String orderBy, int firstResult, int maxResults) {
		return ((order.equals("asc")) ? findByCriteria(Order.asc(orderBy), firstResult, maxResults, Restrictions.eq("dsId.id", dsId)) 
                        : findByCriteria(Order.desc(orderBy), firstResult, maxResults, Restrictions.eq("dsId.id", dsId)));
	}
        
        @Override
        public List<Measurement> getMeasurementsByDSId(Long dsId, String order, String orderBy, int firstResult, int maxResults, Date from,
			Date to) {
		return ((order.equals("asc")) ? findByCriteria(Order.asc(orderBy), firstResult, maxResults, Restrictions.eq("dsId.id", dsId), 
                        Restrictions.ge("timedate", from), Restrictions.le("timedate", to)) 
                        : findByCriteria(Order.desc(orderBy), firstResult, maxResults, Restrictions.eq("dsId.id", dsId), 
                                Restrictions.ge("timedate", from), Restrictions.le("timedate", to)));
	}

	@Override
	public List<Measurement> getMeasurementsByDSId(Long dsId, Date from,
			Date to) {
		return findByCriteria(Order.asc("timedate"), Restrictions.eq("dsId.id", dsId),
				Restrictions.ge("timedate", from), Restrictions.le("timedate", to));
	}

	@Override
	public List<Measurement> getMeasurementsByDSId(Long dsId,
			int firstResult, int maxResults) {
		return findByCriteria(Order.asc("timedate"), firstResult, maxResults,
				Restrictions.eq("dsId.id", dsId));
	}

	@Override
	public List<Measurement> getMeasurementsByDSId(Long dsId, Date from,
			Date to, int firstResult, int maxResults) {
		if ( from == null )
			return findByCriteria(Order.asc("id.timedate"), firstResult, maxResults, Restrictions.eq("id.dsId", dsId),
					Restrictions.le("id.timedate", to));
		else if ( to == null)
			return findByCriteria(Order.asc("id.timedate"), firstResult, maxResults, Restrictions.eq("id.dsId", dsId),
					Restrictions.ge("id.timedate", from));
		else
			return findByCriteria(Order.asc("id.timedate"), firstResult, maxResults, Restrictions.eq("id.dsId", dsId),
				Restrictions.ge("id.timedate", from), Restrictions.le("id.timedate", to));
	}

	@Override
	public Integer getMeasurementsByDSIdCount(Long dsId) {
		return findByCriteriaCount(Restrictions.eq("dsId.id", dsId));
	}

	@Override
	public Integer getMeasurementsByDSIdCount(Long dsId, Date from, Date to) {
		if ( from == null )
			return findByCriteriaCount(Restrictions.eq("dsId.id", dsId),
				Restrictions.le("timedate", to));
		else if ( to == null)
			return findByCriteriaCount(Restrictions.eq("dsId.id", dsId),
					Restrictions.ge("timedate", from));
		else
			return findByCriteriaCount(Restrictions.eq("dsId.id", dsId),
					Restrictions.ge("timedate", from), Restrictions.le("timedate", to));
	}

}
