/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.dao;

import org.springframework.stereotype.Repository;
import pt.feup.entity.Equipment;

/**
 *
 * @author José
 */
@Repository
public class EquipmentDaoImpl extends AbstractDaoImpl<Equipment, String> implements EquipmentDao{

    public EquipmentDaoImpl() {
        super(Equipment.class);
    }
    
}
