package pt.feup.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pt.feup.dao.CampaignDao;
import pt.feup.entity.Campaign;
import pt.feup.entity.Site;

@Repository
public class CampaignDaoImpl extends AbstractDaoImpl<Campaign, Long>
implements CampaignDao {

	protected CampaignDaoImpl() {
		super(Campaign.class);
	}

	@Override
	public void saveCampaign(Campaign m) {
		saveOrUpdate(m);
	}

	@Override
	public Campaign getCampaignById(Long cId) {
		List<Campaign> list = findByCriteria(Restrictions.eq("id", cId));
		if ( list.size() > 0 )
			return list.get(0);
		else
			return null;
	}

	@Override
	public List<Campaign> getCampaignsByOwner(Long ownerId) {
		// TODO: rever melhor forma de fazer uma query por uma foreign key
		List<Campaign> list = findByCriteria(Restrictions.eq("owner.userId", ownerId));
		return list;
	}

	/*@Override
	public boolean campaignDataBulkInsert(Long cId,
			Collection<? extends CampaignDataItem> data) {
		Session s = null;
		//Transaction t = null;

		try {
			s = getCurrentSession();
			if (!s.isConnected())
				throw new RuntimeException("!connected");
			if (!s.isOpen())
				throw new RuntimeException("!open");
			int count = data.size();
			int n = 0;
			for (CampaignDataItem m : data) {
				n++;
				if (n % 10 == 1)
					;//t = s.beginTransaction();
				m.setCId(cId);
				s.saveOrUpdate(m);
				if (n % 10 == 0 || n == count)
					{s.flush();s.clear();}//t.commit();
			}
		} catch(RuntimeException e){
			try {
				;//t.rollback();
			} catch(RuntimeException rbe) {
				//log.error("Couldnâ€™t roll back transaction", rbe);
			}
			throw e;
		} finally {

			if(s != null){
				//s.close();
			}
		}
		return true;
	}*/

	@Override
	public List<Campaign> findByCampaignId(Long cId) {
		return findByCriteria(Restrictions.eq("parentId", cId));
	}

	@Override
	public Collection<Campaign> getLastCampaigns(int pageSize) {
		// TODO Auto-generated method stub
		//List<Campaign> list = findByCriteria(Restrictions.eq("updated_at", ownerId));
		List<Campaign> list = findByCriteria(Order.desc("updated_at"), 0, pageSize,
				Restrictions.isNotNull("name"));
		return list;
	}

    @Override
    public Collection<Campaign> getAllCampaigns() {
       return findByCriteria(null);
    }

    @Override
    public Site getSiteById(Long cId) {
        return findById(cId).getSite();
    }
}
