package pt.feup.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

/**
 * Generic DAO interface.
 *
 * @param <E>  DAO Object Id type.
 * @param <I>  DAO Object type.
 */
public interface AbstractDao<E, I extends Serializable> {

	/**
	 * Gets Object by Id.
	 * 
	 * @param id  Object Id.
	 * @return the Object, if found; otherwise, null.
	 */
	E findById(I id);

	/**
	 * Saves a new Object.
	 * 
	 * @param e  Object to be saved.
	 */
	void save(E e);

	/**
	 * Saves a Object.
	 * 
	 * @param e  Object to be saved.
	 */
	void saveOrUpdate(E e);

	/**
	 * Deletes a Object permanently from the DB.
	 * 
	 * @param e  Object to be deleted.
	 */
	void delete(E e);

	/**
	 * Flushes the session, writes changes to the database.
	 */
	void flush();
        
        void clear();

	/**
	 * Gets Objects based on given Criterion.
	 * 
	 * @param criterion  Criterion to filter results.
	 * @return List of Objects.
	 */
	List<E> findByCriteria(Criterion criterion);
	List<E> findByCriteria(Order order, Criterion... criterion);

	/**
	 * Gets a paginated list Objects based on given Criterion.
	 * 
	 * @see #findByCriteria(Criterion)
	 * @param order  Order of the results.
	 * @param firstResult  Starting index of results.
	 * @param maxResults  Number of results to retrieve.
	 * @param criterion  Criterion to filter results.
	 * @return List of Objects.
	 */
	List<E> findByCriteria(Order order, int firstResult, int maxResults,
			Criterion... criterion);

	/**
	 * Gets number of results based on given Criteria.
	 * 
	 * @param criterion  Criteria to filter results.
	 * @return  Number of results.
	 */
	Integer findByCriteriaCount(Criterion... criterion);
	Integer findByCriteriaCount(int firstResult, int maxResults,
			Criterion... criterion);
        
        void bulkInsert(ArrayList<E> obj, Session s);
        
        void save(E obj, Session s);
}
