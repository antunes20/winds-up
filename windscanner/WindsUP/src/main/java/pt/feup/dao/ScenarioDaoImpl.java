/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.feup.dao;

import org.springframework.stereotype.Repository;
import pt.feup.entity.Dataset;
import pt.feup.entity.Scenario;

/**
 *
 * @author José Magalhães
 */
@Repository
public class ScenarioDaoImpl extends AbstractDaoImpl<Scenario, Long> implements ScenarioDao  {
    
    public ScenarioDaoImpl(){
        super(Scenario.class);
    }
    
    @Override
    public void saveScenario(Scenario m) {
       saveOrUpdate(m);
    }
}
