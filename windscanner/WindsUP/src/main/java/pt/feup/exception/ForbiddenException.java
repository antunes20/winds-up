package pt.feup.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Forbidden")
public class ForbiddenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4619532012965075179L;

	public ForbiddenException() {
		super("");
	}

	public ForbiddenException(String message) {
		super(message);
	}
}
