package pt.feup.exception;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class WSControllerAdvice {

	@ExceptionHandler(ForbiddenException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	private ErrorMessage handleForbidden(ForbiddenException exc) {
		return new ErrorMessage(exc.getMessage());
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	private ErrorMessage handleNotFound(HttpServletRequest request, HttpServletResponse response,
			ResourceNotFoundException exc) throws IOException {
		if(supportsJsonResponse(request.getHeader("Accept"))) {
			response.setContentType(JSON_MEDIA_TYPE.toString());
			return new ErrorMessage(exc.getMessage());
		} else {
			response.setContentType(MediaType.TEXT_HTML_VALUE);
			response.sendError(HttpStatus.NOT_FOUND.value(), exc.getMessage());
			return null;
		}
	}

	@ExceptionHandler(BadRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	private ErrorMessage badRequest(BadRequestException exc) {
		return new ErrorMessage(exc.getMessage());
	}

	@ExceptionHandler(WSUnhandledException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	@ResponseBody
	private ErrorMessage unhandled(WSUnhandledException exc) {
		return new ErrorMessage(exc.getMessage());
	}

	private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
	private static final MediaType JSON_MEDIA_TYPE = new MediaType("application", "json", DEFAULT_CHARSET);

	private boolean supportsJsonResponse(String acceptHeader) {
		List<MediaType> mediaTypes = MediaType.parseMediaTypes(acceptHeader);

		for(MediaType mediaType : mediaTypes) {
			if(JSON_MEDIA_TYPE.includes(mediaType))
				return true;
		}
		return false;
	}
}
