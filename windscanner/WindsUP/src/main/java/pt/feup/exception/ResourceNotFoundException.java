package pt.feup.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7242855496825952786L;

	public ResourceNotFoundException() {
		super("");
	}

	public ResourceNotFoundException(String message) {
		super(message);
	}
}
