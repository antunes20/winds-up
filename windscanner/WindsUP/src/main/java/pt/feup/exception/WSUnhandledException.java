package pt.feup.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="UnhandledException")
public class WSUnhandledException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7578001415756003470L;

	public WSUnhandledException() {
		super("");
	}

	public WSUnhandledException(String message) {
		super(message);
	}
}
