package pt.feup.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="BadRequest")
public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2424924849236788865L;

	public BadRequestException() {
		super("");
	}

	public BadRequestException(String message) {
		super(message);
	}
}
