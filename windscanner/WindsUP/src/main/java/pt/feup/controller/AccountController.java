package pt.feup.controller;

import java.io.File;
import java.security.Principal;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pt.feup.util.HDF5Converter;

@Controller
public class AccountController {
	/*@RequestMapping(value="/welcome", method = RequestMethod.GET)
	public String printWelcome(ModelMap model, Principal principal ) {

		String name = principal.getName();
		model.addAttribute("username", name);
		model.addAttribute("message", "Spring Security Custom Form example");
		return "hello";

	}*/

	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(ModelMap model) throws Exception {
		return "account/login";
	}

	@RequestMapping(value="/accessdenied", method = RequestMethod.GET)
	public String loginerror(ModelMap model) {

		model.addAttribute("error", "true");
		return "account/login";

	}

	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {

		return "account/login";

	}

	@RequestMapping(value="/403", method = RequestMethod.GET)
	public ResponseEntity<String> logout() {

		return new ResponseEntity<String>("403 - Forbidden", HttpStatus.FORBIDDEN);

	}
        
        @RequestMapping(value="/signup", method = RequestMethod.GET)
	public String signup(ModelMap model) throws Exception {
		return "account/signup";
	}
}
