/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.controller;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pt.feup.entity.Campaign;
import pt.feup.entity.Site;
import pt.feup.service.SiteService;

/**
 *
 * @author José
 */
@Controller
@RequestMapping("/sites")
public class SiteController {

    @Autowired
    private SiteService siteService;

    public SiteController() {
        //Initialize controller properties here or 
        //in the Web Application Context

        //setCommandClass(MyCommand.class);
        //setCommandName("MyCommandName");
        //setSuccessView("successView");
        //setFormView("formView");
    }

    @RequestMapping(method = RequestMethod.GET, value = "add")
    String loadSiteForm(HttpServletRequest request, Model model) {
        model.addAttribute("site", new Site());
        return "site/add";
    }

    @RequestMapping(method = RequestMethod.POST, value = "add")
    @ResponseBody Site CreateCampaign(@RequestBody Site site, Model model) {
        siteService.saveSite(site);
        return site;
    }

}
