/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pt.feup.entity.User;
import pt.feup.service.UserService;
import pt.feup.util.HDF5Converter;

/**
 *
 * @author José
 */
@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private HDF5Converter converter;

    public HomeController() {
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String SayHello(ModelMap model) throws Exception {
      return "index";
    }

    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String student(ModelMap model) {
        return "teste";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addStudent(ModelMap model) throws Exception {
        converter.HDF5Import("C:/hdf/wind.h5");
        return "login";
    }

    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
