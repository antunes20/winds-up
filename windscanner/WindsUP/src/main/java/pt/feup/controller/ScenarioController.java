/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pt.feup.entity.Campaign;
import pt.feup.entity.Dataset;
import pt.feup.entity.DatasetColumn;
import pt.feup.entity.DatasetColumnPK;
import pt.feup.entity.DatasetTemplate;
import pt.feup.entity.Equipment;
import pt.feup.entity.Measurement;
import pt.feup.entity.Produced;
import pt.feup.entity.ProducedPK;
import pt.feup.entity.Scenario;
import pt.feup.entity.Station;
import pt.feup.pojo.Datatable;
import pt.feup.service.MeasurementService;
import pt.feup.service.ScenarioService;
import pt.feup.pojo.DatatableResponse;
import pt.feup.pojo.Order;
import pt.feup.pojo.DatasetTemplateForm;
import pt.feup.service.CampaignService;
import pt.feup.service.EquipmentService;
import pt.feup.service.StationService;
import pt.feup.service.UserService;
import pt.feup.util.HDF5Converter;

/**
 *
 * @author José Magalhães
 */
@Controller
@RequestMapping("/campaigns")
public class ScenarioController {

    @Autowired
    ScenarioService scService;
    @Autowired
    MeasurementService msService;
    @Autowired
    private UserService userService;
    @Autowired
    private CampaignService cpgnService;
    @Autowired
    EquipmentService equipmentService;
    @Autowired
    StationService stationService;

    @SuppressWarnings("unused")
    private static final org.apache.log4j.Logger logger = LogManager.getLogger(ScenarioController.class.getName());

    @RequestMapping(method = RequestMethod.GET, value = "/scenarios/add")
    String loadScenarioForm(HttpServletRequest request, @RequestParam(value = "c", required = false) Long cid, Model model) {
        if (cid == null) {
            cid = -1L;
        }
        model.addAttribute("scenario", new Scenario());
        model.addAttribute("cid", cid);
        model.addAttribute("campaigns", cpgnService.getAllCampaigns());
        return "scenario/add";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/scenarios/add")
    String CreateScenario(@ModelAttribute Scenario scenario, Model model) {
        scenario.setCreatedAt(new Date());
        scenario.setUpdatedAt(new Date());
        scService.saveScenario(scenario);
        model.addAttribute("created", true);
        return "redirect:/campaigns/scenarios/" + scenario.getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/scenarios/{cId}")
    String getScenario(HttpServletRequest request, @PathVariable String cId, @RequestParam(required = false) boolean created, Model model) {
        Long lCId = Long.decode(cId);
        Scenario scenario = scService.findById(lCId, false);
        model.addAttribute("scenario", scenario);
        model.addAttribute("datasets", scenario.getDatasetCollection());
        if (created) {
            model.addAttribute("message", "Campaign successfully created.");
        }
        return "scenario/details";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/scenarios/datasets/{cId}")
    String getDataset(HttpServletRequest request, @PathVariable String cId, @RequestParam(required = false) boolean created, Model model) {
        Produced p = new Produced();
        p.setProducedPK(new ProducedPK());
        if (created) {
            model.addAttribute("message", "Campaign successfully created.");
        }
        Long lCId = Long.decode(cId);
        Dataset dt = scService.findByDsId(lCId);
        model.addAttribute("dataset", dt);
        model.addAttribute("dsTemplateColumns", cpgnService.getDatasetDataColumns(dt));
        model.addAttribute("produced", dt.getProducedCollection());
        model.addAttribute("equipment", new Equipment());
        model.addAttribute("station", new Station());
        model.addAttribute("stations", stationService.getAllStations());
        model.addAttribute("equipments", equipmentService.getAllEquipments());
        model.addAttribute("producedItem", p);
        return "dataset/details";
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST, value = "/scenarios/datasets/{cId}")
    @ResponseBody
    DatatableResponse getMeasurments(@PathVariable String cId, @RequestBody Datatable request) {
        DatatableResponse datatable;
        Long lCId = Long.decode(cId);
        Dataset dt = scService.findByDsId(lCId);
        Order order = (request.getOrder().get(0));
        int totalRecords = msService.getMeasurementsByDSIdCount(lCId);
        int numRecordsToShow = request.getLength();
        if (numRecordsToShow == -1) {
            numRecordsToShow = totalRecords;
        }
        if ((request.getDateFrom() != null && !request.getDateFrom().equals("")) && (request.getDateTo() != null && !request.getDateTo().equals(""))) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date from = null;
            Date to = null;
            try {
                from = formatter.parse(request.getDateFrom());
                to = formatter.parse(request.getDateTo());
            } catch (ParseException ex) {
                Logger.getLogger(ScenarioController.class.getName()).log(Level.SEVERE, null, ex);
                return new DatatableResponse(request.getDraw(), 0, 0, null);
            }
            if (from.before(to)) {
                List<Measurement> msData = msService.getMeasurementsByDSId(lCId, order.getDir(), order.getColumn(), request.getStart(), numRecordsToShow, from, to);
                int recordsFiltered = msService.getMeasurementsByDSIdCount(lCId, from, to);
                datatable = new DatatableResponse(request.getDraw(), totalRecords, recordsFiltered, msData);
                return datatable;
            }
        }
        List<Measurement> msData = msService.getMeasurementsByDSId(lCId, order.getDir(), order.getColumn(), request.getStart(), numRecordsToShow);
        datatable = new DatatableResponse(request.getDraw(), totalRecords, totalRecords, msData);

        //Dataset dt = scService.findByDsId(lCId);
        //model.addAttribute("dataset", dt);
        //model.addAttribute("measurements",scService.getMeasurementsByDSId(lCId));
        return datatable;

    }
    
     @RequestMapping(method = RequestMethod.POST, value = "/scenarios/datasets/{cId}/produced")
    String createProducedDataset(HttpServletRequest request, @PathVariable String cId, @ModelAttribute Produced produced) {
        produced.getProducedPK().setDatasetId(Long.parseLong(cId));
        cpgnService.saveProduced(produced);
        return "redirect:/campaigns/scenarios/datasets/" + cId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/scenarios/datasets/add")
    String loadDatasetForm(HttpServletRequest request, @RequestParam(value = "s", required = false) Long sid, @RequestParam(value = "c") Long cid, Model model) {
        if (sid == null) {
            sid = -1L;
        }
        model.addAttribute("scenario", new Scenario());
        model.addAttribute("sid", sid);
        model.addAttribute("dataset", new Dataset());
        model.addAttribute("datasetTemplate", new DatasetTemplateForm());
        model.addAttribute("templates", cpgnService.getAllDSTemplates());
        model.addAttribute("scenarios", cpgnService.getCampaignScenarios(cid));
        return "dataset/add";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/scenarios/datasets/add")
    String CreateDataset(@ModelAttribute Dataset dataset, Model model) {
        dataset.setCreatedAt(new Date());
        dataset.setUpdatedAt(new Date());
        cpgnService.saveDataset(dataset);
        model.addAttribute("created", true);
        return "redirect:/campaigns/scenarios/datasets/" + dataset.getId();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/scenarios/datasets/templates/add")
    @ResponseBody
    DatasetTemplateForm CreateTemplate(@ModelAttribute DatasetTemplateForm dt, Model model) {
        short i = 0;
        DatasetTemplate ds = new DatasetTemplate();
        Collection<DatasetColumn> c = dt.getDatasetColumnCollection();
        ds.setName(dt.getName());
        ds.setDescription(dt.getDescription());
        ds.setCreatedAt(new Date());
        ds.setUpdatedAt(new Date());
        ds.setOwnerId(userService.findByUserId(100L));
        for (DatasetColumn d : c) {
            d.getDatasetColumnPK().setColumnPos(i);
            d.getDatasetColumnPK().setDatasetTemplate(ds);
            i++;
        }
        ds.setDatasetColumnCollection(c);
        cpgnService.saveDSTemplate(ds);
        dt.setId(ds.getId());
        //cpgnService.saveDSTemplate(ds);
        return dt;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"), true);
        binder.registerCustomEditor(Date.class, editor);
    }

    /*@RequestMapping(method = RequestMethod.GET, value = "")
     String getLastCampaigns(HttpServletRequest request, Model model) {
     1
     //model.addAttribute("campaigns", cpgnService.getAllCampaigns());
     return "campaign/index";
     }*/
}
