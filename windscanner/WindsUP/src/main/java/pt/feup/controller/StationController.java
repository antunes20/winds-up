/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pt.feup.entity.Station;
import pt.feup.service.StationService;

/**
 *
 * @author José
 */
@Controller
@RequestMapping("/stations")
public class StationController {

    @Autowired
    StationService stationService;
    
    @RequestMapping(method = RequestMethod.GET, value = "add")
    String loadSiteForm(HttpServletRequest request, Model model) {
        model.addAttribute("station", new Station());
        return "station/add";
    }

    @RequestMapping(method = RequestMethod.POST, value = "add")
    @ResponseBody
    Station CreateCampaign(@RequestBody Station station) {
        stationService.saveStation(station);
        return station;
    }
}
