/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pt.feup.entity.Equipment;
import pt.feup.service.EquipmentService;

/**
 *
 * @author José
 */
@Controller
@RequestMapping("/equipments")
public class EquipmentController {

    @Autowired
    EquipmentService equipmentService;

    public EquipmentController() {
        //Initialize controller properties here or 
        //in the Web Application Context

        //setCommandClass(MyCommand.class);
        //setCommandName("MyCommandName");
        //setSuccessView("successView");
        //setFormView("formView");
    }
    
     @RequestMapping(method = RequestMethod.POST, value = "/produced/add")
    @ResponseBody Equipment createEquipmentJson(HttpServletRequest request, @RequestBody Equipment equipment) {
        equipmentService.saveEquipment(equipment);
        return equipment;
    }

    @RequestMapping(method = RequestMethod.GET, value = "add")
    String loadEquipmentForm(HttpServletRequest request, Model model) {

        model.addAttribute("equipment", new Equipment());
        return "equipment/add";
    }

    @RequestMapping(method = RequestMethod.POST, value = "add")
    String createEquipment(HttpServletRequest request, @ModelAttribute Equipment equipment, Model model) {
        equipmentService.saveEquipment(equipment);
        model.addAttribute("created", true);
        return "redirect:/equipments"; //+ equipment.getSerialNumber();
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    String getLastCampaigns(HttpServletRequest request, Model model) {

        model.addAttribute("equipments", equipmentService.getAllEquipments());
        return "equipment/index";
    }
}
