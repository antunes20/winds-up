package pt.feup.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pt.feup.entity.Campaign;
import pt.feup.service.CampaignService;
import pt.feup.service.MeasurementService;
import pt.feup.service.SiteService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import pt.feup.entity.Site;
import pt.feup.service.UserService;

@Controller
@RequestMapping("/campaigns")
public class CampaignController {

    @Autowired
    private CampaignService cpgnService;

    //@Autowired
    //private CpgnResourceService cpgnResourceService;
    @Autowired
    private MeasurementService measService;
    @Autowired
    private SiteService siteService;
    @Autowired
    private UserService userService;

    @SuppressWarnings("unused")
    private static final Logger logger = LogManager.getLogger(CampaignController.class.getName());

    private Date from, to;

    @RequestMapping(method = RequestMethod.GET, value = "add")
    String loadCampaignForm( HttpServletRequest request, Model model) {
      
        model.addAttribute("campaign", new Campaign());
        model.addAttribute("site", new Site());
        model.addAttribute("sites", siteService.getAllSites());
        return "campaign/add";
    }

    @RequestMapping(method = RequestMethod.POST, value = "add")
    String CreateCampaign(@ModelAttribute Campaign campaign, Model model) {
        campaign.setOwnerId(userService.findByUserId(100L));
        campaign.setCreatedAt(new Date());
        campaign.setUpdatedAt(new Date());
        cpgnService.saveCampaign(campaign);
        model.addAttribute("created", true);
        return "redirect:/campaigns/" + campaign.getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{cId}")
    String getCampaign(HttpServletRequest request, @PathVariable String cId, @RequestParam(required = false) boolean created, Model model) {
        Long lCId = Long.decode(cId);
        Campaign campaign = cpgnService.findByCId(lCId, false);
        model.addAttribute("campaign", campaign);
        model.addAttribute("site", campaign.getSite());
        model.addAttribute("scenarios", campaign.getScenarioCollection());
        if(created)
            model.addAttribute("message", "Campaign successfully created.");          
        return "campaign/details";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/my")
    String getUserCampaigns(HttpServletRequest request,
            @RequestParam(value = "page", required = false) Long page,
            @RequestParam(value = "pagesize", required = false) Long lPageSize,
            @RequestParam(value = "format", required = false) String format, Model model) {
        model.addAttribute("viewType", "my");
        model.addAttribute("menuItem", "mycampaigns");
        model.addAttribute("viewTitle", "My Campaigns");
        return "MyCampaignsView";
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    String getLastCampaigns(HttpServletRequest request, Model model) {

        model.addAttribute("campaigns", cpgnService.getAllCampaigns());
        return "campaign/index";
    }

    /**
     * Builds dates from request parameters, in the yyyy-M-d HH:mm:CIdss format
     *
     * @param strFrom interval start
     * @param strTo interval end
     * @return whether the interval is valid
     */
    protected boolean getDates(String strFrom, String strTo) {
        final String dateFormat = "yyyy-M-d'T'HH:mm:ss'Z'";
        final String dateFormatAlt = "yyyy-M-d HH:mm:ss";
        if (strFrom != null && strTo != null) {
            Calendar mydate = new GregorianCalendar();
            try {
                try {
                    from = new SimpleDateFormat(dateFormat, Locale.ENGLISH).parse(strFrom);
                } catch (ParseException e) {
                    from = new SimpleDateFormat(dateFormatAlt, Locale.ENGLISH).parse(strFrom);
                }
                mydate.setTime(from);
                try {
                    to = new SimpleDateFormat(dateFormat, Locale.ENGLISH).parse(strTo);
                } catch (ParseException e) {
                    to = new SimpleDateFormat(dateFormatAlt, Locale.ENGLISH).parse(strTo);
                }
                mydate.setTime(to);
                if (to.before(from)) {
                    to = from;
                }
            } catch (ParseException e) {
                // Ignore interval if dates are invalid
                from = null;
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true);
        binder.registerCustomEditor(Date.class, editor);
    }
    /*@InitBinder
     public void initBinder(WebDataBinder webDataBinder) {
     SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
     dateFormat.setLenient(false);
     webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
     }*/

    /*@RequestMapping(method = RequestMethod.GET, value = "/{dsId}/metadata")
     String getCampaignMetadata(HttpServletRequest request, @PathVariable String dsId,
     @RequestParam(value = "page", required = false) Long page,
     @RequestParam(value = "pagesize", required = false) Long lPageSize,
     @RequestParam(value = "from", required = false) String strFrom,
     @RequestParam(value = "to", required = false) String strTo, 
     @RequestParam(value = "format", required = false) String format, Model model) {
     Long lDsId = Long.decode(dsId);
     Campaign campaign = cpgnService.findByCId(lDsId);
     int pageSize = 20;
     if (lPageSize != null && lPageSize > 1 && lPageSize < 200)
     pageSize = lPageSize.intValue();
     int first = 0;
     if (page == null)
     page = Long.valueOf(1);
     first = (page.intValue()-1)*pageSize;
     int rowCount = 0;
     getDates(strFrom, strTo);

     List<CpgnMetadata> dataset;

     if (from == null) {
     rowCount = measService.getCpgnMetadataByDSIdCount(lDsId);
     dataset = measService.getCpgnMetadataByDSId(lDsId, first, pageSize);
     } else {
     rowCount = measService.getCpgnMetadataByDSIdCount(lDsId, from, to);
     dataset = measService.getCpgnMetadataByDSId(lDsId, from, to, first, pageSize);
     }
     String uri = "http://windscanner.eu/dataset/" + dsId + "/metadata";

     ServletUriComponentsBuilder ucb = ServletUriComponentsBuilder.fromRequest(request);

     // TODO: support for complete dataset retrieval
     if ( from != null) {
     model.addAttribute("csvURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../csv").build().toUriString());
     model.addAttribute("hdfURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../hdf").build().toUriString());
     DateFormat df = new SimpleDateFormat("yyyy, MM-1, dd, HH, mm, ss");
     String attFrom = df.format(from);
     String attrTo = df.format(to);
     model.addAttribute("fromDate", attFrom);
     model.addAttribute("toDate", attrTo);
     }

     model.addAttribute("datasetURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../").build().toUriString());
     model.addAttribute("resourcesURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../resources").build().toUriString());

     model.addAttribute("firstURL", ucb.replaceQueryParam("page").build().toUriString());
     if (page > 1)
     model.addAttribute("previousURL", ucb.replaceQueryParam("page", page-1).build().toUriString());

     int lastPage = (int) Math.ceil((double)rowCount / (double)pageSize);
     if (page < lastPage)
     model.addAttribute("nextURL", ucb.replaceQueryParam("page", page+1).build().toUriString());
     model.addAttribute("lastURL", ucb.replaceQueryParam("page", lastPage).build().toUriString());

     model.addAttribute("campaign", campaign);
     model.addAttribute("dataseturi", uri);
     model.addAttribute("dataset", dataset);

     return "CampaignMetadataView";
     }

     @RequestMapping(method = RequestMethod.GET, value = "/{cId}/resources")
     String getCampaignResources(HttpServletRequest request, @PathVariable String cId,
     @RequestParam(value = "page", required = false) Long page,
     @RequestParam(value = "pagesize", required = false) Long lPageSize,
     @RequestParam(value = "from", required = false) String strFrom,
     @RequestParam(value = "to", required = false) String strTo, 
     @RequestParam(value = "format", required = false) String format, Model model) {
     Long lCId = Long.decode(cId);
     Campaign campaign = cpgnService.findByCId(lCId);
     int pageSize = 20;
     if (lPageSize != null && lPageSize > 1 && lPageSize < 200)
     pageSize = lPageSize.intValue();
     int first = 0;
     if (page == null)
     page = Long.valueOf(1);
     first = (page.intValue()-1)*pageSize;
     int rowCount = 0;
     getDates(strFrom, strTo);

     List<CpgnResource> dataset = null;

     // TODO: paginacao
     boolean hasPagination = false;
     if ( !hasPagination && from == null) {
     rowCount = cpgnResourceService.getCpgnResourceByCIdCount(lCId);
     dataset = cpgnResourceService.getCpgnResourceByCId(lCId, first, pageSize);
     } else {
     //rowCount = cpgnResourceService.getCpgnMetadataByDSIdCount(lCId, from, to);
     //dataset = cpgnResourceService.getCpgnMetadataByDSId(lCId, from, to, first, pageSize);
     }
     String uri = "http://windscanner.eu/dataset/" + cId + "/resources";

     ServletUriComponentsBuilder ucb = ServletUriComponentsBuilder.fromRequest(request);

     if ( from != null) {
     model.addAttribute("csvURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../csv").build().toUriString());
     model.addAttribute("hdfURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../hdf").build().toUriString());
     DateFormat df = new SimpleDateFormat("yyyy, MM-1, dd, HH, mm, ss");
     String attFrom = df.format(from);
     String attrTo = df.format(to);
     model.addAttribute("fromDate", attFrom);
     model.addAttribute("toDate", attrTo);
     }

     model.addAttribute("datasetURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../").build().toUriString());
     model.addAttribute("metadataURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").pathSegment("../metadata").build().toUriString());
     model.addAttribute("baseResURL", ServletUriComponentsBuilder.fromRequest(request)
     .replaceQueryParam("page").replaceQueryParam("from").replaceQueryParam("to")
     .build().toUriString());

     model.addAttribute("firstURL", ucb.replaceQueryParam("page").build().toUriString());
     if (page > 1)
     model.addAttribute("previousURL", ucb.replaceQueryParam("page", page-1).build().toUriString());

     int lastPage = (int) Math.ceil((double)rowCount / (double)pageSize);
     if (page < lastPage)
     model.addAttribute("nextURL", ucb.replaceQueryParam("page", page+1).build().toUriString());
     model.addAttribute("lastURL", ucb.replaceQueryParam("page", lastPage).build().toUriString());

     Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
     if ( user.getClass().equals(UserAuth.class) &&
     ((UserAuth)user).getId().equals(campaign.getOwner().getUserId()) )
     model.addAttribute("userIsOwner", true);

     model.addAttribute("campaign", campaign);
     model.addAttribute("campaignId", cId);
     model.addAttribute("dataseturi", uri);
     model.addAttribute("dataset", dataset);

     return "CampaignResourcesView";
     }

     @RequestMapping(method = RequestMethod.GET, value = "/{cId}/resources/{rId}")
     @ResponseBody FileSystemResource getCampaignResource(HttpServletRequest request,
     @PathVariable Long cId, @PathVariable Long rId,
     Model model, HttpServletResponse response) {
     return getCampaignResource_name(request, cId, rId, null, model, response);
     }

     @RequestMapping(method = RequestMethod.GET, value = "/{cId}/resources/{rId}/{name}")
     @ResponseBody FileSystemResource getCampaignResource_name(HttpServletRequest request,
     @PathVariable Long cId, @PathVariable Long rId, @PathVariable String name,
     Model model, HttpServletResponse response) {
     //Campaign campaign = cpgnService.findByCId(lDsId);

     CpgnResource r = cpgnResourceService.findById(rId);
     if ( r == null )
     throw new ResourceNotFoundException();

     response.setContentType ("application/download");  
     response.setHeader ("Content-Disposition", "attachment; filename=\""+
     r.getFilename().substring(r.getFilename().lastIndexOf("/") + 1)
     .substring(r.getFilename().lastIndexOf("\\") + 1)+"\"");
     return new FileSystemResource(new File(r.getFilename()));
     }*/
}
