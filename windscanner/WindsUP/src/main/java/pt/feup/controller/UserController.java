package pt.feup.controller;

import java.util.Collection;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.feup.entity.SecurityRole;
import pt.feup.entity.User;
import pt.feup.entity.UserAuth;
import pt.feup.exception.BadRequestException;
/*import pt.feup.exception.ForbiddenException;
import pt.feup.exception.ResourceNotFoundException;
import pt.feup.exception.WSUnhandledException;
import pt.feup.service.SecurityRoleService;*/
import pt.feup.service.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

   // @Autowired
    //private SecurityRoleService securityRoleService;

  /*  @RequestMapping(method = RequestMethod.GET, value = "/{uId}")
    String getUser(HttpServletRequest request, @PathVariable Long uId, Model model) {
        String uri = ServletUriComponentsBuilder.fromRequest(request).build().toUriString();
        User user = userService.findByUserId(uId);

        if (user == null) {
            throw new ResourceNotFoundException();
        }

        model.addAttribute("useruri", uri);
        if (user != null) {
            model.addAttribute("user", user);
        }

        Object pUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (pUser.getClass().equals(UserAuth.class)
                && ((UserAuth) pUser).getId().equals(user.getUserId())) {
            model.addAttribute("userIsMe", true);
        }

        return "UserView";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/me")
    String getCurrentUser(HttpServletRequest request, Model model) {
        Object pUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!pUser.getClass().equals(UserAuth.class)) {
            throw new ForbiddenException("You need to sign in");
        }

        User user = userService.findByUserId(((UserAuth) pUser).getId());

        if (user == null) {
            throw new WSUnhandledException();
        }
        return "redirect:/users/" + user.getUserId();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{uId}")
    @ResponseBody
    User editUser(HttpServletRequest request, @PathVariable Long uId,
            @RequestBody User editedUser) {
        User user = userService.findByUserId(uId);

        if (user == null) {
            throw new ResourceNotFoundException();
        }

        Object pUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!pUser.getClass().equals(UserAuth.class)
                || ((UserAuth) pUser).getId().compareTo(user.getUserId()) != 0) {
            throw new ForbiddenException("You cannot edit this user");
        }

        if (editedUser.getFirstname() != null) {
            user.setFirstname(editedUser.getFirstname());
        }
        if (editedUser.getFamilyname() != null) {
            user.setFamilyname(editedUser.getFamilyname());
        }

        userService.saveUser(user);
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{uId}/roles/grant")
    @ResponseBody
    User grantUserRole(HttpServletRequest request, @PathVariable Long uId,
            @RequestBody SecurityRole srole) {
        // Spring Secuirty checks for Admin
        User user = userService.findByUserId(uId);
        if (user == null) {
            throw new ResourceNotFoundException();
        }
        user.getSecurityRoleCollection().add(
                securityRoleService.getSecurityRoleByName(srole.getName()));

        userService.saveUser(user);
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{uId}/roles/revoke")
    @ResponseBody
    User revokeUserRole(HttpServletRequest request, @PathVariable Long uId,
            @RequestBody SecurityRole srole) {
        // Spring Secuirty checks for Admin
        User user = userService.findByUserId(uId);
        if (user == null) {
            throw new ResourceNotFoundException();
        }
        user.getSecurityRoleCollection().remove(
                securityRoleService.getSecurityRoleByName(srole.getName()));

        userService.saveUser(user);
        return user;
    }

    

    @RequestMapping(method = RequestMethod.GET, value = "/list")
    String getUsers(HttpServletRequest request,
            @RequestParam(value = "filter", required = false) String filter, Model model) {
        String role = null;
        if (filter != null) {
            role = filter;
        }
        Collection<User> users = userService.getUsersWithRole(role);

        model.addAttribute("users", users);

        return "UsersView";
    }*/
    
    @RequestMapping(method = RequestMethod.POST, value = "/add")
    @ResponseBody
    User createUser(HttpServletRequest request, @RequestBody User formUser) {
        Object pUser = SecurityContextHolder.getContext().getAuthentication();
        if (!(pUser instanceof AnonymousAuthenticationToken)) {
            throw new BadRequestException("You are signed in");
        }

        User newUser = new User();
        newUser.setUsername(formUser.getUsername());
        newUser.setPassword(formUser.getPassword());
        newUser.setFirstName(formUser.getFirstName());
        newUser.setFamilyName(formUser.getFamilyName());
        newUser.setEmail(formUser.getEmail());
        newUser.setActive(true);
        userService.addUser(newUser);
        return newUser;
    }

}
