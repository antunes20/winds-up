/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import org.springframework.util.AutoPopulatingList;
import pt.feup.entity.DatasetColumn;

/**
 *
 * @author José
 */
public class DatasetTemplateForm {
   
    private long id;
    
    private String name;
    
    private String description;
  
    private List <DatasetColumn> datasetColumnCollection = new AutoPopulatingList(DatasetColumn.class);
    
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @JsonIgnore
    public List <DatasetColumn> getDatasetColumnCollection() {
        return datasetColumnCollection;
    }

    public void setDatasetColumnCollection(List <DatasetColumn> datasetColumnCollection) {
        this.datasetColumnCollection = datasetColumnCollection;
    }
    
}
