/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pt.feup.entity.Measurement;

/**
 *
 * @author José
 */
public class DatatableResponse {

    int draw;
    int recordsTotal;
    int recordsFiltered;
    List<List<String>> data;

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<List<String>> getData() {
        return data;
    }

    public void setData(List<List<String>> data) {
        this.data = data;
    }

    public DatatableResponse(int _draw, int _recordsTotal, int _recordsFiltered, List<Measurement> _data) {
        draw = _draw;
        recordsTotal = _recordsTotal;
        recordsFiltered = _recordsFiltered;
        data = new ArrayList<>();
        for (Measurement d : _data) {
            List<String> l = new ArrayList<>();
            l.add(d.getId().toString());
            l.add(d.getTimedate().toString());
            for (BigDecimal b : d.getData()) {
                l.add(b.toString());
            }
            data.add(l);
        }
    }

}
