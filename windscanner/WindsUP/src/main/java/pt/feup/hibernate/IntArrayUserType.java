package pt.feup.hibernate;

import java.io.Serializable;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

public class IntArrayUserType implements UserType {

    protected static final int SQLTYPE = java.sql.Types.ARRAY;

    private int[] toPrimitive(Integer[] array) {
        int[] a = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            a[i] = array[i];
        }
        return a;
    }

    private Integer[] toObject(int[] array) {
        if (array == null) {
            return new ArrayList<Integer>().toArray(new Integer[0]);
        }
        Integer[] a = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            a[i] = array[i];
        }
        return a;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names,
            SessionImplementor session, Object owner) throws HibernateException,
            SQLException {
        Array array = rs.getArray(names[0]);
        if (array == null) {
            return new Integer[0];
        }
        Integer[] javaArray = (Integer[]) array.getArray();
        //return toPrimitive(javaArray);
        return javaArray;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index,
            SessionImplementor session) throws HibernateException, SQLException {
        //System.out.println("test null safe set...");
        Integer[] castObject;
        Connection connection = st.getConnection();
        if (value == null) 
            castObject = new Integer[0];
        else castObject = (Integer[]) value;
        //Integer[] integers = toObject(castObject);
        Array array = connection.createArrayOf("integer", castObject);

        st.setArray(index, array);
        //System.out.println("test null safe set...");
        /*Connection connection = st.getConnection();
         int[] castObject = (int[]) value;
         Integer[] integers = toObject(castObject);
         Connection conn = ((DelegatingConnection) connection).getInnermostDelegate();
         Array array = conn.createArrayOf("integer", integers);
         st.setArray(index, array);*/
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object deepCopy(final Object o) throws HibernateException {
        //return o == null ? null : ((int[]) o).clone();
        return o;
    }

    @Override
    public Serializable disassemble(final Object o) throws HibernateException {
        return (Serializable) o;
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        //return x == null ? y == null : Arrays.equals((Integer[])x, (Integer[])y);
        boolean b;
        if (x == null) {
            b = y == null;
        } else {
            b = Arrays.equals((Integer[]) x, (Integer[]) y);
        }
        return b;
    }

    @Override
    public int hashCode(final Object o) throws HibernateException {
        return o == null ? 0 : o.hashCode();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return original;
    }

    @Override
    public Class<Integer[]> returnedClass() {
        return Integer[].class;
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{SQLTYPE};
    }

}
