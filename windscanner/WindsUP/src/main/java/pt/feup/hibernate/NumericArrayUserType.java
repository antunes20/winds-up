package pt.feup.hibernate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

public class NumericArrayUserType implements UserType {

    protected static final int SQLTYPE = java.sql.Types.ARRAY;

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names,
            SessionImplementor session, Object owner) throws HibernateException,
            SQLException {
        Array array = rs.getArray(names[0]);
        return (BigDecimal[]) array.getArray();
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index,
            SessionImplementor session) throws HibernateException, SQLException {
        Connection connection = st.getConnection();
        BigDecimal[] floats = (BigDecimal[]) value;
        Array array = connection.createArrayOf("numeric", floats);

        st.setArray(index, array);
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object deepCopy(final Object o) throws HibernateException {
        //return o == null ? null : ((BigDecimal[]) o).clone();
        return o;
    }

    @Override
    public Serializable disassemble(final Object o) throws HibernateException {
        return (Serializable) o;
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        //return x == null ? y == null : Arrays.equals((BigDecimal[])x, (BigDecimal[])y);
        boolean b;
        if (x == null) {
            b = y == null;
        } else {
            b = Arrays.equals((BigDecimal[]) x, (BigDecimal[]) y);
        }
        return b;
    }

    @Override
    public int hashCode(final Object o) throws HibernateException {
        return o == null ? 0 : o.hashCode();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return original;
    }

    @Override
    public Class<BigDecimal[]> returnedClass() {
        return BigDecimal[].class;
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{SQLTYPE};
    }

}
