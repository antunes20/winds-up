package pt.feup.service;

import java.util.Collection;

import pt.feup.entity.User;

/**
 * UserService provides access to the DB for retrieval of Users.
 * 
 * @author Filipe Gomes
 * @see User
 */
public interface UserService {

	/**
	 * Gets a User by Id.
	 * 
	 * @param userId  the User Id.
	 * @return  the User, if found; otherwise, null.
	 */
	User findByUserId(Long userId);

	void saveUser(User user);
	void addUser(User user);

	/**
	 * Returns the list of users with a given role
	 * @param role role name; null for users with no roles
	 * @return
	 */
	Collection<User> getUsersWithRole(String role);
}
