/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.StationDao;
import pt.feup.entity.Station;

/**
 *
 * @author José
 */
@Service("StationService")
@Transactional
public class StationServiceImpl implements StationService {
    
    @Autowired
    StationDao stationDao;

    @Override
    public Station findById(Long id) {
        return stationDao.findById(id);
    }

    @Override
    public void saveStation(Station s) {
        stationDao.saveOrUpdate(s);
    }

    @Override
    public Collection<Station> getAllStations() {
        return stationDao.findByCriteria(null);
    }
    
}
