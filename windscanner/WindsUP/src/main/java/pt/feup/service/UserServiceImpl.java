package pt.feup.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.feup.dao.UserDao;
import pt.feup.entity.User;

@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;


	@Override
	public User findByUserId(Long userId) {
		return userDao.findById(userId);
	}

	@Override
	public void saveUser(User user) {
		userDao.saveUser(user);
	}

	@Override
	public void addUser(User user) {
		userDao.addUser(user);
	}

	@Override
	public Collection<User> getUsersWithRole(String role) {
		return userDao.getUsersWithRole(role);
	}
}
