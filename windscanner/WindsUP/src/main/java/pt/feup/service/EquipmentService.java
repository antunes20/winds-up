/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.List;
import pt.feup.entity.Equipment;

/**
 *
 * @author José
 */
public interface EquipmentService {
    
    Equipment findBySerialNumber(String serialnumber);
    
    void saveEquipment(Equipment e);

    public List <Equipment> getAllEquipments();
}
