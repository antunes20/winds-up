/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.Collection;
import java.util.List;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.DatasetDao;
import pt.feup.dao.ScenarioDao;
import pt.feup.entity.Dataset;
import pt.feup.entity.Measurement;
import pt.feup.entity.Scenario;

/**
 *
 * @author José Magalhães
 */
@Service("ScenarioService")
@Transactional
public class ScenarioServiceImpl implements ScenarioService {

    @Autowired
    ScenarioDao scenarioDao;

    @Autowired
    DatasetDao dtDao;

    @Override
    public Scenario findById(Long id, boolean lazy) {
        Scenario s = scenarioDao.findById(id);
        if(!lazy){
            Hibernate.initialize(s.getDatasetCollection());
            Hibernate.initialize(s.getCampaign());
        }
        return s;

    }

    @Override
    public void saveScenario(Scenario scenario) {
        scenarioDao.saveScenario(scenario);
    }

    @Override
    public Collection<Dataset> getScenarioDatsets(Long id) {
        Collection<Dataset> dt = findById(id, false).getDatasetCollection();
        //Hibernate.initialize(dt);
        return dt;
    }

    @Override
    public List<Scenario> findCampaignsByOwner(Long ownerId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Measurement> getMeasurementsByDSId(Long dsId) {
       Collection<Measurement> m  = dtDao.getMeasurementsByDSId(dsId);
       Hibernate.initialize(m);
       return m;
    }

    @Override
    public Dataset findByDsId(Long id) {
        Dataset d = dtDao.findById(id);
        Hibernate.initialize(d.getScenario());
        return d;
    }

}
