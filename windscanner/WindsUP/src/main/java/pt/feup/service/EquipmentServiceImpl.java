/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.EquipmentDao;
import pt.feup.entity.Equipment;

/**
 *
 * @author José
 */

@Service("EquipmentService")
@Transactional
public class EquipmentServiceImpl implements EquipmentService {

    @Autowired
    EquipmentDao equipmentDao;
    
    @Override
    public Equipment findBySerialNumber(String serialnumber) {
        return equipmentDao.findById(serialnumber);
    }

    @Override
    public void saveEquipment(Equipment e) {
       equipmentDao.saveOrUpdate(e);
    }

    @Override
    public List<Equipment> getAllEquipments() {
        return equipmentDao.findByCriteria(null);
    }
    
}
