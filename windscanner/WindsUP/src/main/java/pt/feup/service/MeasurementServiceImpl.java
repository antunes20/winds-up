package pt.feup.service;

import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.feup.dao.MeasurementDao;
import pt.feup.entity.Measurement;

/**
 * TODO: determinar melhor tipo para guardar valores do dataset
 *
 * @author filipe
 *
 */
@Service("MeasurementService")
@Transactional
public class MeasurementServiceImpl implements MeasurementService {

    @Autowired
    private MeasurementDao measurementDao;
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public Measurement findByMeasurementId(Long mId) {
        return measurementDao.findById(mId);
    }

    @Override
    public void saveMeasurement(Measurement m) {
        measurementDao.saveMeasurement(m);
    }

    @Override
    public List<Measurement> getMeasurementsByDSId(Long dsId, int firstResult, int maxResults) {
        return measurementDao.getMeasurementsByDSId(dsId, firstResult, maxResults);
    }

    @Override
    public List<Measurement> getMeasurementsByDSId(Long dsId, String order, int orderBy, int firstResult, int maxResults) {
        String column;
        switch (orderBy) {
            case 0:
                column = "id";
                break;
            case 1:
                column = "timedate";
                break;
            default:
                int num = orderBy - 1;
                column = "data[" + num + "]";
                Session s = sessionFactory.getCurrentSession();
                //SQLQuery query = s.createSQLQuery("select * from measurement where ds_id = 32 order by data[7] desc limit 10 offset 0");
                SQLQuery query = s.createSQLQuery("select * from measurement where ds_id = :dataset order by " + column + " " + order + " limit :limit offset :offset");
                query.setParameter("dataset", dsId);
                //query.setParameter("column", column);
                query.setParameter("limit", maxResults);
                query.setParameter("offset", firstResult);
                query.addEntity(Measurement.class);
                List results = query.list();
                return results;
        }

        return measurementDao.getMeasurementsByDSId(dsId, order, column, firstResult, maxResults);
    }

    @Override
    public List<Measurement> getMeasurementsByDSId(Long dsId, String order, int orderBy, int firstResult, int maxResults, Date from, Date to) {
        String column;
        switch (orderBy) {
            case 0:
                column = "id";
                break;
            case 1:
                column = "timedate";
                break;
            default:
                int num = orderBy - 1;
                column = "data[" + num + "]";
                Session s = sessionFactory.getCurrentSession();
                //SQLQuery query = s.createSQLQuery("select * from measurement where ds_id = 32 order by data[7] desc limit 10 offset 0");
                SQLQuery query = s.createSQLQuery("select * from measurement where ds_id = :dataset and timedate between :from and :to order by " + column + " " + order + " limit :limit offset :offset");
                query.setParameter("dataset", dsId);
                //query.setParameter("column", column);
                query.setParameter("limit", maxResults);
                query.setParameter("offset", firstResult);
                query.setParameter("from", from);
                 query.setParameter("to", to);
                query.addEntity(Measurement.class);
                List results = query.list();
                return results;
        }
        return measurementDao.getMeasurementsByDSId(dsId, order, column, firstResult, maxResults, from, to);
    }

    @Override
    public List<Measurement> getMeasurementsByDSId(Long dsId, Date from,
            Date to) {
        return measurementDao.getMeasurementsByDSId(dsId, from, to);
    }

    @Override
    public List<Measurement> getMeasurementsByDSId(Long dsId, Date from,
            Date to, int firstResult, int maxResults) {
        return measurementDao.getMeasurementsByDSId(dsId, from, to, firstResult, maxResults);
    }

    @Override
    public Integer getMeasurementsByDSIdCount(Long dsId) {
        return measurementDao.getMeasurementsByDSIdCount(dsId);
    }

    @Override
    public Integer getMeasurementsByDSIdCount(Long dsId, Date from, Date to) {
        return measurementDao.getMeasurementsByDSIdCount(dsId, from, to);
    }

    /*@Override
     public CpgnMetadata findByCpgnMetadataId(Long mId) {
     return cpgnmetadataDao.findById(mId);
     }

     @Override
     public void saveCpgnMetadata(CpgnMetadata m) {
     cpgnmetadataDao.saveCpgnMetadata(m);
     }

     @Override
     public List<CpgnMetadata> getCpgnMetadataByDSId(Long cId, int firstResult, int maxResults) {
     return cpgnmetadataDao.getCpgnMetadataByCId(cId, firstResult, maxResults);
     }

     @Override
     public List<CpgnMetadata> getCpgnMetadataByDSId(Long cId, Date from,
     Date to) {
     return cpgnmetadataDao.getCpgnMetadataByCId(cId, from, to);
     }

     @Override
     public List<CpgnMetadata> getCpgnMetadataByDSId(Long cId, Date from,
     Date to, int firstResult, int maxResults) {
     return cpgnmetadataDao.getCpgnMetadataByCId(cId, from, to, firstResult, maxResults);
     }

     @Override
     public Integer getCpgnMetadataByDSIdCount(Long cId) {
     return cpgnmetadataDao.getCpgnMetadataByCIdCount(cId);
     }

     @Override
     public Integer getCpgnMetadataByDSIdCount(Long cId, Date from, Date to) {
     return cpgnmetadataDao.getCpgnMetadataByCIdCount(cId, from, to);
     }*/
}
