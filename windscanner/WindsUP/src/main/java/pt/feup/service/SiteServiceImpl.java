/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.SiteDao;
import pt.feup.entity.Site;

/**
 *
 * @author José
 */
@Service("SiteService")
@Transactional
public class SiteServiceImpl implements SiteService {
    
    @Autowired
    private SiteDao siteDao;

    @Override
    public Site findById(Long id) {
        return siteDao.findById(id);
    }

    @Override
    public void saveSite(Site s) {
       siteDao.saveOrUpdate(s);
    }

    @Override
    public Collection<Site> getAllSites() {
     return  siteDao.getAllSites();
    }
    
}
