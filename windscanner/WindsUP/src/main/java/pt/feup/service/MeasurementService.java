package pt.feup.service;

import java.util.Date;
import java.util.List;

import pt.feup.entity.Measurement;

/**
 * MeasurementService provides access to the DB for retrieval and storage of
 * Dataset Measurements.
 * 
 * @author Filipe Gomes & José Magalhães
 * @see Measurement
 */
public interface MeasurementService {


	Measurement findByMeasurementId(Long mId);
	void saveMeasurement(Measurement m);
	List<Measurement> getMeasurementsByDSId(Long dsId, int firstResult, int maxResults);
        List<Measurement> getMeasurementsByDSId(Long dsId, String order, int orderBy, int firstResult, int maxResults);
        List<Measurement> getMeasurementsByDSId(Long dsId, String order, int orderBy, int firstResult, int maxResults, Date from, Date to);
	/**
	 * Gets a list of Measurements from a given Dataset,
	 * bounded by the given timespan.
	 * 
	 * @param dsId  Dataset Id.
	 * @param from  interval start, or null for no minimum restriction.
	 * @param to  interval end, or null for no maximum restriction.
	 * @return list of Measurements in the timespan.
	 */
	List<Measurement> getMeasurementsByDSId(Long dsId, Date from, Date to);

	/**
	 * Gets a paginated list of Measurements from a given
	 * Dataset, bounded by the given timespan.
	 * 
	 * @see #getMeasurementsByDSId(Long, Date, Date)
	 * @param dsId  Measurement Run Id.
	 * @param from  interval start, or null for no minimum restriction.
	 * @param to  interval end, or null for no maximum restriction.
	 * @param firstResult  starting index of the results.
	 * @param maxResults  number of items to retrieve.
	 * @return list of Measurements in the timespan.
	 */
	List<Measurement> getMeasurementsByDSId(Long dsId, Date from, Date to,
			int firstResult, int maxResults);

	/**
	 * Gets the number of Measurements from a given Dataset.
	 * 
	 * @param dsId  Dataset Id.
	 * @return the number of records.
	 */
	Integer getMeasurementsByDSIdCount(Long dsId);

	/**
	 * Gets the number of Measurements from a given Dataset,
	 * bounded by the given timespan.
	 * @param dsId  Dataset Id.
	 * @param from  interval start, or null for no minimum restriction.
	 * @param to  interval end, or null for no maximum restriction.
	 * @return the number of records.
	 */
	Integer getMeasurementsByDSIdCount(Long dsId, Date from, Date to);

	/**
	 * Gets Campaign Metadata item by Id.
	 * 
	 * @param mId CpgnMetadata Id.
	 * @return The CpgnMetadata, if found; otherwise, null.
	 */
	/*CpgnMetadata findByCpgnMetadataId(Long mId);

	void saveCpgnMetadata(CpgnMetadata m);
	List<CpgnMetadata> getCpgnMetadataByDSId(Long dsId, int firstResult, int maxResults);
	List<CpgnMetadata> getCpgnMetadataByDSId(Long dsId, Date from, Date to);
	List<CpgnMetadata> getCpgnMetadataByDSId(Long dsId, Date from, Date to,
			int firstResult, int maxResults);

	Integer getCpgnMetadataByDSIdCount(Long dsId);
	Integer getCpgnMetadataByDSIdCount(Long dsId, Date from, Date to);*/
}
