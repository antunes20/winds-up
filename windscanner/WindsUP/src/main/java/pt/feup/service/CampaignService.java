package pt.feup.service;

import java.util.Collection;
import java.util.List;

import pt.feup.dao.CampaignDao;
import pt.feup.dao.DatasetDao;
import pt.feup.entity.Campaign;
import pt.feup.entity.Dataset;
import pt.feup.entity.DatasetColumn;
import pt.feup.entity.DatasetTemplate;
import pt.feup.entity.Produced;
import pt.feup.entity.Scenario;
import pt.feup.entity.Site;

/**
 * CampaignService provides access to the DB for retrieval and storage of Campaigns, Datasets and associated templates.
 *
 * @author Filipe Gomes
 * @see Campaign
 * @see Dataset
 * @see DSTemplate
 * @see MeasurementService
 */
public interface CampaignService {

    /**
     * @param cId Campaign Id.
     * @param lazy lazy load.
     * @return the Campaign object, if found; otherwise, null.
     */
    Campaign findByCId(Long cId, boolean lazy);

    /**
     * Saves Campaign or Measurement Run to the DB, a new record will be created if needed.
     *
     * @param campaign Campaign to be saved.
     */
    void saveCampaign(Campaign campaign);

    /**
     * Method that inserts multiple objects in the DB, associating them to a given Campaign.
     *
     * @see CampaignDao#campaignDataBulkInsert(Long, Collection)
     * @param cId Campaign Id to be associated.
     * @param data Collection of CampaignDataItems to be inserted.
     * @return whether the operation was successful.
     */
	//boolean campaignDataBulkInsert(Long cId, Collection<? extends CampaignDataItem> data);
    /**
     * Gets Campaigns or Measurement Runs owned by a given user.
     *
     * @param ownerId User Id.
     * @return A list of Campaigns.
     */
    List<Campaign> findCampaignsByOwner(Long ownerId);

    /**
     *
     * @param dsId Dataset Id.
     * @return the Dataset object, if found; otherwise, null.
     */
    Dataset findByDSId(Long dsId);

    /**
     * Saves Dataset to the DB, a new record will be created if needed.
     *
     * @param ds Dataset to be saved.
     */
    void saveDataset(Dataset ds);

    /**
     * Gets the Scenarios of a given Campaign.
     *
     * @param cId Campaign Id.
     * @return List of the Campaign's Scenarios.
     */
    Collection<Scenario> getCampaignScenarios(Long cId);

    /**
     * Method that inserts multiple objects in the DB, associating them to a given Dataset.
     *
     * {@link DatasetDao#datasetDataBulkInsert(Long, Collection)}
     *
     * @param dsId Dataset Id.
     * @param data item collection to insert.
     * @return whether the operation was successful.
     */
	//boolean datasetDataBulkInsert(Long dsId, Collection<? extends DatasetDataItem> data);
    /**
     * This method adds columns to a given DSTemplate.
     *
     * @param dsTemplate DSTemplate to be modified.
     * @param columns DSColumns to be added.
     */
    void addDatasetDataColumns(DatasetTemplate dsTemplate, Collection<DatasetColumn> columns);

    /**
     * Gets the columns of a given Dataset's template.
     *
     * @param ds Dataset to be queried.
     * @return Collection of columns.
     */
    Collection<DatasetColumn> getDatasetDataColumns(Dataset ds);

    /**
     * Gets the list of Datasets associated with a given Campaign.
     *
     * @param cId Campaign Id.
     * @return List of Datasets.
     */
    List<Dataset> getDatasetsByParent(Long cId);

    /**
     * Gets Datasets whose name matches a given string.
     *
     * A Dataset name is matched when the string is a sub-string of the name.
     *
     * @param query String to be matched.
     * @return Collection of matched Datasets.
     */
    Collection<Dataset> getDatasetsByNameMatch(String query);

    /**
     * Gets DSTemplate by Id.
     *
     * @param ds_template_id DSTemplate Id.
     * @return DSTemplate, if found; otherwise, null.
     */
    DatasetTemplate getDSTemplateById(Long ds_template_id);

    /**
     * Saves a DSTemplate.
     *
     * @param template
     */
    void saveDSTemplate(DatasetTemplate template);

    /**
     * Saves a DSTemplate columns.
     *
     * @param template
     */
    void saveDSTemplateColumns(DatasetTemplate template);

    /**
     * Saves given columns, associating with a given DSTemplate.
     *
     * @param template DSTemplate to receive columns.
     * @param columns Columns to be saved.
     */
    void saveDSTemplateColumns(DatasetTemplate template, Collection<DatasetColumn> columns);

    /**
     * Updates DSTemplate columns.
     *
     * @param modTemplate DSTemplate to be modified.
     * @param columns Columns to be associated with the template.
     */
    void updateDSTemplateColumns(DatasetTemplate modTemplate,
            Collection<DatasetColumn> columns);

    /**
     * Deletes a given DSTemplate.
     *
     * @param template DSTemplate retrieved from the DB to be deleted.
     */
    void deleteDSTemplate(DatasetTemplate template);

    public Collection<DatasetTemplate> getAllDSTemplates();

    /**
     * Gets DSTemplates whose name matches a given string.
     *
     * A DSTemplate name is matched when the string is a sub-string of the name.
     *
     * @param query String to be matched.
     * @return Collection of matched DSTemplates.
     */
    Collection<DatasetTemplate> getDSTemplatesByNameMatch(String query);

    Collection<DatasetTemplate> getDSTemplates(Long ownerId, int firstResult, int pageSize);

    Integer getDSTemplatesCount(Long ownerId);

    Collection<Campaign> getLastCampaigns(int pageSize);

    Collection<Campaign> getAllCampaigns();

    Long getLastDatasetId();

    Site getSiteByID(long cId);
    
    void saveProduced(Produced produced);

}
