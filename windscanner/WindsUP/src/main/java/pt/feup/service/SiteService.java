/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.Collection;
import pt.feup.entity.Site;

/**
 *
 * @author José
 */
public interface SiteService {
    
    public Site findById(Long id);
    
    public void saveSite(Site s);
    
    public Collection <Site> getAllSites();
}
