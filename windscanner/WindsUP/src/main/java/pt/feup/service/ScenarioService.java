/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.feup.service;

import java.util.Collection;
import java.util.List;
import pt.feup.entity.Campaign;
import pt.feup.entity.Dataset;
import pt.feup.entity.Measurement;
import pt.feup.entity.Scenario;

/**
 *
 * @author José Magalhães
 */
public interface ScenarioService {
    /**
	 * @param id  Campaign Id.
         * @param lazy lazy load.
	 * @return the Campaign object, if found; otherwise, null.
	 */
	Scenario findById(Long id, boolean lazy);

	/**
	 * Saves Measurement Run to the DB, a new record will be created if needed.
	 * @param scenario  scenario to be saved.
	 */
	void saveScenario(Scenario scenario);

	/**
	 * Gets Measurement Runs owned by a given user.
	 * 
	 * @param ownerId  User Id.
	 * @return A list of Campaigns.
	 */
	List<Scenario> findCampaignsByOwner(Long ownerId);
        
        /**
	 * Gets the Scenarios of a given Campaign.
	 * @param id  Scenario Id.
	 * @return List of the Scenarios's datasets.
	 */
	Collection<Dataset> getScenarioDatsets(Long id);
        
        Collection<Measurement> getMeasurementsByDSId(Long dsId);
        
        Dataset findByDsId(Long id);
}
