package pt.feup.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.hibernate.Hibernate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.feup.dao.DSColumnDao;
import pt.feup.dao.CampaignDao;
import pt.feup.dao.DSTemplateDao;
import pt.feup.dao.DatasetDao;
import pt.feup.dao.ProducedDao;
import pt.feup.entity.DatasetColumn;
import pt.feup.entity.Campaign;
import pt.feup.entity.DatasetTemplate;
import pt.feup.entity.Dataset;
import pt.feup.entity.Produced;
import pt.feup.entity.Scenario;
import pt.feup.entity.Site;
import pt.feup.exception.BadRequestException;
import pt.feup.service.CampaignService;

@Service("CampaignService")
@Transactional
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    private CampaignDao campaignDao;

    @Autowired
    private DatasetDao datasetDao;

    @Autowired
    private DSColumnDao dsColumnDao;

    @Autowired
    private DSTemplateDao dsTemplateDao;
    
    @Autowired
    private ProducedDao producedDao;

    @Override
    public Campaign findByCId(Long cId, boolean lazy) {
        Campaign c;
        c = campaignDao.findById(cId);
        if (!lazy) {
            Hibernate.initialize(c.getScenarioCollection());
            Hibernate.initialize(c.getSite());
        }
        return c;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveCampaign(Campaign campaign) {
        campaignDao.saveCampaign(campaign);
    }

    /*@Override
     public boolean campaignDataBulkInsert(Long cId,
     Collection<? extends CampaignDataItem> data) {
     return campaignDao.campaignDataBulkInsert(cId, data);
     }*/
    @Override
    public List<Campaign> findCampaignsByOwner(Long ownerId) {
        return campaignDao.getCampaignsByOwner(ownerId);
    }

    @Override
    public Dataset findByDSId(Long dsId) {
        return datasetDao.findById(dsId);
    }

    @Override
    public Collection<Scenario> getCampaignScenarios(Long cId) {
        Collection<Scenario> scenarios = campaignDao.findById(cId).getScenarioCollection();
        Hibernate.initialize(scenarios);
        return scenarios;
    }

    @Override
    @Transactional(readOnly = false)
    public void saveDataset(Dataset dataset) {
        datasetDao.saveDataset(dataset);
    }

    /*@Override
     public boolean datasetDataBulkInsert(Long dsId,
     Collection<? extends DatasetDataItem> data) {
     return datasetDao.datasetDataBulkInsert(dsId, data);
     }*/
    @Override
    public void addDatasetDataColumns(DatasetTemplate dsTemplate, Collection<DatasetColumn> columns) {
        for (DatasetColumn c : columns) {
            c.getDatasetColumnPK().setDatasetTemplate(dsTemplate);
            dsColumnDao.saveOrUpdate(c);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<DatasetColumn> getDatasetDataColumns(Dataset ds) {
        return dsColumnDao.getDSColumnsByDSTId(ds.getDsTemplateId());
    }

    @Override
    public List<Dataset> getDatasetsByParent(Long cId) {
        return datasetDao.getDatasetsByParent(cId);
    }

    @Override
    public Collection<Dataset> getDatasetsByNameMatch(String query) {
        return datasetDao.getByNameMatch(query);
    }

    @Override
    public DatasetTemplate getDSTemplateById(Long ds_template_id) {
        return dsTemplateDao.findById(ds_template_id);
    }

    @Override
    public void saveDSTemplate(DatasetTemplate template) {
        dsTemplateDao.saveOrUpdate(template);
    }

    @Override
    public void saveDSTemplateColumns(DatasetTemplate template) {
        for (DatasetColumn c : template.getDatasetColumnCollection()) {
            c.getDatasetColumnPK().setDatasetTemplate(template);
            dsColumnDao.saveOrUpdate(c);
        }
    }

    @Override
    public void saveDSTemplateColumns(DatasetTemplate template,
            Collection<DatasetColumn> columns) {
        for (DatasetColumn c : columns) {
            c.getDatasetColumnPK().setDatasetTemplate(template);
            dsColumnDao.saveOrUpdate(c);
        }
    }

    @Override
    public void updateDSTemplateColumns(DatasetTemplate template,
            Collection<DatasetColumn> columns) {
        /* Check new columns */
        int[] arrPos = new int[columns.size()];
        int[] arrDisplay = new int[columns.size()];
        int i = 0;
        for (DatasetColumn col : columns) {
            arrPos[i] = col.getDatasetColumnPK().getColumnPos();
            arrDisplay[i++] = col.getDisplayPos();
        }
        Arrays.sort(arrPos);
        Arrays.sort(arrDisplay);
        for (i = 1; i < arrPos.length; i++) {
            if (arrPos[i] != i) {
                throw new BadRequestException("Invalid Column Positions");
            }
            if (arrDisplay[i] != i) {
                throw new BadRequestException("Invalid Display Positions");
            }
        }
        for (DatasetColumn curC : template.getDatasetColumnCollection()) {
            dsColumnDao.delete(curC);
        }
        dsColumnDao.flush();
        template = this.getDSTemplateById(template.getId());
        for (DatasetColumn c : columns) {
            if (c.getLongName().isEmpty() || c.getShortName().isEmpty() || c.getSize() == null
                    || c.getUnit().isEmpty() || c.getDataType() == null) {
                throw new BadRequestException("Invalid column");
            }
            c.getDatasetColumnPK().setDatasetTemplate(template);
            dsColumnDao.saveOrUpdate(c);
        }
    }

    @Override
    public void deleteDSTemplate(DatasetTemplate template) {
        dsTemplateDao.delete(template);
    }

    @Override
    public Collection<DatasetTemplate> getDSTemplatesByNameMatch(String query) {
        return dsTemplateDao.getByNameMatch(query);
    }

    @Override
    public Collection<DatasetTemplate> getDSTemplates(Long ownerId, int firstResult,
            int pageSize) {
        return dsTemplateDao.getDSTemplates(ownerId, firstResult, pageSize);
    }

    @Override
    public Integer getDSTemplatesCount(Long ownerId) {
        return dsTemplateDao.getDSTemplatesCount(ownerId);
    }

    @Override
    public Collection<Campaign> getLastCampaigns(int pageSize) {
        return campaignDao.getLastCampaigns(pageSize);
    }

    @Override
    public Long getLastDatasetId() {
        return datasetDao.getLastDatasetId();
    }

    @Override
    public Collection<Campaign> getAllCampaigns() {
        return campaignDao.getAllCampaigns();
    }

    @Override
    public Site getSiteByID(long cId) {
        Site s = campaignDao.getSiteById(cId);
        Hibernate.initialize(s);
        return s;
    }
    
    @Override
   public  Collection<DatasetTemplate> getAllDSTemplates(){
        return dsTemplateDao.getAllDSTemplates();
    }

    @Override
    public void saveProduced(Produced produced) {
        producedDao.saveOrUpdate(produced);
    }

}
