/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.service;

import java.util.Collection;
import pt.feup.entity.Station;

/**
 *
 * @author José
 */
public interface StationService {

    Station findById(Long id);

    void saveStation(Station s);

    Collection<Station> getAllStations();
}
