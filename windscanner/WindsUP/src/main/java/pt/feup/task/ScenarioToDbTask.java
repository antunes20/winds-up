/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.task;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pt.feup.dao.ScenarioDao;

/**
 *
 * @author José Magalhães
 * @param <Scenario>
 */
@Component(value="scenarioToDbTask")
@Scope(value="prototype")
@Lazy(value=true)
public class ScenarioToDbTask<Scenario> extends ImportToDbTask {

    @Autowired
    ScenarioDao sc;
    
    public ScenarioToDbTask() {
        super();
    }

    @Override
    public void run() {
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();
        sc.bulkInsert(obj, s);
        s.flush();
        s.clear();
        tx.commit();
        s.close();
        obj.clear();
    }

}
