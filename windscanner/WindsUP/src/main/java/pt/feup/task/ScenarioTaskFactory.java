/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.feup.task;

import org.springframework.stereotype.Component;

/**
 *
 * @author José Magalhães
 */
@Component(value="scenarioTaskFactory")
public abstract class ScenarioTaskFactory {

    public abstract ScenarioToDbTask createTask();

}