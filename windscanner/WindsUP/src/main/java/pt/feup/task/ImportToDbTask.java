/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.task;

import java.util.ArrayList;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author José Magalhães
 */
public abstract class ImportToDbTask<E> implements Runnable {

    public ArrayList<E> obj;
    @Autowired
    protected SessionFactory sessionFactory;

    public ImportToDbTask() {
        obj = new ArrayList<>();
    }
}
