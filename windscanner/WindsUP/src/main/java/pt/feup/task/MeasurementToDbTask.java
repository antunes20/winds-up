/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.task;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.MeasurementDao;

/**
 *
 * @author José Magalhães
 * @param <Measurement>
 */
@Component
@Scope("prototype")
public class MeasurementToDbTask<Measurement> extends ImportToDbTask {

    @Autowired
    private MeasurementDao datasetDao;

    public MeasurementToDbTask() {
        super();
    }

    @Override
    public void run() {
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();
        datasetDao.bulkInsert(obj, s);
        s.flush();
        s.clear();
        tx.commit();
        s.close();
    }
}
