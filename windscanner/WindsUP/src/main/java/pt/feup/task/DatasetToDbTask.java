/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.feup.task;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pt.feup.dao.DatasetDao;
import pt.feup.entity.Dataset;

/**
 *
 * @author José Magalhães
 * @param <DataSet>
 */
@Component(value = "datasetToDbTask")
@Scope("prototype")
public class DatasetToDbTask<DataSet> extends ImportToDbTask {

    @Autowired
    private DatasetDao datasetDao;

    public DatasetToDbTask() {
        super();
    }

    @Override
    public void run() {
        Session s = sessionFactory.openSession();
        Transaction tx = s.beginTransaction();
        datasetDao.bulkInsert(obj, s);
        //s.flush();
        //s.clear();
        tx.commit();
        s.close();
        obj.clear();
    }

}
