<%--
 ** Navigation bar and results table for paginated results pages **
 **
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
%><%@ attribute name="previousURL" required="false"
%><%@ attribute name="firstURL" required="false" 
%><%@ attribute name="nextURL" required="false" 
%><%@ attribute name="lastURL" required="false"
%><%@ attribute name="sourceUrl" required="true"
%><%@ attribute name="baseUrl" required="true"
%><%@ attribute name="idField" required="true"
%><%@ attribute name="colsName" required="true"
%><%@ attribute name="colsDisplay" required="true"
%><%@ attribute name="page" required="false" %>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {
function loadResources() {
	$.ajax({
        url: '<c:url value="${sourceUrl}"/>',
        cache: false,
        type: 'GET',
        success: function(response, textStatus, XMLHttpRequest) {
        	var rUrl = '<c:url value="${baseUrl}"/>';
        	var html = $.map(response, function (item, i) {
        	  created_at = new Date(item.created_at);
        	  updated_at = new Date(item.updated_at);
        	  return "<tr>" <c:forEach var="current" items="${colsName}" varStatus = "status"
        	  	><c:if test="${status.first}">+"<td><a href=\"" + rUrl + item.${idField} + "\">" + item.${current} + "</td>"</c:if
        	  	><c:if test="${!status.first}">+ "<td>" + item.${current} + "</td>"</c:if
        	  	></c:forEach>
        	  + "</tr>";
          	}).join("");
        	if (html === "")
        		$("#tbl_resources > tbody:last").html("<tr><td colspan=\"4\">No results found!</td></tr>");
        	else
        		$("#tbl_resources > tbody:last").html(html);
        },
        error: function (request, status, error) {
        	jsonValue = jQuery.parseJSON(request.responseText);
 	        $('#error-box .error-message').html(jsonValue.error);
 	       $('#error-box').removeClass("hide").hide().fadeIn("slow");
 	   }
    });
}
loadResources();
});
//]]>
</script>
<div>
<ul class="pagination"><c:choose>
	<c:when test="${not empty previousURL}"><li><a href="${firstURL}">&laquo; First</a></li>
	<li><a href="${previousURL}"><i class="glyphicon glyphicon-arrow-left"></i>Previous</a></li></c:when>
	<c:otherwise><li class="disabled"><a href="#">&laquo; First</a></li>
	<li class="disabled"><a href="#"><i class="glyphicon glyphicon-arrow-left"></i>Previous</a></li></c:otherwise>
	</c:choose><c:choose><c:when test="${not empty nextURL}"
		><li><a href="${nextURL}"><i class="glyphicon glyphicon-arrow-right"></i>Next</a></li>
		<li><a href="${lastURL}">Last &raquo;</a></li></c:when><c:otherwise
		><li class="disabled"><a href="#"><i class="glyphicon glyphicon-arrow-right"></i>Next</a></li>
		<li class="disabled"><a href="#">Last &raquo;</a></li></c:otherwise></c:choose>
</ul>

	<table id="tbl_resources" class="table table-striped table-condensed">
		<tr>
			<c:forEach var="current" items="${colsDisplay}">
				<th>${current}</th>
			</c:forEach>
		</tr>
		<tbody>
		</tbody>
	</table>
</div>