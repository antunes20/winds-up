<%-- 
    Document   : stationForm
    Created on : 18-May-2015, 01:24:21
    Author     : José
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
           %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
           %><%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"
           %><%@ taglib  uri="http://www.springframework.org/tags/form"
           prefix="f" %>
<%@tag description="Site form" pageEncoding="UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="panel panel-success" style="margin: 0">
    <div class="panel-heading">
        <h3 class="panel-title">Create Station</h3>
    </div>
    <div class="panel-body">
        <f:form id="stationForm" modelAttribute="station" class="form-horizontal" action="${contextPath}/stations/add">
            <div class="form-group">
                <label for="inputName" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                    <f:input path="name" type="text" class="form-control" placeholder="Name" autocomplete="off" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="latitude" class="col-lg-2 control-label">Latitude</label>
                <div class="col-lg-10">
                    <f:input path="latitude" type="number" class="form-control" placeholder="Latitude" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="longitude" class="col-lg-2 control-label">Longitude</label>
                <div class="col-lg-10">
                    <f:input path="longitude" type="number" class="form-control" placeholder="Longitude" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-lg-2 control-label">Description</label>
                <div class="col-lg-10">
                    <f:input path="description" type="text" class="form-control" placeholder="Description" required="required" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-2"> </div>
                <div class="col-lg-8">
                    <f:button id="stationBtn" type="submit" class="btn btn-success btn-block"> Submit </f:button>
                </div>
            </div>
        </f:form>
    </div>
</div>