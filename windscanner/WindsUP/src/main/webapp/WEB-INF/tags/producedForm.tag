<%-- 
    Document   : producedForm
    Created on : 17/Mai/2015, 17:28:12
    Author     : Jose
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
           %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
           %><%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"
           %><%@ taglib  uri="http://www.springframework.org/tags/form"
           prefix="f" %>
<%@tag description="Dataset Equipment form" pageEncoding="UTF-8"%>

<div class="panel panel-success" style="margin: 0">
    <div class="panel-heading">
        <h3 class="panel-title">Add Equipment</h3>
    </div>
    <div class="panel-body">
        <f:form id="producedForm" modelAttribute="producedItem" class="form-horizontal" action="${dataset.id}/produced">

            <div class="form-group">
                <label for="Equipment" class="col-lg-2 control-label">Equipment</label>
                <div class="col-lg-10" style="margin-top:8px">
                    <div class="input-group">
                        <f:select id="equipmentSelect" path="producedPK.equipmentId" class="form-control chosen-select">
                            <f:options items="${equipments}" itemValue="serialNumber" itemLabel="name" ></f:options>
                        </f:select>       
                        <span class="input-group-btn">
                            <button id="createEquipmentButton" style="margin-left: 2px; margin-top:2px" class="btn btn-success btn-xs" title="Add Equipment"><span class="glyphicon glyphicon-plus"></span></button>
                        </span>
                    </div>
                </div>
            </div>  
            
             <div class="form-group">
                <label for="Station" class="col-lg-2 control-label">Station</label>
                <div class="col-lg-10" style="margin-top:8px">
                    <div class="input-group">
                        <f:select id="stationSelect" path="stationId.id" class="form-control chosen-select">
                            <f:options items="${stations}" itemValue="id" itemLabel="name" ></f:options>
                        </f:select>       
                        <span class="input-group-btn">
                            <button id="createStationButton" style="margin-left: 2px; margin-top:2px" class="btn btn-success btn-xs" title="Add Equipment"><span class="glyphicon glyphicon-plus"></span></button>
                        </span>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label for="startDate" class="col-lg-2 control-label">Start Date</label>
                <div class="col-lg-10">
                    <div class='input-group date' id='datetimepicker3'>
                        <f:input path="startDate" type='text' class="form-control" id="startDate" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="endDate" class="col-lg-2 control-label">End Date</label>
                <div class="col-lg-10">
                    <div class='input-group date' id='datetimepicker4'>
                        <f:input path="endDate" type='text' class="form-control" id="endDate" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="height" class="col-lg-2 control-label">Height</label>
                <div class="col-lg-10">
                    <f:input path="height" type="number" class="form-control" placeholder="Height" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="azimuth" class="col-lg-2 control-label">Azimuth</label>
                <div class="col-lg-10">
                    <f:input path="azimuthAngle" type="number" class="form-control" placeholder="Azimuth Angle" required="required" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-2"> </div>
                <div class="col-lg-8">
                    <f:button id="producedBtn" type="submit" class="btn btn-success btn-block"> Submit </f:button>
                    </div>
                </div>
        </f:form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#createEquipmentButton").click(function (event) {
            event.preventDefault();
            $("#mainModal").modal('toggle');
            $("#equipmentModal").modal('show');
        });
        
        $("#createStationButton").click(function (event) {
            event.preventDefault();
            $("#mainModal").modal('toggle');
            $("#stationModal").modal('show');
        });

        $('#datetimepicker3').datetimepicker({
            locale: 'en',
            format: 'DD-MM-YYYY HH:mm:ss'
        });
        $('#datetimepicker4').datetimepicker({
            locale: 'en',
            format: 'DD-MM-YYYY HH:mm:ss'
        });
        $("#datetimepicker3").on("dp.change", function (e) {
            $('#datetimepicker4').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker4").on("dp.change", function (e) {
            $('#datetimepicker3').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>