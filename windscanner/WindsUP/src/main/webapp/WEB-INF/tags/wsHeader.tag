<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
           %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
           %><%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"
           %><%@ attribute name="title" required="true"
           %><%@ attribute name="menuActiveItem" required="false" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" scope="application" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>${title}<c:if test="${not empty title}"> | </c:if>WindS&#64;UP</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta name="description" content="The WindScanner E-Science Platform"/>
            <meta name="author" content=""/>
            <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.2.0/flatly/bootstrap.min.css"/>
            <link rel="stylesheet" type="text/css" media="screen" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
            <link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/resources/css/ws-style.css"/>"/>
        <link rel="stylesheet" type="text/css" media="screen" href="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.css"/>

        <link rel="stylesheet" href="<c:url value="/resources/css/chosen.css"/>" type="text/css" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" ></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="<c:url value="/resources/js/typeahead.min.js"/>"></script>
        <script src="<c:url value="/resources/js/ws-common.js"/>"></script>
        <script src="<c:url value="/resources/js/chosen.jquery.js"/>"></script>
        <script src="<c:url value="/resources/js/validator.js"/>"></script>
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>
        <script src="http://cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment-with-locales.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

        <script src="<c:url value="/resources/js/tables.js"/>"></script>
        <script src="<c:url value="/resources/js/dynamic_list_helper2.js"/>"></script>
    </head>
    <body>
        <div class="container">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <nav class="navbar navbar-inverse navbar-fixed-top">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="${contextPath}">WindS&#64UP</a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li<c:if test="${empty menuActiveItem}"> class="active"</c:if>><a href="<c:url value="/"/>">Home</a></li>
                                    <li>
                                        <a href="<c:url value="/#about"/>">About</a>
                                </li>
                                <li>
                                    <a href="<c:url value="/#contact"/>">Contact</a>
                                </li>
                                <li${menuActiveItem=='search' ? ' class="active"' : ''}>
                                    <a href="<c:url value="/semantic" />" title="Search resources">Search</a>
                                </li>
                                <sec:authorize access="isAuthenticated()">
                                    <sec:authorize access="hasRole('ROLE_PROVIDER')">
                                        <li${menuActiveItem=='mycampaigns' ? ' class="active"' : ''}>
                                            <a href="<c:url value="/campaigns/my" />">My campaigns</a>
                                        </li>
                                    </sec:authorize>
                                    <li${menuActiveItem=='derive' ? ' class="active"' : ''}>
                                        <a href="<c:url value="/derive" />" title="Derive data">Derive</a>
                                    </li>
                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                        <li${menuActiveItem=='users' ? ' class="active"' : ''}>
                                            <a href="<c:url value="/users/list"/>">Users</a></li>
                                        </sec:authorize>
                                    <li${menuActiveItem=='user' ? ' class="active"' : ''}>
                                        <a href="<c:url value="/users/"/>
                                           <sec:authentication property="principal.id"/>">
                                            <sec:authentication property="principal.username"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<c:url value="/j_spring_security_logout" />">Sign out</a>
                                    </li>
                                </sec:authorize>
                                <sec:authorize access="! isAuthenticated()">
                                    <li>
                                        <a href="<c:url value="/login" />">Sign in</a>
                                    </li>
                                </sec:authorize>
                            </ul>
                        </div><!-- /.nav-collapse -->
                        <!-- /.container -->
                    </nav><!-- /.navbar -->
                </div>
            </div>