<%--
 ** Navigation bar for paginated results pages **
 ** @Deprecated -- should use paginationList where possible
 **
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
%><%@ attribute name="previousURL" required="false"
%><%@ attribute name="firstURL" required="false" 
%><%@ attribute name="nextURL" required="false" 
%><%@ attribute name="lastURL" required="false" %>
<ul class="pagination"><c:choose>
	<c:when test="${not empty previousURL}"><li><a href="${firstURL}">&laquo; First</a></li>
	<li><a href="${previousURL}"><i class="glyphicon glyphicon-arrow-left"></i>Previous</a></li></c:when>
	<c:otherwise><li class="disabled"><a href="#">&laquo; First</a></li>
	<li class="disabled"><a href="#"><i class="glyphicon glyphicon-arrow-left"></i>Previous</a></li></c:otherwise>
	</c:choose><c:choose><c:when test="${not empty nextURL}"
		><li><a href="${nextURL}"><i class="glyphicon glyphicon-arrow-right"></i>Next</a></li>
		<li><a href="${lastURL}">Last &raquo;</a></li></c:when><c:otherwise
		><li class="disabled"><a href="#"><i class="glyphicon glyphicon-arrow-right"></i>Next</a></li>
		<li class="disabled"><a href="#">Last &raquo;</a></li></c:otherwise></c:choose>
</ul>