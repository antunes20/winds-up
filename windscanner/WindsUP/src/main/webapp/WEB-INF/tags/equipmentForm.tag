<%-- 
    Document   : equipmentForm
    Created on : 13-May-2015, 16:42:37
    Author     : José
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
           %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
           %><%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"
           %><%@ taglib  uri="http://www.springframework.org/tags/form"
           prefix="f" %>
<%@tag description="Equipment form" pageEncoding="UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="panel panel-success" style="margin: 0">
    <div class="panel-heading">
        <h3 class="panel-title">Create Equipment</h3>
    </div>
    <div class="panel-body">
        <f:form id="equipmentForm" modelAttribute="equipment" class="form-horizontal">
            <div class="form-group">
                <label for="serialNumber" class="col-lg-2 control-label">Serial Number</label>
                <div class="col-lg-10">
                    <f:input path="serialNumber" type="text" class="form-control" placeholder="Serial Number" autocomplete="off" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                    <f:input path="name" type="text" class="form-control" placeholder="Name" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="type" class="col-lg-2 control-label">Type</label>
                <div class="col-lg-10">
                    <f:input path="type" type="text" class="form-control" placeholder="Type" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label for="model" class="col-lg-2 control-label">Model</label>
                <div class="col-lg-10">
                    <f:input path="model" type="text" class="form-control" placeholder="Model" required="required" />
                </div>
            </div>

            <div class="form-group">
                <label for="manufacturer" class="col-lg-2 control-label">Manufacturer</label>
                <div class="col-lg-10">
                    <f:input path="manufacturer" type="text" class="form-control" placeholder="Manufacturer" required="required" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-2"> </div>
                <div class="col-lg-8">
                    <f:button id="siteBtn" type="submit" class="btn btn-success btn-block"> Submit </f:button>
                    </div>
                </div>
        </f:form>
    </div>
</div>