<%-- 
    Document   : templateForm
    Created on : 30-Apr-2015, 13:24:39
    Author     : José
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"
           %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"
           %><%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"
           %><%@ taglib  uri="http://www.springframework.org/tags/form"
           prefix="f" %>
<%@tag description="Site form" pageEncoding="UTF-8"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="panel panel-success" style="margin: 0">
    <div class="panel-heading">
        <h3 class="panel-title">Create Template</h3>
    </div>
    <div class="panel-body">
        <f:form action="templates/add" modelAttribute="datasetTemplate" class="form-horizontal" method="post" id="templateForm">
            <div class="form-group">
                <label for="inputName" class="col-lg-2 control-label">Name</label>
                <div class="col-lg-10">
                    <f:input path="name" type="text" class="form-control" placeholder="Name" autocomplete="off" required="required" />
                </div>
            </div>
              
            <div class="form-group">
                <label for="description" class="col-lg-2 control-label">Description</label>
                <div class="col-lg-10">
                    <f:input path="description" type="text" class="form-control" placeholder="Description" autocomplete="off" required="required" />
                </div>
            </div>
            <div class="form-group" id="tableDiv">
                <table class="table .table-condensed" style="margin-left:2%">
                    <thead>
                        <tr>
                            <th>Short Name</th>
                            <th>Longe Name</th>
                            <th>Unit</th>
                            <th>Size</th>
                            <th>Data Type</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="columnListContainer"> 
                        <%-- 
                            IMPORTANT 
                            There must always be one row.
                            This is to allow the JavaScript to clone the row.
                            If there is no row at all, it cannot possibly add a new row.

                    If this 'default row' is undesirable 
                        remove it by adding the class 'defaultRow' to the row
                    I.e. in this case, class="person" represents the row.
                    Add the class 'defaultRow' to have the row removed.
                    This may seem weird but it's necessary because 
                    a row (at least one) must exist in order for the JS to be able clone it.
                    <tr class="person"> : The row will be present
                    <tr class="person defaultRow"> : The row will not be present
                        --%>
                        <tr class="column">    
                            <td><input type="text" class="form-control" name="datasetColumnCollection[].shortName" value="" required="required"/></td>
                            <td><input type="text" class="form-control" name="datasetColumnCollection[].longName" value="" required="required"/></td>
                            <td><input type="text" class="form-control" name="datasetColumnCollection[].unit" value="" required="required"/></td>
                            <td><input type="text" class="form-control" name="datasetColumnCollection[].size" value="" required="required"/></td>
                            <td><input type="text" class="form-control" name="datasetColumnCollection[].dataType" value="" required="required"/></td>

                            <td><a href="#" class="removeColumn">Remove Column</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form-group">

                <div class="col-lg-6">                 
                    <f:button id="addColumn" type="button" class="btn btn-success btn-block"> Add Column </f:button>
                    </div>
                    <div class="col-lg-6"> 
                    <f:button type="submit" class="btn btn-success btn-block"> Submit </f:button>
                    </div>
                </div>       
        </f:form>
    </div>
</div>

<script type="text/javascript">
    function rowAdded(rowElement) {
        //clear the imput fields for the row
        $(rowElement).find("input").val('');
        //$('.removeColumn').removeClass('hide');
        //may want to reset <select> options etc

        //in fact you may want to submit the form
        //saveNeeded();
    }
    function rowRemoved(rowElement) {
        //saveNeeded();
        //alert("Removed Row HTML:\n" + $(rowElement).html());
    }

    function saveNeeded() {
        $('#submit').css('color', 'red');
        $('#submit').css('font-weight', 'bold');
        if ($('#submit').val().indexOf('!') != 0) {
            $('#submit').val('!' + $('#submit').val());
        }
    }

    function beforeSubmit() {
        alert('submitting....');
        return true;
    }



    $(document).ready(function () {
        $('#templateForm').validator();
        var config = {
            rowClass: 'column',
            addRowId: 'addColumn',
            removeRowClass: 'removeColumn',
            formId: 'templateForm',
            rowContainerId: 'columnListContainer',
            indexedPropertyName: 'datasetColumnCollection',
            indexedPropertyMemberNames: 'shortName,longName,unit,size,dataType',
            rowAddedListener: rowAdded,
            rowRemovedListener: rowRemoved,
            beforeSubmit: beforeSubmit,
            afterSubmit: template_success
        };
        new DynamicListHelper(config);
    });
</script>