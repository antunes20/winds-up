<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" %>
<t:wsHeader title="${campaign.name}" />

<c:if test="${not empty message}">
    <div id="success-box" class="alert alert-success fade in">
        <button type="button" class="close">&times;</button>
        <div class="success-message">${message}</div>
    </div>
</c:if>
<div class="row">
    <ol class="breadcrumb">
        <li><a href="${applicationScope.contextPath}"> Home</a></li>
        <li><a href="${applicationScope.contextPath}/campaigns"> Campaigns</a></li>
        <li class="active">${campaign.name}</li>
    </ol>
    <h2>Campaign</h2>
</div>
<div class="row">
    <h4>${campaign.name}</h4>
    <hr />

    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#scenarios" aria-controls="scenarios" role="tab" data-toggle="tab">Scenarios</a> </li>
            <li role="presentation"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a></li>
            <li role="presentation"><a href="#site" aria-controls="site" role="tab" data-toggle="tab">Site</a></li>
            <a href="scenarios/add?c=<c:out value="${campaign.id}"/>" class="btn btn-success btn-xs" title="Add Scenario" style="margin-top:11px"><span class="glyphicon glyphicon-plus"></span></a>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="scenarios">
                <div class="col-md-12"><br/>

                    <table cellpadding="0" cellspacing="0" border="0" class="client-side table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Created at
                                </th>
                                <th>
                                    Updated at
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${scenarios}" var="scenario"> 
                                <tr>
                                    <td>
                                        ${scenario.id}
                                    </td>
                                    <td>
                                        <a href="<c:url value="scenarios/${scenario.id}"/>">${scenario.name}</a>
                                    </td>
                                    <td>
                                        ${scenario.createdAt}
                                    </td>
                                    <td>
                                        ${scenario.updatedAt}
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div> 
            </div>
            <div role="tabpanel" class="tab-pane" id="info">
                <div class="col-md-6"><br/>
                    <dl class="dl-horizontal">
                        <dt class="info-label">
                        ID:
                        </dt>

                        <dd class="info-data">
                            ${campaign.id}
                        </dd>

                        <dt class="info-label">
                        Name:
                        </dt>

                        <dd class="info-data">
                            ${campaign.name}
                        </dd>

                        <dt class="info-label">
                        Created at:
                        </dt>

                        <dd class="info-data">
                            ${campaign.createdAt}
                        </dd>

                        <dt class="info-label">
                        Updated at:
                        </dt>

                        <dd class="info-data">
                            ${campaign.updatedAt}
                        </dd>
                    </dl>
                </div>        
            </div>
            <div role="tabpanel" class="tab-pane" id="site">
                <div class="col-md-6"><br/>

                    <dl class="dl-horizontal">
                        <dt class="info-label">
                        ID:
                        </dt>

                        <dd class="info-data">
                            ${site.id}
                        </dd>

                        <dt class="info-label">
                        Name:
                        </dt>

                        <dd class="info-data">
                            ${site.name}
                        </dd>

                        <dt class="info-label">
                        Description:
                        </dt>

                        <dd class="info-data">
                            ${site.description}
                        </dd>

                        <dt class="info-label">
                        Latitude:
                        </dt>

                        <dd class="info-data">
                            ${site.latitude}
                        </dd>

                        <dt class="info-label">
                        Longitude:
                        </dt>

                        <dd class="info-data">
                            ${site.longitude}
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>

<t:wsFooter />