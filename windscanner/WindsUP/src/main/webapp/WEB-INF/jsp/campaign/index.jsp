<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"
         %>
<t:wsHeader title="Campaigns" />
<div class="row">
    <ol class="breadcrumb">
        <li><a href="${applicationScope.contextPath}"> Home</a></li>
        <li class="active">Campaigns</li>
    </ol>
</div>

<div class="row">

    <h2>Campaigns <a href="campaigns/add" class="btn btn-success btn-xs" title="Add Campaign"><span class="glyphicon glyphicon-plus"></span></a></h2>

    <table cellpadding="0" cellspacing="0" border="0" class="client-side table table-striped table-bordered">
        <thead>
            <tr>
                <th>
                    ID
                </th>
                <th>
                    Name
                </th>
                <th>
                    Start Date
                </th>
                <th>
                    End Date
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${campaigns}" var="campaign"> 
                <tr>
                    <td>
                        ${campaign.id}
                    </td>
                    <td>
                        <a href="<c:url value="campaigns/${campaign.id}"/>">${campaign.name}</a>
                    </td>
                    <td>

                        <fmt:formatDate value="${campaign.timestampFrom}" pattern="dd-MM-yyy" /> 
                    </td>
                    <td>
                        <fmt:formatDate value="${campaign.timestampTo}" pattern="dd-MM-yyy" />
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>


<t:wsFooter />