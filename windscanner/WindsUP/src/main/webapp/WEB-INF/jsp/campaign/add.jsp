<%-- 
    Document   : add
    Created on : 27/Ago/2014, 23:43:15
    Author     : José Magalhães
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" 
         %><%@ taglib  uri="http://www.springframework.org/tags/form"
         prefix="f" %>
<t:wsHeader title="Create Campaign" />
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-right: 10px; padding-top: 5px"><span aria-hidden="true">&times;</span></button>
            <t:SiteForm />      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="error-box" class="alert alert-danger fade in hide">
    <button type="button" class="close">&times;</button>
    <div class="error-message"></div>
</div>

<div id="success-box" class="alert alert-success fade in hide">
    <button type="button" class="close">&times;</button>
    <div class="success-message"></div>
</div>

<div class="row" style="margin-top: 5%">
    <div class="col-md-8 col-md-offset-2">
        <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Create Campaign</h3>
                </div>
                <div class="panel-body">
                <f:form modelAttribute="campaign" class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName" class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                            <f:input path="name" type="text" class="form-control" placeholder="Name" autocomplete="off" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="startDate" class="col-lg-2 control-label">Start date</label>
                        <div class="col-lg-10">
                            <f:input path="timestampFrom" type="date" class="form-control" placeholder="Start Date" autocomplete="off" required="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="endDate" class="col-lg-2 control-label">End Date</label>
                        <div class="col-lg-10">
                            <f:input path="timestampTo" type="date" class="form-control"  placeholder="End Date" autocomplete="off" required="required" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="site" class="col-lg-2 control-label">Site</label>
                        <div class="col-lg-10" style="margin-top:8px">
                            <div class="input-group">
                                <f:select id="siteSelect" path="site.id" class="form-control chosen-select">
                                    <f:options items="${sites}" itemValue="id" itemLabel="name" ></f:options>
                                </f:select>       
                                <span class="input-group-btn">
                                    <button id="addSiteButton" style="margin-left: 2px; margin-top:2px" class="btn btn-success btn-xs" title="Add Site"><span class="glyphicon glyphicon-plus"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2"> </div>
                        <div class="col-lg-8">
                            <f:button type="submit" class="btn btn-success btn-block"> Submit </f:button>
                            </div>
                        </div>
                </f:form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src=<c:url value="/resources/js/formoid-solid-green.js"/>></script>
<script type="text/javascript">

    function site_success(response) {
        $('#siteSelect').append(new Option(response.name, response.id, true, true));
        $("#siteSelect").trigger("chosen:updated");
        //alert(response.id);
        strSuccess = "Site successfully created.";
        $('#success-box .success-message').html(strSuccess);
        $('#success-box').removeClass("hide").hide().fadeIn("slow");
        $('.modal').modal('toggle');
    }

    function serialize_form(arg) {
        return  arg.serializeObject();
    }



    $(document).ready(function () {
        wsPost('#siteForm', site_success, undefined, undefined, undefined);
        $("#addSiteButton").click(function (e) {
            e.preventDefault();
            // validate form inputs
            // if validated
            $('#myModal').modal('show');
        });
    });
</script>