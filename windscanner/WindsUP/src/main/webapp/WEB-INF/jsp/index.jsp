<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
  prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
  prefix="fn" %>
<t:wsHeader title="Home" />

<div class="jumbotron jumbotron-hp">
  <div class="container">
    <h1>The WindScanner <span class="nowrap">E-Science</span> Platform</h1>
  </div>
</div>

<div class="container marketing">
  <div class="row">
    <div class="col-md-4 col-lg-4">
      <img class="img-circle" data-src="holder.js/140x140" alt="140x140" style="width: 140px; height: 140px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAACMCAYAAACuwEE+AAAFGklEQVR4Xu2YSUtkSxBGoxFFcFrowoXDSlR067QRRH+2AypOCCrigAMuBHdSOCM4PTLBxipflxVdH9VonAIX3htEGuc75s1bvwqFwpvxgUCFBH4hTIWkKMsEEAYRXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEfj2wry+vtrOzo4VCgWbmZkpGv7p6clWVlbs+fn597107eTkxC4vL3NtR0eH9fX1WX19fUXgar1eRX9UDYu+tTA3Nzd2fHxsV1dXGVmpMOne+fl50b3Dw0O7uLiwwcHBfD393tXVZQMDA19ir/V6X/5B/6DgWwszOztrbW1tdn19/UmYx8dH29rasoeHh6J7q6ur+drk5GS+vrS0ZE1NTTY0NGSbm5vW3Nxso6Oj9vb2ZhsbG7l2ZGTEWltbTbnexMTEP4i7+iW/tTC7u7t5p1hcXPwkzN7enrW3t9v+/n7RvYWFBXt5ebHp6eksxfz8vNXV1dnU1FTerdKOlHab9Bg7PT213t7e/MhKH/V61cdX+w7fWph3XOk//+Mj6fb21g4ODvJOMTc3V3SvtPbj70mk9fX1LEs6qzQ0NNj4+HgW6uNHtV7t465+xR8pzPb2tvX09OQdpjTccjtMwnl2dpZ/0qe7u9v6+/s/US7tWc161UdY2w4/Upj3QEtRpkPx2tqa3d/f5zNMeiQtLy/nM0w6U6Q3qHTGSTtK2mXS/XS9sbGx7A7zt+vVNmrNaj9SmHKPj/dzyv+9Jb2/QaUDcBLm6OjIOjs7bXh4uKwwf7ueJsLadgknTDqnJBFKv4dJb0PpLamlpcXGxsZ+vyXd3d3lt6T0NvanM1M5Yf60XqXf+9RWh69X+xHCfD0mFSoCCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkD8IECVo1JsKoSAbpgzBBglaNiTAqkkH6IEyQoFVjIoyKZJA+CBMkaNWYCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkD8IECVo1JsKoSAbpgzBBglaNiTAqkkH6IEyQoFVjIoyKZJA+CBMkaNWYCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkz3/z5NemdHQmIwAAAABJRU5ErkJggg==">
      <h2><a href="<c:url value="/campaigns"/>">Campaigns</a></h2>
      <p>Browse campaigns, their metadata and datasets; enabling users to explore, analyze and download research data.</p>
      </div>
    <div class="col-md-4 col-lg-4">
      <img class="img-circle" data-src="holder.js/140x140" alt="140x140" style="width: 140px; height: 140px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAACMCAYAAACuwEE+AAAFGklEQVR4Xu2YSUtkSxBGoxFFcFrowoXDSlR067QRRH+2AypOCCrigAMuBHdSOCM4PTLBxipflxVdH9VonAIX3htEGuc75s1bvwqFwpvxgUCFBH4hTIWkKMsEEAYRXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEfj2wry+vtrOzo4VCgWbmZkpGv7p6clWVlbs+fn597107eTkxC4vL3NtR0eH9fX1WX19fUXgar1eRX9UDYu+tTA3Nzd2fHxsV1dXGVmpMOne+fl50b3Dw0O7uLiwwcHBfD393tXVZQMDA19ir/V6X/5B/6DgWwszOztrbW1tdn19/UmYx8dH29rasoeHh6J7q6ur+drk5GS+vrS0ZE1NTTY0NGSbm5vW3Nxso6Oj9vb2ZhsbG7l2ZGTEWltbTbnexMTEP4i7+iW/tTC7u7t5p1hcXPwkzN7enrW3t9v+/n7RvYWFBXt5ebHp6eksxfz8vNXV1dnU1FTerdKOlHab9Bg7PT213t7e/MhKH/V61cdX+w7fWph3XOk//+Mj6fb21g4ODvJOMTc3V3SvtPbj70mk9fX1LEs6qzQ0NNj4+HgW6uNHtV7t465+xR8pzPb2tvX09OQdpjTccjtMwnl2dpZ/0qe7u9v6+/s/US7tWc161UdY2w4/Upj3QEtRpkPx2tqa3d/f5zNMeiQtLy/nM0w6U6Q3qHTGSTtK2mXS/XS9sbGx7A7zt+vVNmrNaj9SmHKPj/dzyv+9Jb2/QaUDcBLm6OjIOjs7bXh4uKwwf7ueJsLadgknTDqnJBFKv4dJb0PpLamlpcXGxsZ+vyXd3d3lt6T0NvanM1M5Yf60XqXf+9RWh69X+xHCfD0mFSoCCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkD8IECVo1JsKoSAbpgzBBglaNiTAqkkH6IEyQoFVjIoyKZJA+CBMkaNWYCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkD8IECVo1JsKoSAbpgzBBglaNiTAqkkH6IEyQoFVjIoyKZJA+CBMkaNWYCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkz3/z5NemdHQmIwAAAABJRU5ErkJggg==">
      <h2><a href="<c:url value="/semantic"/>">Search</a></h2>
      <p>Find a resource.
      We offer prepared search queries but also support powerful <abbr title="SPARQL Protocol and RDF Query Language">SPARQL</abbr> queries.
      Users can explore related resources following semantic connections.</p>
    </div>
    <div class="col-md-4 col-lg-4">
      <img class="img-circle" data-src="holder.js/140x140" alt="140x140" style="width: 140px; height: 140px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAACMCAYAAACuwEE+AAAFGklEQVR4Xu2YSUtkSxBGoxFFcFrowoXDSlR067QRRH+2AypOCCrigAMuBHdSOCM4PTLBxipflxVdH9VonAIX3htEGuc75s1bvwqFwpvxgUCFBH4hTIWkKMsEEAYRXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEUAYFy6KEQYHXAQQxoWLYoTBARcBhHHhohhhcMBFAGFcuChGGBxwEfj2wry+vtrOzo4VCgWbmZkpGv7p6clWVlbs+fn597107eTkxC4vL3NtR0eH9fX1WX19fUXgar1eRX9UDYu+tTA3Nzd2fHxsV1dXGVmpMOne+fl50b3Dw0O7uLiwwcHBfD393tXVZQMDA19ir/V6X/5B/6DgWwszOztrbW1tdn19/UmYx8dH29rasoeHh6J7q6ur+drk5GS+vrS0ZE1NTTY0NGSbm5vW3Nxso6Oj9vb2ZhsbG7l2ZGTEWltbTbnexMTEP4i7+iW/tTC7u7t5p1hcXPwkzN7enrW3t9v+/n7RvYWFBXt5ebHp6eksxfz8vNXV1dnU1FTerdKOlHab9Bg7PT213t7e/MhKH/V61cdX+w7fWph3XOk//+Mj6fb21g4ODvJOMTc3V3SvtPbj70mk9fX1LEs6qzQ0NNj4+HgW6uNHtV7t465+xR8pzPb2tvX09OQdpjTccjtMwnl2dpZ/0qe7u9v6+/s/US7tWc161UdY2w4/Upj3QEtRpkPx2tqa3d/f5zNMeiQtLy/nM0w6U6Q3qHTGSTtK2mXS/XS9sbGx7A7zt+vVNmrNaj9SmHKPj/dzyv+9Jb2/QaUDcBLm6OjIOjs7bXh4uKwwf7ueJsLadgknTDqnJBFKv4dJb0PpLamlpcXGxsZ+vyXd3d3lt6T0NvanM1M5Yf60XqXf+9RWh69X+xHCfD0mFSoCCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkD8IECVo1JsKoSAbpgzBBglaNiTAqkkH6IEyQoFVjIoyKZJA+CBMkaNWYCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkD8IECVo1JsKoSAbpgzBBglaNiTAqkkH6IEyQoFVjIoyKZJA+CBMkaNWYCKMiGaQPwgQJWjUmwqhIBumDMEGCVo2JMCqSQfogTJCgVWMijIpkkD4IEyRo1ZgIoyIZpA/CBAlaNSbCqEgG6YMwQYJWjYkwKpJB+iBMkKBVYyKMimSQPggTJGjVmAijIhmkz3/z5NemdHQmIwAAAABJRU5ErkJggg==">
      <h2><a href="<c:url value="/resources/my"/>">Resources</a></h2>
      <p>Resources are user-submitted pieces of content that range from images to publications.</p>
    </div>
  </div>
</div>

<div id="about" class="container">
  <div class="row">
    <h1>About WindS&#64UP</h1>
    <p>The foreseen WindScanner facility (WindScanner.eu) is a laser-based wind measurement
system that can generate detailed maps of wind conditions in the proximity of a wind farm
covering several square kilometres. The facility will rely on innovative remote sensing
laser-based wind measurement devices called LiDAR (Light Detection And Ranging). Basis of
this WindScanner.eu facility is the Danish WindScanner.dk facility headed up by
the Technical University of Denmark &#8211; DTU Wind Energy &#8211; during 2009-2012.</p>
    <p>This is a prototype for en e-Science platform that aims to be an Open Access distributed
    and mobile research infrastructure promoting the dissemination of results including
    innovation products and their exploitation.</p>
    <p>This platform may be used by Data Providers and Researchers. The former upload their
experimental data, along with associated metadata, organized in campaigns and datasets.
Researchers may browse published data, search them, and process them to extract knowledge
from data.</p>
  </div>
</div>

<div id="contact" class="container">
  <div class="row">
    <h2>Contact</h2>
    <p>Don't call us, we'll call you.</p>
  </div>
</div>
<!-- Page generated at ${generatedDate} -->
<t:wsFooter />