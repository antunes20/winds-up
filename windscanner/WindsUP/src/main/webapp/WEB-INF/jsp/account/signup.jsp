<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" %>
<t:wsHeader title="Sign up" />

<div id="error-box" class="alert alert-danger fade in hide" style="display: block;">
    <button type="button" class="close">×</button>
    <div class="error-message"><c:if test="${not empty error}">Your login attempt was not successful, try again.<br /> Caused :
            ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</c:if></div>
    </div>

    <div id="success-box" class="alert alert-success alert-dismissible fade in hide" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <strong>Success!</strong> You will be redirect to login page in  <strong id="timeout">5</strong> seconds...
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Sign up to continue to Winds@UP</h1>
            <div class="account-wall">
                <form id="form-signup" class="form-signin" action="<c:url value='/users/add' />" method="POST"  onSubmit="$('.btn').button('loading')">
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="firstName" placeholder="First Name" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="familyName" placeholder="Last Name" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="E-mail" required>
                </div>
                <button class="btn btn-success btn-block" type="submit"
                        data-loading-text="Creating...">Sign up</button>
                <a href="#" class="pull-right need-help">Need help? </a>
                <span class="clearfix"></span>
            </form>
        </div>
        <a href="<c:url value="/login"/>" class="text-center new-account">Do you already have an account? Sign in! </a>
    </div>
</div>
<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function() {<c:if test="${not empty error}">
        $('#error-box').removeClass("hide").hide().fadeIn("slow");</c:if>
        wsPost('#form-signup', function() {
            $('input').val("");
            $('#success-box').removeClass("hide").hide().fadeIn("slow");
            $('button').button('reset');
            var redirect = "<c:url value="/login"/>";
            var timeout = 5;
            var interval = window.setInterval(function() {
                if (timeout === 1)
                    window.clearInterval(interval);
                timeout--;
                $('#timeout').html(timeout);
            }, 1000);
            window.setTimeout(function() {
                window.location.href = redirect;
            }, 5000);
        },
                function(request, status, error) {
                    jsonValue = jQuery.parseJSON(request.responseText);
                    var errmsg = "Invalid input found in the following fields:<ul><li>"
                            + jsonValue.error.split(',').join('</li><li>') + "</li></ul>";
                    $('#error-box .error-message').html(errmsg);
                    $('#error-box').removeClass("hide").hide().fadeIn("slow");
                    $('button').button('reset');
                });
    });
    //]]>
</script>
<t:wsFooter />