<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" %>
<t:wsHeader title="Sign In" />

<div id="error-box" class="alert alert-danger fade in hide" style="display: block;">
    <button type="button" class="close">×</button>
    <div class="error-message"><c:if test="${not empty error}">Your login attempt was not successful, try again.<br /> Caused :
            ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</c:if></div>
    </div>

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Sign in to continue to Winds@UP</h1>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                     alt="">
                <form class="form-signin" action="<c:url value='j_spring_security_check' />" method="POST">
                    <div class="form-group">
                    <input type="text" class="form-control" name="j_username" placeholder="Username" required autofocus>
                    </div>
                    <div class="form-group">
                    <input type="password" class="form-control" name="j_password" placeholder="Password" required>
                    </div>
                    <button class="btn btn-success btn-block" type="submit">
                        Sign in</button>
                    <label class="checkbox pull-left">
                        <input type="checkbox" value="remember-me">
                        Remember me
                    </label>
                    <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                </form>
            </div>
            <a href="<c:url value="/signup"/>" class="text-center new-account">Create an account </a>
        </div>
    </div>
    <script type="text/javascript">
        //<![CDATA[
        $(document).ready(function() {<c:if test="${not empty error}">
            $('#error-box').removeClass("hide").hide().fadeIn("slow");</c:if>
            wsPost('#tab', function() {
                $('input[type=text]').val("");
                $('.nav-tabs a[href=#login]').tab('show');
            },
                    function(request, status, error) {
                        jsonValue = jQuery.parseJSON(request.responseText);
                        var errmsg = "Invalid input found in the following fields:<ul><li>"
                                + jsonValue.error.split(',').join('</li><li>') + "</li></ul>";
                        $('#error-box .error-message').html(errmsg);
                        $('#error-box').removeClass("hide").hide().fadeIn("slow");
                        $('#tab button').button('reset');
                    });
        });
        //]]>
    </script>
<t:wsFooter />