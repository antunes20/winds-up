<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" %>
<t:wsHeader title="${scenario.name}" />

<c:if test="${not empty message}">
    <div id="success-box" class="alert alert-success fade in">
        <button type="button" class="close">&times;</button>
        <div class="success-message">${message} </div>
    </div>
</c:if>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="${applicationScope.contextPath}"> Home</a></li>
        <li><a href="${applicationScope.contextPath}/campaigns"> Campaigns</a></li>
        <li><a href="${applicationScope.contextPath}/campaigns/<c:out value="${scenario.campaign.id}"/>"> ${scenario.campaign.name}</a></li>
        <li class="active">${scenario.name}</li>
    </ol>
    <h2>Scenario</h2>
</div>
<div class="row">
    <h4>${scenario.name}</h4>
    <hr />
    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#datasets" aria-controls="datasets" role="tab" data-toggle="tab">Datasets</a> </li>
            <li role="presentation"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a></li>
            <a href="datasets/add?s=<c:out value="${scenario.id}"/>&c=<c:out value="${scenario.campaign.id}"/>" class="btn btn-success btn-xs" title="Add Dataset" style="margin-top:11px"><span class="glyphicon glyphicon-plus"></span></a>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="datasets">
                <div class="col-md-12"><br/>
                    <table id="menu" cellpadding="0" cellspacing="0" border="0" class="client-side table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Created at
                                </th>
                                <th>
                                    Updated at
                                </th>
                                <th>
                                    Completed
                                </th>
                                <th>
                                    Published
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${datasets}" var="dataset"> 
                                <tr>
                                    <td>
                                        ${dataset.id}
                                    </td>
                                    <td>
                                        <a href="<c:url value="datasets/${dataset.id}"/>">${dataset.name}</a>
                                    </td>
                                    <td>
                                        ${dataset.createdAt}
                                    </td>
                                    <td>
                                        ${dataset.updatedAt}
                                    </td>
                                    <td>
                                        ${dataset.complete}
                                    </td>
                                    <td>
                                        ${dataset.published}
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="info">
                <div class="col-md-6"><br/>
                    <h4>Info</h4>
                    <hr />
                    <dl class="dl-horizontal">
                        <dt class="info-label">
                        ID:
                        </dt>

                        <dd class="info-data">
                            ${scenario.id}
                        </dd>

                        <dt class="info-label">
                        Name:
                        </dt>

                        <dd class="info-data">
                            ${scenario.name}
                        </dd>

                        <dt class="info-label">
                        Created at:
                        </dt>

                        <dd class="info-data">
                            ${scenario.createdAt}
                        </dd>

                        <dt class="info-label">
                        Updated at:
                        </dt>

                        <dd class="info-data">
                            ${scenario.updatedAt}
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
<t:wsFooter />