<%-- 
    Document   : add
    Created on : 30-Apr-2015, 11:41:15
    Author     : José
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" 
         %><%@ taglib  uri="http://www.springframework.org/tags/form"
         prefix="f" %>
<t:wsHeader title="Create Scenario" />
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="row" style="margin-top: 5%">
    <div class="col-md-8 col-md-offset-2">
        <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Create Scenario</h3>
                </div>
                <div class="panel-body">
                <f:form  modelAttribute="scenario" class="form-horizontal" action="add" method="POST">
                    <div class="form-group">
                        <label for="inputName" class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                            <f:input path="name" type="text" class="form-control" placeholder="Name" autocomplete="off" required="required" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="startDate" class="col-lg-2 control-label">Start Date</label>
                        <div class="col-lg-10">
                            <div class='input-group date' id='datetimepicker1'>
                                <f:input path="timestampFrom" type='text' placeholder="Start Date" class="form-control" id="startDate" required="required"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="endDate" class="col-lg-2 control-label">End Date</label>
                        <div class="col-lg-10">
                            <div class='input-group date' id='datetimepicker2'>
                                <f:input path="timestampTo" type='text' placeholder="End Date" class="form-control" id="endDate" required="required"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="campaign" class="col-lg-2 control-label">Campaign</label>
                        <div class="col-lg-10" style="margin-top:8px">
                            <div class="input-group" style="display: block">
                                <f:select id="siteSelect" path="campaign.id" class="form-control chosen-select" style="display: block">
                                    <c:forEach var="c" items="${campaigns}"  >
                                        <option value="${c.id}" ${c.id == cid ? 'selected="selected"' : ''}>${c.name}</option>
                                    </c:forEach>
                                </f:select>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-2"> </div>
                        <div class="col-lg-8">
                            <f:button type="submit" class="btn btn-success btn-block"> Submit </f:button>
                            </div>
                        </div>
                </f:form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#datetimepicker1').datetimepicker({
            locale: 'en',
            format: 'DD-MM-YYYY HH:mm:ss'
        });
        $('#datetimepicker2').datetimepicker({
            locale: 'en',
            format: 'DD-MM-YYYY HH:mm:ss'
        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        });
    });
</script>