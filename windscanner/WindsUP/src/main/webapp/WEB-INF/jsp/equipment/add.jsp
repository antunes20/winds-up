<%-- 
    Document   : add
    Created on : 13-May-2015, 16:41:45
    Author     : José
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" 
         %><%@ taglib  uri="http://www.springframework.org/tags/form"
         prefix="f" %>
<t:wsHeader title="Create Equipment" />
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>



<div id="error-box" class="alert alert-danger fade in hide">
    <button type="button" class="close">&times;</button>
    <div class="error-message"></div>
</div>

<div id="success-box" class="alert alert-success fade in hide">
    <button type="button" class="close">&times;</button>
    <div class="success-message"></div>
</div>

<div class="row" style="margin-top: 5%">
    <div class="col-md-8 col-md-offset-2">
        <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
        <t:equipmentForm/>
    </div>
</div>
<t:wsFooter />
