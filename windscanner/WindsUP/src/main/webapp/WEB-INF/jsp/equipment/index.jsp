<%-- 
    Document   : index
    Created on : 13-May-2015, 16:51:22
    Author     : José
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"
         %>
<t:wsHeader title="Equipments" />
<div class="row">
    <ol class="breadcrumb">
        <li><a href="${applicationScope.contextPath}"> Home</a></li>
        <li class="active">Equipments</li>
    </ol>
</div>

<div class="row">

    <h2>Equipments <a href="equipments/add" class="btn btn-success btn-xs" title="Add Equipment"><span class="glyphicon glyphicon-plus"></span></a></h2>

    <table cellpadding="0" cellspacing="0" border="0" class="client-side table table-striped table-bordered">
        <thead>
            <tr>
                <th>
                    S/N
                </th>
                <th>
                    Name
                </th>
                <th>
                    Type
                </th>
                <th>
                    Model
                </th>
                <th>
                    Manufacturer
                </th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${equipments}" var="equipment"> 
                <tr>

                    <td>
                        <a href="<c:url value="equipments/${equipment.serialNumber}"/>">${equipment.serialNumber}</a>
                    </td>
                    <td>
                        ${equipment.name}
                    </td>
                    <td>

                        ${equipment.type}
                    </td>
                    <td>
                        ${equipment.model}
                    </td>
                    <td>
                        ${equipment.manufacturer}
                    </td>

                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>


<t:wsFooter />
