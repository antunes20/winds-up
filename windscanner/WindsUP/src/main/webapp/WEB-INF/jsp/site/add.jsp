<%-- 
    Document   : add
    Created on : 16-Mar-2015, 16:12:49
    Author     : José
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" 
         %><%@ taglib  uri="http://www.springframework.org/tags/form"
         prefix="f" %>
<t:wsHeader title="Create Site" />

<div class="row" style="margin-top: 5%">
    <div class="col-md-8 col-md-offset-2">
        <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
        <t:SiteForm />
    </div>
</div>
<script type="text/javascript" src=<c:url value="/resources/js/formoid-solid-green.js"/>></script>