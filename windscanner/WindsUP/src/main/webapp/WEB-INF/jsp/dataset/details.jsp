<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" %><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"
         %>
<t:wsHeader title="${dataset.name}" />

<c:if test="${not empty message}">
    <div id="success-box" class="alert alert-success fade in">
        <button type="button" class="close">&times;</button>
        <div class="success-message">${message} </div>
    </div>
</c:if>

<div class="modal fade" id="mainModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-right: 10px; padding-top: 5px"><span aria-hidden="true">&times;</span></button>
            <t:producedForm />      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="equipmentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-right: 10px; padding-top: 5px"><span aria-hidden="true">&times;</span></button>
            <t:equipmentForm />      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="stationModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-right: 10px; padding-top: 5px"><span aria-hidden="true">&times;</span></button>
            <t:stationForm />      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="row">
    <ol class="breadcrumb">
        <li><a href="${applicationScope.contextPath}"> Home</a></li>
        <li><a href="${applicationScope.contextPath}/campaigns"> Campaigns</a></li>
        <li><a href="${applicationScope.contextPath}/campaigns/scenarios/<c:out value="${dataset.scenario.id}"/>"> ${dataset.scenario.name}</a></li>
        <li class="active">${dataset.name}</li>
    </ol>
    <h2>Dataset</h2>
</div>
<div class="row">
    <h4>${dataset.name}</h4>
    <hr />
    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#measurements" aria-controls="measurements" role="tab" data-toggle="tab">Measurements</a> </li>
            <li role="presentation"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a></li>
            <li role="presentation"><a href="#equipments" aria-controls="info" role="tab" data-toggle="tab">Equipments</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="measurements">
                <div class="row">
                    <div class="col-md-8 col-md-offset-5"><br/>
                        <form id="filterForm" class="form-inline">

                            <div class="form-group">
                                <label for="dsFrom">From</label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" id="dsFrom" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="dsTo">To</label>
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' class="form-control" id="dsTo" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="filterButton" type="submit" class="btn btn-default">Filter</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><br/>

                        <table id="measurementsTable" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <c:forEach var="column" items="${dsTemplateColumns}"  >
                                        <th>
                                            <c:out value="${column.shortName}" />
                                        </th>
                                    </c:forEach>                       
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="info">
                <div class="col-md-6"><br/>
                    <h4>Info</h4>
                    <hr />
                    <dl class="dl-horizontal">
                        <dt class="info-label">
                        ID:
                        </dt>

                        <dd class="info-data">
                            ${dataset.id}
                        </dd>

                        <dt class="info-label">
                        Name:
                        </dt>

                        <dd class="info-data">
                            ${dataset.name}
                        </dd>

                        <dt class="info-label">
                        Created at:
                        </dt>

                        <dd class="info-data">
                            ${dataset.createdAt}
                        </dd>

                        <dt class="info-label">
                        Updated at:
                        </dt>

                        <dd class="info-data">
                            ${dataset.updatedAt}
                        </dd>
                    </dl>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="equipments">
                <div class="row">
                    <div class="col-md-12"><br/>
                        <table id="equipmentsTable" cellpadding="0" cellspacing="0" border="0" class="client-side table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>
                                        Equipment S/N
                                    </th>
                                    <th>
                                        Start date
                                    </th>
                                    <th>
                                        End date
                                    </th>
                                    <th>
                                        Station
                                    </th>
                                    <th>
                                        Height
                                    </th>
                                    <th>
                                        Azimuth
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${produced}" var="item"> 
                                    <tr>
                                        <td>
                                            <a href="<c:url value="/equipments/${item.producedPK.equipmentId}"/>"> ${item.producedPK.equipmentId}</a>
                                        </td>
                                        <td>
                                            <fmt:formatDate value="${item.startDate}" pattern="dd-MM-yyy HH:mm:ss" />
                                        </td>
                                        <td>
                                            <fmt:formatDate value="${item.endDate}" pattern="dd-MM-yyy HH:mm:ss" />
                                        </td>
                                        <td>
                                            <a href="<c:url value="/stations/{item.stationId.id}"/>"> ${item.stationId.name}</a>
                                        </td>
                                        <td>
                                            ${item.height}
                                        </td>
                                        <td> 
                                            ${item.azimuthAngle}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <form id="filterForm" class="form-inline">
                            <div class="form-group">
                                <a id="addEquipmentButton" href="#">Add Equipment</a>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<t:wsFooter />

<script type="text/javascript">
    $(function () {

        function equipment_success(response) {
            $('#equipmentSelect').append(new Option(response.serialNumber, response.serialNumber, true, true));
            $("#equipmentSelect").trigger("chosen:updated");
            $("#equipmentModal").modal('toggle');
            $("#mainModal").modal('toggle');
        }
        
        function station_success(response) {
            $('#stationSelect').append(new Option(response.name, response.id, true, true));
            $("#stationSelect").trigger("chosen:updated");
            $("#stationModal").modal('toggle');
            $("#mainModal").modal('toggle');
        }
        
        wsPost('#equipmentForm', equipment_success, undefined, undefined, "${contextPath}/equipments/produced/add");
        wsPost('#stationForm', station_success, undefined, undefined, "${contextPath}/stations/add");
        $("#filterForm").submit(function (event) {
            event.preventDefault();
            $('#measurementsTable').DataTable().ajax.reload();
        });

        $('#datetimepicker1').datetimepicker({
            locale: 'en',
            format: 'DD-MM-YYYY HH:mm:ss'
        });
        $('#datetimepicker2').datetimepicker({
            locale: 'en',
            format: 'DD-MM-YYYY HH:mm:ss'
        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        });

        $("#addEquipmentButton").click(function (event) {
            event.preventDefault();
            $('#mainModal').modal('show');
            $('#mainModal').on('shown.bs.modal', function () {
                $('.chosen-select').chosen('destroy').chosen();
            });
        });
    });

</script>
