<%-- 
    Document   : add
    Created on : 30-Apr-2015, 13:21:51
    Author     : José
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"
         %><%@ taglib tagdir="/WEB-INF/tags" prefix="t"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" 
         prefix="c"
         %><%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
         prefix="fn" 
         %><%@ taglib  uri="http://www.springframework.org/tags/form"
         prefix="f" %>
<t:wsHeader title="Create Dataset" />
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-right: 10px; padding-top: 5px"><span aria-hidden="true">&times;</span></button>
            <t:templateForm />      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="error-box" class="alert alert-danger fade in hide">
    <button type="button" class="close">&times;</button>
    <div class="error-message"></div>
</div>

<div id="success-box" class="alert alert-success fade in hide">
    <button type="button" class="close">&times;</button>
    <div class="success-message"></div>
</div>

<div class="row" style="margin-top: 5%">
    <div class="col-md-8 col-md-offset-2">
        <c:if test="${not empty message}"><div class="message green">${message}</div></c:if>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Create Dataset</h3>
                </div>
                <div class="panel-body">
                <f:form modelAttribute="dataset" class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName" class="col-lg-2 control-label">Name</label>
                        <div class="col-lg-10">
                            <f:input path="name" type="text" class="form-control" placeholder="Name" autocomplete="off" required="required" />
                        </div>
                    </div>
 
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label  for="complete" style="margin-right: 5%">  
                                    <f:checkbox path="complete"   />Complete                 
                                </label>
                                <label  for="published" style="margin-right: 5%">
                                    <f:checkbox path="published"  />Published
                                </label>
                                <label  for="derived" style="margin-right: 5%">
                                    <f:checkbox path="derived"   />Derived
                                </label> 
                            </div>      
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="scenario" class="col-lg-2 control-label">Scenario</label>
                        <div class="col-lg-10" style="margin-top:8px">
                            <div class="input-group" style="display: block">
                                <f:select id="scenarioSelect" path="scenario.id" class="form-control chosen-select" style="display: block">
                                    <c:forEach var="s" items="${scenarios}"  >
                                        <option value="${s.id}" ${s.id == sid ? 'selected="selected"' : ''}>${s.name}</option>
                                    </c:forEach>
                                </f:select>

                            </div>
                        </div>
                    </div>                
                    <div class="form-group">
                        <label for="template" class="col-lg-2 control-label">Template</label>
                        <div class="col-lg-10" style="margin-top:8px">
                            <div class="input-group">
                                <f:select id="templateSelect" path="dsTemplateId.id" class="form-control chosen-select">
                                    <f:options items="${templates}" itemValue="id" itemLabel="name" ></f:options>
                                </f:select>       
                                <span class="input-group-btn">
                                    <button id="addTemplateButton" style="margin-left: 2px; margin-top:2px" class="btn btn-success btn-xs" title="Add Template"><span class="glyphicon glyphicon-plus"></span></button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-2"> </div>
                        <div class="col-lg-8">
                            <f:button type="submit" class="btn btn-success btn-block"> Submit </f:button>
                            </div>
                        </div>
                </f:form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src=<c:url value="/resources/js/formoid-solid-green.js"/>></script>
<script type="text/javascript">

    function template_success(response) {
        $('#templateSelect').append(new Option(response.name, response.id, true, true));
        $("#templateSelect").trigger("chosen:updated");
        //alert(response.id);
        strSuccess = "Template successfully created.";
        $('#success-box .success-message').html(strSuccess);
        $('#success-box').removeClass("hide").hide().fadeIn("slow");
        $('.modal').modal('toggle');
        // $('#templateForm').trigger('reset');
        $('#templateForm').each(function () {
            this.reset();
        });
    }

    $(document).ready(function () {
        $("#addTemplateButton").click(function (e) {
            e.preventDefault();
            // validate form inputs
            // if validated
            $('#myModal').modal('show');
        });
    });
</script>