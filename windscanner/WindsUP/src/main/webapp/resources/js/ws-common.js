$.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function randomId(size)
{
    var text = "";
    var charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (var i = 0; i < size; i++)
        text += charSet.charAt(Math.floor(Math.random() * charSet.length));
    return text;
}

/**
 * Generic AJAX POST request binding
 *
 * @param form_id form id
 * @param success_cb callback for success response
 * @param error_cb callback for error response
 * @param get_data function returning the form data
 */
function wsPost(form_id, success_cb, error_cb, get_data, action) {
    success_cb = typeof success_cb !== 'undefined' ? success_cb : false;
    error_cb = typeof error_cb !== 'undefined' ? error_cb : false;
    get_data = typeof get_data !== 'undefined' ? get_data : false;
    if (!action)
        action = $(form_id).attr('action');
    $(form_id).on('submit', function (event) {
        _wsPostAjaxCall(form_id, success_cb, error_cb, get_data, action, undefined,'application/json');
        return false;
    });
}
/**
 * Generic AJAX POST request
 *
 * @param form_id form id
 * @param success_cb callback for success response
 * @param error_cb callback for error response
 * @param get_data function returning the form data
 * @param action form target URL
 */
function wsPostNow(form_id, success_cb, error_cb, get_data, action) {
    success_cb = typeof success_cb !== 'undefined' ? success_cb : false;
    error_cb = typeof error_cb !== 'undefined' ? error_cb : false;
    get_data = typeof get_data !== 'undefined' ? get_data : false;
    if (!action)
        action = $(form_id).attr('action');
    _wsPostAjaxCall(form_id, success_cb, error_cb, get_data, action, undefined,'application/json');
    return false;
}
function _wsPostAjaxCall(form_id, success_cb, error_cb, get_data, action, fdata, type) {
    if (typeof fdata == 'undefined') {
        if (get_data == false) {
            fdata = JSON.stringify($(form_id).serializeObject());
        }
        else {
            fdata = get_data($(form_id));
        }
    }
    $.ajax({
        url: action,
        cache: false,
        type: 'POST',
        contentType: type,
        data: fdata,
        success: function (response, textStatus, XMLHttpRequest) {
            if (success_cb != false)
                success_cb(response);
        },
        error: function (request, status, error) {
            if (error_cb != false)
                error_cb(request, status, error);
            else {
                var strErr = "";
                try {
                    jsonValue = jQuery.parseJSON(request.responseText);
                    strErr = jsonValue.error;
                } catch (err) {
                    strErr = "Unknown error.";
                } finally {
                    $('#error-box .error-message').html(strErr);
                    $('#error-box').removeClass("hide").hide().fadeIn("slow");
                }
            }
        }
    });
}
/**
 * Bind typeahead to input field
 * @param item_obj Object containing parameters for the typeahead
 *                 {url,input,hidden,json_name,json_id}
 */
function wsTypeAhead(item_obj) {
    $(item_obj.input).typeahead([{
            name: 'typeahead_' + item_obj.input,
            remote: {url: item_obj.url + '%QUERY',
                filter: function (response) {
                    $.each(response, function (i, object) {
                        object.value = object[ item_obj.json_name ];
                    });
                    return response;
                }
            },
            template: '<p><strong>{{' + item_obj.json_name + '}}</strong> – {{'
                    + item_obj.json_id + '}}</p>',
            engine: Hogan
        }
    ])
            .bind('typeahead:selected', function (obj, datum) {
                $(item_obj.hidden).val(datum[ item_obj.json_id ]);
            });
}

/**
 * Creates a form and adds it to the DOM, substituting the element doc_element
 * @param doc_element element to be substituted
 * @param fields form fields in the form [type,name,value]
 * @param url form action URL
 */
function createForm(doc_element, fields, url, success_cb, submit_label) {
    var id = randomId(6);
    $form = $("<form></form>");
    $form.attr("id", id);
    $form.attr("method", "post");
    $form.attr("action", url);
    $form.addClass('form form-inline');

    $.each(fields, function () {
        $form.append('<input type="' + this[0] + '" name="' + this[1] + '" value="'
                + this[2] + '" />');
    });
    submit_label = typeof submit_label !== 'undefined' ? submit_label : "Edit";
    $form.append('<button type="submit" class="btn btn-primary" onclick="$(this).button(\'loading\')" data-loading-text="Loading...">'
            + submit_label + '</button>');

    doc_element.replaceWith($form);
    success_cb = typeof success_cb !== 'undefined' ? success_cb : function () {
        location.reload(false);
    };

    wsPost('#' + id, success_cb);
}

function stdErrorHandler(request, status, error) {
    jsonValue = jQuery.parseJSON(request.responseText);
    $('#error-box .error-message').html(jsonValue.error);
    $('#error-box').removeClass("hide").hide().fadeIn("slow");
}

$(document).ready(function () {
    /* Close button in alert boxes should close them */
    $('.alert .close').on("click", function (e) {
        $(this).parent().hide();
    });
});