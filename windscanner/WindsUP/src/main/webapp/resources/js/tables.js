/* Table initialisation */
$(document).ready(function () {
    $('.client-side').dataTable({
        "order": [[0, 'desc']]
    });

   $('#measurementsTable').DataTable({
        serverSide: true,
        searching: false,
        ajax: {
            type: 'POST',
            contentType: 'application/json',
            data: function (d) {
                d.dateFrom = $('#dsFrom').val();
                d.dateTo = $('#dsTo').val();
                var r = JSON.stringify(d);
                return r;
            }
        },
        "order": [[0, 'desc']]
    });

    $(".chosen-select").chosen({
    });
});

