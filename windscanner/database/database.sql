--
-- PostgreSQL database dump

-- DROPS
drop schema public cascade;
create schema public;
SET client_encoding = 'UTF8';

-- Authentication --
CREATE TABLE users (
  id bigserial PRIMARY KEY,
  username character varying(45) UNIQUE NOT NULL,
  password character varying(60) NOT NULL,
  first_name character varying(45) NOT NULL,
  family_name character varying(45) NOT NULL,
  email character varying(100) NOT NULL,
  active bool NOT NULL
);

CREATE TABLE security_role (
  role_id bigserial PRIMARY KEY,
  name character varying(50) UNIQUE NOT NULL
);

CREATE TABLE user_roles (
  user_id bigint,
  role_id bigint,
  PRIMARY KEY (user_id, role_id),
  CONSTRAINT FK_roles_user FOREIGN KEY (user_id) REFERENCES users (id),
  CONSTRAINT FK_roles_role_id FOREIGN KEY (role_id) REFERENCES security_role (role_id)
);
-- --

CREATE TABLE location(
  id bigserial PRIMARY KEY,
  latitude NUMERIC(9,6),
  longitude NUMERIC(9,6)
);

CREATE TABLE station (
  id bigserial PRIMARY KEY,
  name character varying(50) NOT NULL,
  loc_id bigint,
  description character varying(100),
  CONSTRAINT FK_loc_station_id FOREIGN KEY (loc_id) REFERENCES location (id)
);

CREATE TABLE site (
  id bigserial PRIMARY KEY,
  name character varying(50) NOT NULL,
  loc_id bigint,
  description character varying(100),
  CONSTRAINT FK_loc_site_id FOREIGN KEY (loc_id) REFERENCES location(id)
);


CREATE TABLE equipment(
  serial_number VARCHAR(50) PRIMARY KEY,
  name character varying(50) NOT NULL,
  type VARCHAR(20),
  model VARCHAR(20),
  manufacturer VARCHAR(20)
);

CREATE TABLE campaign (
  id bigserial PRIMARY KEY,
  name VARCHAR(100),
  owner_id bigint,
  l_id bigint,
  created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  timestamp_from TIMESTAMP WITHOUT TIME ZONE,
  timestamp_to TIMESTAMP WITHOUT TIME ZONE,
  CONSTRAINT FK_exp_owner FOREIGN KEY (owner_id) REFERENCES users (id),
  CONSTRAINT to_then_from CHECK ((timestamp_to >= timestamp_from)),
  CONSTRAINT FK_l_id FOREIGN KEY (l_id) REFERENCES site(id)
);

CREATE TABLE measuring_run (
  id bigserial PRIMARY KEY,
  name VARCHAR(100),
  created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  c_id bigint,
  CONSTRAINT FK_parent_c_id FOREIGN KEY (c_id) REFERENCES campaign (id)
  );
  
CREATE TABLE campaign_document (
  id bigserial PRIMARY KEY,
  owner_id BIGINT NOT NULL,
  c_id BIGINT,
  m_id BIGINT,
  type character varying(20) NOT NULL,
  name character varying(200) NOT NULL,
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  filename character varying(254),
  description character varying(100),
  CONSTRAINT FK_res_owner FOREIGN KEY (owner_id) REFERENCES users (id),
  CONSTRAINT FK_c_id_document FOREIGN KEY (c_id) REFERENCES campaign (id),
  CONSTRAINT FK_m_id_document FOREIGN KEY (m_id) REFERENCES measuring_run (id)
  --CONSTRAINT u_constraint UNIQUE (c_id, name) // trigger
);


CREATE TABLE dataset_template (
  id bigserial PRIMARY KEY,
  owner_id bigint NOT NULL,
  name VARCHAR(50) NOT NULL,
  description VARCHAR(200),
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  CONSTRAINT FK_ds_template_owner FOREIGN KEY (owner_id) REFERENCES users (id)
);

CREATE TABLE dataset (
  id bigserial PRIMARY KEY,
  name VARCHAR(100),
  owner_id bigint,
  created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  m_id bigint,
  ds_template_id bigint,
  config_id bigint,
  equipment_id varchar(50),
  station_id bigint,
  complete boolean DEFAULT false NOT NULL,
  published boolean DEFAULT false NOT NULL,
  CONSTRAINT FK_dataset_owner FOREIGN KEY (owner_id) REFERENCES users (id),
  CONSTRAINT FK_parent_c_id FOREIGN KEY (m_id) REFERENCES campaign (id),
  CONSTRAINT FK_config_id FOREIGN KEY (config_id) REFERENCES campaign_document (id),
  CONSTRAINT FK_equipment_id FOREIGN KEY (equipment_id) REFERENCES equipment (serial_number),
  CONSTRAINT FK_station_id FOREIGN KEY (station_id) REFERENCES station (id),
  CONSTRAINT FK_ds_template_id FOREIGN KEY (ds_template_id) REFERENCES dataset_template (id) ON DELETE RESTRICT,
  CONSTRAINT complete_then_published CHECK ((complete >= published))
);

CREATE TABLE dataset_column (
  ds_template_id bigint,
  column_pos smallint,
  display_pos smallint,
  short_name VARCHAR(50),
  long_name VARCHAR(50),
  unit VARCHAR(20),
  size smallint,
  data_type smallint,
  PRIMARY KEY (ds_template_id, column_pos),
  CONSTRAINT FK_ds_template_id_column FOREIGN KEY (ds_template_id) REFERENCES dataset_template (id) ON DELETE CASCADE,
  CONSTRAINT dataset_column_dispos UNIQUE (ds_template_id, display_pos)
  );

-- TODO
CREATE TABLE measurement (
  ds_id BIGINT,
  m_id BIGINT UNIQUE,
  timedate TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  data NUMERIC(8,6)[],
  data_int INTEGER[],
  PRIMARY KEY (ds_id, timedate),
  CONSTRAINT FK_dataset_id FOREIGN KEY (ds_id) REFERENCES dataset (id) ON DELETE CASCADE
);


CREATE TABLE event(
  id bigserial PRIMARY KEY,
  name character varying(50) NOT NULL,
  description VARCHAR(200) NOT NULL,
  "date" TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  owner_id bigint,
  CONSTRAINT FK_dataset_owner FOREIGN KEY (owner_id) REFERENCES dataset(id)
);
-- Data

INSERT INTO users (id, username, first_name, family_name, active, email, password) VALUES
(100, 'user', 'Utilizador', 'X.', TRUE, 'user@fe.up.pt', '$2a$10$Gph6Z2d7py2F1224rkPFVuEg/g6f8MyryMEI7GJVjLEKEJgAiuJ4e'),
(200, 'prov', 'Data', 'Provider', TRUE, 'prov@fe.up.pt', '$2a$10$dMucvvNYQgFbt0Mo01uUbem697UosF6M5IqC5/MHjxPuRjEjSVSFe'),
(201, 'prov2', 'Data', 'Provider II', TRUE, 'prov2@fe.up.pt', '$2a$10$9JYGowVPXWeWWg0Yyxvdpeyf/SxY9jrdKK4Fz9sjQnxrWhbETcy8K'),
(300, 'admin', 'Administrador', 'Y.', TRUE, 'admin@fe.up.pt', '$2a$10$x.zcwGc7wrQcO/IqNRmUvucHf1bjqUgkE0emsy7du11zynxIuNhiC');

INSERT INTO security_role (role_id, name) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_RESEARCHER'),
(3, 'ROLE_PROVIDER'),
(4, 'ROLE_ADMIN');

INSERT INTO user_roles (user_id, role_id) VALUES
(100, 2),
(200, 3),
(201, 3),
(300, 4);

INSERT INTO campaign (name, owner_id, created_at, updated_at) VALUES
('WS Netherlands June2013', 200, '2013-06-04 21:47:01', '2013-06-05 10:30:23'),
('test3', 201, '2013-06-06 21:34:21', '2013-06-08 12:31:23'),
('test4', 201, '2013-06-06 21:34:21', '2013-06-08 12:31:23');

INSERT INTO measuring_run ( name, created_at, updated_at, c_id) VALUES 
('Netherlands - First run','2013-06-06 21:34:21', '2013-06-08 12:31:23', 1);

INSERT INTO dataset_template (owner_id, name) VALUES
(200, 'WS raw data'),
(200, 'Test template'),
(200, 'WS derived');

INSERT INTO dataset (name, owner_id, created_at, updated_at, m_id, ds_template_id) VALUES
('R2D1', 200, '2013-06-04 21:47:01', '2013-06-05 10:30:23', 2, 1),
('R2D2', 200, '2013-06-04 21:47:01', '2013-06-05 10:30:23', 2, 1),
('R2D3', 200, '2013-06-04 21:47:01', '2013-06-05 10:30:23', 2, 1),
('derived R2D1', 200, '2013-06-10 21:47:01', '2013-06-11 10:30:23', 2, 2);

INSERT INTO dataset_column (ds_template_id, column_pos, display_pos, short_name, long_name, unit, size) VALUES
(1, 1, 1, 'pos', 'Position', 'phase', 1),
(1, 2, 2, 'spectrum', 'Spectrum', '', 255),
(2, 1, 1, 'var1', 'Variable 1', 'm/s', 1),
(2, 2, 2, 'var2', 'Variable 2', 'm/s', 1),
(2, 3, 3, 'var3', 'Variable 3', 'm/s', 1),
(2, 4, 4, 'var4', 'Variable 4', 'm/s', 1),
(2, 5, 5, 'var5', 'Variable 5', 'm/s', 1),
(2, 6, 6, 'var6', 'Variable 6', 'm/s', 1),
(2, 7, 7, 'var7', 'Variable 7', 'm/s', 1),
(2, 8, 8, 'var8', 'Variable 8', 'm/s', 1),
(2, 9, 9, 'var9', 'Variable 9', 'm/s', 1),
(2, 10, 10, 'var10', 'Variable 10', 'm/s', 1),
(2, 11, 11, 'var11', 'Variable 11', 'm/s', 1),
(2, 12, 12, 's_flag', 'Status flag', 'flag', 1),
(2, 13, 13, 'Points', 'Points', '', 1),
(2, 14, 14, 'st1', 'Status 1', '', 1),
(2, 15, 15, 'st2', 'Status 2', '', 1),
(2, 16, 16, 'st3', 'Status 3', '', 1),
(3, 1, 1, 'speed', 'Wind speed', 'm/s', 1);

--SELECT pg_catalog.setval('campaign_c_id_seq', 4, true);
--SELECT pg_catalog.setval('dataset_ds_id_seq', 4, true);