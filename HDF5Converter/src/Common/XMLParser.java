/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import static Common.Constants.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author José Magalhães
 */
public class XMLParser {

    public static void read() {

        try {

            File fXmlFile = new File(CONF_FILE);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            //NodeList nList = doc.getElementsByTagName("campaign");
            NodeList nList = doc.getDocumentElement().getChildNodes();

            //System.out.println("----------------------------");
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                //System.out.println("\nCurrent Element: " + nNode.getNodeName());
                Element eElement = (Element) nNode;
                //if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                //Element eElement = (Element) nNode;
                switch (nNode.getNodeName()) {
                    case "campaign":
                        Config.campaignID = Long.parseLong(eElement.getAttribute("id"));
                        break;
                    case "scenario":
                        Config.scenarioID = Long.parseLong(eElement.getAttribute("id"));
                        break;
                    case "equipment":
                        Config.equipmentID = Long.parseLong(eElement.getAttribute("id"));
                        break;
                    case "template":
                        Config.templateID = Long.parseLong(eElement.getAttribute("id"));
                        break;
                    case "path":
                        Config.path = eElement.getAttribute("ftp");
                        break;
                }

                /*System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
                 System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
                 System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
                 System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());*/
                // }
            }
        } catch (ParserConfigurationException | SAXException | IOException | DOMException e) {
            e.printStackTrace();
        }
    }

    public static void Write() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("conf");
            doc.appendChild(rootElement);

            // campaign elements
            Element campaign = doc.createElement("campaign");
            rootElement.appendChild(campaign);
            campaign.setAttribute("id", Long.toString(Config.campaignID));

            // set attribute to staff element
            // Attr attr = doc.createAttribute("id");
            //attr.setValue("1");
            //campaign.setAttributeNode(attr);
            // scenario elements
            Element scenario = doc.createElement("scenario");
            rootElement.appendChild(scenario);
            scenario.setAttribute("id", Long.toString(Config.scenarioID));

            // equipment elements
            Element equipment = doc.createElement("equipment");
            rootElement.appendChild(equipment);
            equipment.setAttribute("id", Long.toString(Config.equipmentID));

            // template elements
            Element template = doc.createElement("template");
            rootElement.appendChild(template);
            template.setAttribute("id", Long.toString(Config.templateID));

            // template elements
            Element path = doc.createElement("path");
            rootElement.appendChild(path);
            path.setAttribute("ftp", Config.path);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("conf.xml"));

            // Output to console for testing
            //StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }

}
