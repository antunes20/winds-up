/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Common;

/**
 *
 * @author José Magalhães
 */
public class Constants {
    public static final int NR_FIELDS = 10;
    public static final int ID_START_FIELD = 0;
    public static final int ID_STOP_FIELD = 1;
    public static final int T_START_FIELD = 2;
    public static final int T_STOP_FIELD = 3;
    public static final int AZIMUTH_FIELD = 4;
    public static final int ELEVATION_FIELD = 5;
    public static final int DISTANCE_FIELD = 6;
    public static final int RADIAL_SPEED_FIELD = 7;
    public static final int CNR_FIELD = 8;
    public static final int DISPERSION_FIELD = 9;
    public static final String UNZIP_DIRECTORY = "./unziped";
    public static final String WIND_FILE = "wind.txt";
    public static final String CONF_FILE = "conf.xml";
    public static final int NUM_THREADS = 4;
    public static final int NUM_ARGS = 10;
}
