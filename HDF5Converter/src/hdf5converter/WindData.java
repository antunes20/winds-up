/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hdf5converter;

/**
 *
 * @author José Magalhães
 */
public class WindData {
    
    public long id_start;
    public long id_stop;
    public double t_start;
    public double t_stop;
    public double azimuth;
    public double elevation;
    public double distance;
    public double radial_speed;
    public double cnr;
    public double dispersion;
    
    public WindData(){
        
    }
    public WindData(long _id_start, long _id_stop, double _t_start, double _t_stop, double _azimuth, double _elevation, double _distance,
            double _radial_speed,double _cnr, double _dispersion){
        id_start = _id_start;
        id_stop = _id_stop;
        t_start = _t_start;
        t_stop = _t_stop;
        azimuth = _azimuth;
        elevation = _elevation;
        distance = _distance;
        radial_speed = _radial_speed;
        cnr = _cnr;
        dispersion = _dispersion;
    }
    @Override
   public String toString(){
 
       return (id_start + " " + id_stop + " " + t_start + " " +  t_stop + " " + azimuth + " " + elevation + " " + distance + " " + radial_speed + " " + cnr + " " + dispersion);
       
    }
}
