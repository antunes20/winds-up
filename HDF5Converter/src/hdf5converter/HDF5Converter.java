/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hdf5converter;

import Common.Config;
import Common.Constants;
import Common.XMLParser;
import au.com.bytecode.opencsv.CSVReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.h5.H5File;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author José Magalhães
 */
public class HDF5Converter {

    private static String fname = "wind.h5";
    private static long[] dims2D = {0, 0};
    private static long[] chunks = {10, 1};
    private static H5File testFile;
    private static int dim_x, dim_y;
    private static Group root;
    private static ExecutorService executorService = Executors.newFixedThreadPool(1);
    private final static Object lock = new Object();
    private static final Options options;
    private static final HelpFormatter formatter;

    static {
        System.loadLibrary("jhdf");
        System.loadLibrary("jhdf5");
        options = new Options();
        formatter = new HelpFormatter();
        options.addOption("c", "campaign", true, "campaign id");
        options.addOption("s", "scenario", true, "scenario id");
        options.addOption("e", "equipment", true, "equipment id");
        options.addOption("t", "template", true, "template id");
        options.addOption("p", "path", true, "location of the zip file");
        options.addOption("h", "help", false, "print this message");
    }

    public static void main(String args[]) throws Exception {
        CommandLineParser parser = new BasicParser();
        CommandLine line = null;
        String arg[] = {"-c", "1", "-s", "2", "-e", "3", "-t", "4", "-p", "path"};
        try {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException exp) {
            // oops, something went wrong
            //System.err.println("Parsing failed.  Reason: " + exp.getMessage());
            formatter.printHelp("hdf5converter", options);
            System.exit(1);
        }
        if (line.hasOption("help") == true) {
            formatter.printHelp("hdf5converter", options);
            System.exit(1);
        }
        if (args.length != 0) {
            Config.campaignID = Integer.parseInt(line.getOptionValue("campaign"));
            Config.scenarioID = Integer.parseInt(line.getOptionValue("scenario"));
            Config.equipmentID = Integer.parseInt(line.getOptionValue("equipment"));
            Config.templateID = Integer.parseInt(line.getOptionValue("template"));
            Config.path = line.getOptionValue("path");
            XMLParser.Write();
        }
        XMLParser.read();
        // retrieve an instance of H5File
        FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);

        if (fileFormat == null) {
            System.err.println("Cannot find HDF5 FileFormat.");
            return;
        }

        // create a new file with a given file name.
        testFile = (H5File) fileFormat.createFile(fname, FileFormat.FILE_CREATE_DELETE);

        if (testFile == null) {
            System.err.println("Failed to create file:" + fname);
            return;
        }
        testFile.open();
        root = (Group) ((javax.swing.tree.DefaultMutableTreeNode) testFile.getRootNode()).getUserObject();
        long[] dims = {1};
        long[] value = {Config.campaignID};
        long[] value2 = {Config.scenarioID};
        Datatype dtype = testFile.createDatatype(Datatype.CLASS_INTEGER, 8, Datatype.NATIVE, Datatype.NATIVE);
        Attribute attr = new Attribute("Campaign", dtype, dims);
        Attribute attr2 = new Attribute("Scenario", dtype, dims);

        System.out.println("Extracting...");
        //unzip(args[0], null);
        System.out.println("Done!");
        System.out.println("Converting...");
        convertToHDF5();
        attr.setValue(value);
        attr2.setValue(value2);
        root.writeMetadata(attr);
        root.writeMetadata(attr2);
        System.out.println("Done!");

        // close file resource
        testFile.close();
        executorService.shutdown();
        //sendToDB(new File(fname));
    }

    public static void readFromFile(File file, Group g1) throws FileNotFoundException, Exception {
        ArrayList<Float> data = new ArrayList<Float>();
        CSVReader reader = null;
        int id_start, id_stop;
        float azimuth, elevation, t_start, t_stop;
        int i = Constants.DISTANCE_FIELD;
        int k = 0;
        String filename = file.getName();
        String name = "";
        int pos = filename.lastIndexOf(".");
        if (pos > 0) {
            name = filename.substring(0, pos);
        }
        reader = new CSVReader(new FileReader(file.getPath()), ';');

        try {
            //Get the CSVReader instance with specifying the delimiter to be used

            String[] nextLine;
            //Read one line at a time
            while ((nextLine = reader.readNext()) != null) {
                String[] nextLine2 = new String[nextLine.length - 6];
                k++;
                id_start = Integer.parseInt(nextLine[Constants.ID_START_FIELD]);
                id_stop = Integer.parseInt(nextLine[Constants.ID_STOP_FIELD]);
                t_start = Float.parseFloat(nextLine[Constants.T_START_FIELD]);
                t_stop = Float.parseFloat(nextLine[Constants.T_STOP_FIELD]);
                azimuth = Float.parseFloat(nextLine[Constants.AZIMUTH_FIELD]);
                elevation = Float.parseFloat(nextLine[Constants.ELEVATION_FIELD]);
                System.arraycopy(nextLine, 6, nextLine2, 0, nextLine.length - 6);
                for (String token : nextLine2) {
                    switch (i) {
                        case Constants.DISTANCE_FIELD:
                            data.add((float) id_start);
                            data.add((float) id_stop);
                            data.add(t_start);
                            data.add(t_stop);
                            data.add(azimuth);
                            data.add(elevation);
                            data.add(Float.parseFloat(token));
                            i = Constants.RADIAL_SPEED_FIELD;
                            break;
                        case Constants.RADIAL_SPEED_FIELD:
                            data.add(Float.parseFloat(token));
                            i = Constants.CNR_FIELD;
                            break;
                        case Constants.CNR_FIELD:
                            data.add(Float.parseFloat(token));
                            i = Constants.DISPERSION_FIELD;
                            break;
                        case Constants.DISPERSION_FIELD:
                            data.add(Float.parseFloat(token));
                            i = Constants.DISTANCE_FIELD;
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        dim_x = Constants.NR_FIELDS;
        dim_y = data.size() / dim_x;
        dims2D[0] = dim_y;
        dims2D[1] = dim_x;
        chunks[0] = dim_y;
        chunks[1] = 1;
        synchronized (lock) {
            // create 2D 32-bit (4 bytes) integer dataset
            Datatype dtype = testFile.createDatatype(Datatype.CLASS_FLOAT, 4, Datatype.NATIVE, Datatype.NATIVE);
            testFile.createScalarDS(name, g1, dtype, dims2D, null, chunks, 9, data.toArray(new Float[data.size()]));
        }
    }
    //Zip4j

    public static void unzip(String source, String password) throws Exception {
        String destination = Constants.UNZIP_DIRECTORY;
        File directory = new File(Constants.UNZIP_DIRECTORY);

        //make sure directory exists
        if (directory.exists()) {
            FileUtils.deleteDirectory(directory);
        }

        try {
            ZipFile zipFile = new ZipFile(source);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(password);
            }
            zipFile.extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }

    public static void convertToHDF5() throws Exception {
        // create new filename filter
        FilenameFilter fileNameFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                if (name.contains(Constants.WIND_FILE)) {
                    return true;
                }
                return false;
            }
        };
        File folder = new File(Constants.UNZIP_DIRECTORY);
        File[] listOfFiles = folder.listFiles();
        ExecutorCompletionService ecs = new ExecutorCompletionService(executorService);
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                System.out.println("File " + listOfFile.getName());
            } else if (listOfFile.isDirectory()) {
                File[] list;
                list = listOfFile.listFiles(fileNameFilter);
                // create groups at the root
                Group g1 = testFile.createGroup(listOfFile.getName(), root);
                for (File list1 : list) {
                    //executorService.execute(;
                    ecs.submit(new Task(list1, g1));
                }
                for (File list1 : list) {
                    ecs.take();
                }
                System.out.println("Directory " + listOfFile.getName() + " completed");
            }
        }
    }

    public static class Task implements Callable {

        File file;
        Group g;

        public Task(File f, Group _g) {
            file = f;
            g = _g;
        }

        @Override
        public Boolean call() throws Exception {
            try {
                readFromFile(file, g);
            } catch (Exception ex) {
                Logger.getLogger(HDF5Converter.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            return true;
        }
    }

    public static void sendToDB(File file) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost uploadFile = new HttpPost("http://localhost:8084/wup/uploadFile");
        System.out.println(file.getAbsolutePath());
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addTextBody("name", file.getName(), ContentType.TEXT_PLAIN);
        builder.addBinaryBody("file", file, ContentType.APPLICATION_OCTET_STREAM, file.getName());
        HttpEntity multipart = builder.build();

        uploadFile.setEntity(multipart);

        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(uploadFile);
        } catch (IOException ex) {
            Logger.getLogger(HDF5Converter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        HttpEntity responseEntity = response.getEntity();
        System.out.println(EntityUtils.toString(responseEntity, Charset.defaultCharset()));
    }
}
